#!/usr/bin/env python

import sys
import os
import datetime
import logging
import argparse
import shutil
import re
import tarfile

import pprint as pp

from osgeo import ogr

from pgclib.scenefiles import SceneFilesCollection, getFileContents, extractFileContentsFromTar
from pgclib.scenes import ScenesCollection, Scene
from pgclib.metadata import MetadataDictionary, getIKMetadataSupplement, getMetatext
from pgclib.footprint import getPgcBaseName
from pgclib import attributes as attrs
from pgclib.constants import *
from pgclib.utils import getFixedPath


__version__ = "2.2"


### Initialization
begin_time = datetime.datetime.now()
argp = argparse.ArgumentParser(description="PGC Renamer v%s" % __version__)

argp.add_argument(dest="src_path", metavar="<source folder>",
				  help="Path to the folder of imagery to rename")
argp.add_argument(dest="output", metavar="<output folder>",
                  help="Path to the folder to put renamed files into")

argp.add_argument("-x", "--execute", action="store_true", dest="execute", default=False,
                  help="Files will not be copied or moved without this flag")
argp.add_argument("-u", "--undo", action="store_true", dest="undo", default=False,
                  help="Signals an UNDO run to restore image files to origin")

argp.add_argument("-d", "--debug", action="store_true", default=False)
argp.add_argument("--log", dest="logpath")
argp.add_argument("--errlog", dest="errlogpath")

args = argp.parse_args()

# Set up logging
logfile = r"pgc_rn_v%s_%s_%s.log" % (__version__, args.src_path.split(os.sep)[-1],
											   begin_time.strftime("%Y%m%dT%H%M"))
errlogfile = r"pgc_rn_errors_v%s_%s_%s.log" % (__version__, args.src_path.split(os.sep)[-1],
                                               begin_time.strftime("%Y%m%dT%H%M"))

if args.logpath:
    if os.path.isdir(args.logpath):
        logfile = os.path.join(args.logpath, logfile)
    else:
        logfile = args.logpath
else:
    default_log_path = os.sep.join((os.environ["HOMEDRIVE"],
                                    os.environ["HOMEPATH"], "My Documents"))
    logfile = os.path.join(default_log_path, logfile)

if args.errlogpath:
    if os.path.isdir(args.errlogpath):
        errlogfile = os.path.join(args.errlogpath, errlogfile)
    else:
        errlogfile = args.errlogpath
else:
    if args.logpath and os.path.isdir(args.logpath):
        errlogfile = os.path.join(args.logpath, errlogfile)
    else:
        default_errlog_path = os.sep.join((os.environ["HOMEDRIVE"],
                                        os.environ["HOMEPATH"], "My Documents"))
        errlogfile = os.path.join(default_errlog_path, errlogfile)

basic_level = logging.DEBUG if args.debug else logging.INFO

logger = logging.getLogger("pgclib")
logger.setLevel(basic_level)

formatter = logging.Formatter("%(asctime)s :: %(module)s - %(funcName)s - %(levelname)s: %(message)s")
logfh = logging.FileHandler(logfile)
logfh.setLevel(basic_level)
logfh.setFormatter(formatter)
logefh = logging.FileHandler(errlogfile)
logefh.setLevel(logging.WARNING)
logefh.setFormatter(formatter)
logch = logging.StreamHandler(sys.stdout)
logch.setLevel(logging.INFO)
logch.setFormatter(formatter)

logger.addHandler(logfh)
logger.addHandler(logefh)
logger.addHandler(logch)


# If this is an UNDO run, src_path is the renamed imagery folder that needs to be
# restored and dst_path is determined from the .rename files

src_path, dst_path = None, None

# Source path must exist and be a folder
if args.src_path and os.path.isdir(args.src_path):
    src_path = args.src_path
else:
    logger.critical("Cannot locate imagery folder %s" % str(args.src_path))
    sys.exit(-1)

if not args.undo:

    # Destination must exist and be a folder for regular rename runs
    if args.output and os.path.isdir(args.output):
        dst_path = args.output
    else:
        logger.error("Cannot locate output folder %s" % str(args.output))
        sys.exit(-1)


# Pre-defined lists

ACN_LIST = (
    attrs.ACNSensor, attrs.ACNAcquisitionTime, attrs.ACNCatalogId,
    attrs.ACNNGAVendorImageId
)

VENDOR_NAME_DEFNS = attrs.getVendorNameDefnsFromAttributes(ACN_LIST)

SCENE_KINDS = [
    (DG, METADATA), (DG, PREVIEW),
    (GE, METADATA), (GE, PREVIEW), (GE, PREVIEW_WORLD),
    (NGA, METADATA), (NGA, PREVIEW), (NGA, PREVIEW_WORLD), (NGA, IMAGE_WORLD),
    (IK, METADATA), (IK, PREVIEW), (IK, PREVIEW_WORLD)
]

### Begin processing
if args.undo:
    logger.info("*******************************")
    logger.info("***** THIS IS AN UNDO RUN *****")
    logger.info("*******************************")

logger.info("Starting at %s" % begin_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Source path: %s" % args.src_path)
logger.info("Destination path: %s" % dst_path)


### Main processing loop

# If this is an UNDO run, all we need do is collect .rename files and reverse
# the rename operation on the imagery; all else was copied, so the new files
# can just be deleted

if args.undo:

    logger.info("Collecting .rename files...")
    scene_files = SceneFilesCollection(src_path, keeps=[UNDO])
    logger.info("Found %d files" % len(scene_files))

    if len(scene_files) == 0:
        logger.error("No files found to process - exiting...")
        sys.exit(-1)

    # Start the reversal
    cnt = 0
    for scene_file in scene_files:

        try:
            pgcfn, orgp = open(getFixedPath(scene_file.as_path), "r").read().split(",")
        except (IOError, OSError) as err:
            logger.error("Could not read %s because %s - skipping" % (scene_file.as_path, err))
            continue

        if os.path.isfile(os.path.join(src_path, pgcfn)):
            if os.path.isdir(os.path.dirname(orgp)):
                if args.execute:
                    logger.info("Reversing %s" % pgcfn)
                    try:
                        os.rename(os.path.join(src_path, pgcfn), orgp)
                        cnt += 1
                    except (IOError, OSError) as err:
                        logger.error("Could not rename %s because %s - skipping" % \
                                     (os.path.join(src_path, pgcfn), err))
                        continue
                else:
                    logger.info("Reversing: %s" % os.path.join(src_path, pgcfn))
                    logger.info("  back to: %s" % orgp)
            else:
                logger.error("Cannot locate destination directory %s - skipping" % \
                             os.path.dirname(orgp))
                continue
        else:
            logger.error("Cannot locate file %s to reverse - skipping" % \
                         os.path.join(src_path, pgcfn))
            continue

    logger.info("  ...done! Reversed %d images" % cnt)

# This is a standard renaming run, we must:
#   Collect scene files and sort them into scenes
#   Generate a pgc_name for each image file and check for collisions
#   Reconcile any duplicates (pgc_name collisions) and build a list of scenes to rename
#   Walk through the scenes and:
#     Rename (move) image file(s) and create .rename file(s)
#     If that was successful, rename other imagery files, or extract them from the
#       scene archive and rename (metadata, preview, etc.)
#     If the scene does not already have an archive, create one and bundle all
#       non-imagery (support) files into it

if not args.undo:

    # Collect SceneFiles, sort into Scenes
    logger.info("Collecting scene files...")
    scene_files = SceneFilesCollection(src_path)
    logger.info("Found %d scene files" % len(scene_files))

    if len(scene_files) == 0:
        logger.warning("No files found to process - exiting...")
        sys.exit(-1)

    logger.info("Sorting into Scenes...")
    scenes = ScenesCollection()
    scenes.loadScenesFromSceneFilesCollection(scene_files)
    scene_files = None
    logger.info("Created %d Scenes" % len(scenes))

    if not scenes.has_valid_scenes:
        logger.warning("Could not create any scenes from files - exiting...")
        sys.exit(-1)

    # The renamed_dict holds the results of the pgc_name generation process and
    # will be used to reconcile name collisions; the key is the pgc_name and
    # the value is a list of tuples, (scene.sort_code, meta_dict)
    renamed_dict = {}

    logger.info("Assigning PGC names...")
    for scene in scenes:

        # Get the baseline metadata dictionary
        meta_text = getMetatext(scene)
        if not meta_text:
            logger.error("Could not obtain metadata for %s - skipping" % \
                         scene.imageSceneFile.as_path)
            continue

        # Try to use the metadata file vendor as it is sometimes different from the
        # scene's overall vendor; otherwise use the scene vendor when creating the
        # metadata dictionary
        if scene.hasKind(METADATA):
            vendor = scene.getSceneFile(METADATA).vendor
        else:
            vendor = scene.vendor

        try:
            mdict = MetadataDictionary(meta_text, vendor, VENDOR_NAME_DEFNS)
        except (RuntimeError, ValueError) as err:
            logger.error("Could not create a metadata dictionary for %s - skipping" % \
                         scene.imageSceneFile.as_path)
            continue

        # If the scene is IKONOS get the metadata supplement
        if scene.imageSceneFile.vendor == IK:
            ik_supp = getIKMetadataSupplement(scene.imageSceneFile, mdict.tree)
            mdict.update(ik_supp)

        # To reduce the memory footprint, just keep a simple dict copy around
        meta_dict = mdict.copy()
        meta_dict["tree"] = mdict.tree
        mdict = None

        if basic_level == logging.DEBUG:
            logger.debug("meta_dict -> %s" % str(meta_dict))

        # Determine the PGC name for each image in the scene and add to the renamed_dict
        # Since most IKONOS scenes come as separate files per band, this step breaks
        # those scenes up into multiple entries; such scenes flagged for keeping will
        # wind up with metadata, preview, archive and world files for each band

        if not scene.vendor in (IK, NGA):
            pgc_name = getPgcBaseName(scene.imageSceneFile, meta_dict, None)
            ext = os.path.splitext(scene.imageSceneFile.file_name)[1].lower()
            renamed_dict.setdefault(pgc_name+ext, []).append((scene.sort_code, meta_dict))
        else:
            geom = ogr.CreateGeometryFromWkt(scene.wkt)
            if geom:
                if scene.vendor == NGA:
                    pgc_name = getPgcBaseName(scene.imageSceneFile, meta_dict, geom)
                    ext = os.path.splitext(scene.imageSceneFile.file_name)[1].lower()
                    renamed_dict.setdefault(pgc_name+ext, []).append((scene.sort_code, meta_dict))
                else:
                    for imgsf in scene.getSceneFile(IMAGE, all_bands=True):
                        if imgsf is None:
                            continue
                        pgc_name = getPgcBaseName(imgsf, meta_dict, geom)
                        ext = os.path.splitext(imgsf.file_name)[1].lower()
                        renamed_dict.setdefault(pgc_name+ext, []).append((scene.sort_code, meta_dict))
            else:
                logger.error("Could not generate geometry for %s - skipping" % \
                             scene.imageSceneFile.as_path)
                continue

        if basic_level == logging.DEBUG:
            logger.debug("Added %s:%s to renamed_dict" % (pgc_name+ext, scene.sort_code))

    logger.info("Finished assigning PGC names, processed %d images" % len(renamed_dict))

    # renamed_dict now maps PGC file names to specific Scene objects, use this to
    # reconcile duplicate names

    logger.info("Reconciling duplicate names...")
    dupes = 0
    keepers = set()
    for pgc_name in sorted(renamed_dict.iterkeys()):

        # If there's only one of this name, add to keepers and continue
        if len(renamed_dict[pgc_name]) == 1:
            scode, md = renamed_dict[pgc_name][0]
            keepers.add((scode, pgc_name))
            logger.debug("\nKEEPER -> %s, %s" % (scode, pgc_name))
            continue

        # Found a namespace collision, we must reconcile a winner from the candidates
        scode1, md1 = renamed_dict[pgc_name].pop()
        while len(renamed_dict[pgc_name]) > 0:
            scode2, md2 = renamed_dict[pgc_name].pop()
            winner, loser = None, None
            scene1, scene2 = scenes.getScene(scode1).pop(), scenes.getScene(scode2).pop()
            img1, img2 = scene1.imageSceneFile, scene2.imageSceneFile
            if basic_level == logging.DEBUG:
                logger.debug("Reconciling duplicates:")
                logger.debug("Scene1: %s" % img1.as_path)
                logger.debug("Scene2: %s" % img2.as_path)

            # If the scenes are DG tiles, we have to pick one order-id and ditch the
            # rest; for now (and in absence of any better method) the lexically-first
            # order-id wins
            if img1.is_dg_tiled and img2.is_dg_tiled:
                oid1 = img1.name_dict.get("oid")
                oid2 = img2.name_dict.get("oid")
                if oid1 and oid2:
                    if oid1 < oid2:
                        winner, loser = (scode1, md1), (scode2, md2)
                    else:
                        winner, loser = (scode2, md2), (scode1, md1)
                else:
                    logger.warning("Could not reconcile duplicate DG tiles by order-id:")
                    logger.warning("Scene1: %s" % img1.as_path)
                    logger.warning("Scene2: %s" % img2.as_path)
                    logger.warning("Falling back to file size")

            # Otherwise, the primary decider is file size, bigger wins
            if winner is None:
                if img1.size >= img2.size:
                    winner, loser = (scode1, md1), (scode2, md2)
                else:
                    winner, loser = (scode2, md2), (scode1, md1)

            # If both scenes are WV2, 8-band beats 4-band and trumps file size
            if scene1.vendor == DG and scene2.vendor == DG:
                if md1.get(attrs.ACNSensor) == "WV02" and md2.get(attrs.ACNSensor) == "WV02":
                    bands1 = scene1.raster_info.get(attrs.ACNRasterBandCount, 0)
                    bands2 = scene2.raster_info.get(attrs.ACNRasterBandCount, 0)

                    # Only change current winner if not tied for band count
                    if bands1 > bands2:
                        winner, loser = (scode1, md1), (scode2, md2)
                    elif bands2 > bands1:
                        winner, loser = (scode2, md2), (scode1, md1)

            # Record loser and continue
            logger.warning("Duplicate: %s" % scenes.getScene(loser[0]).pop().imageSceneFile.as_path)
            logger.warning(">> Replicates: %s" % scenes.getScene(winner[0])[0].imageSceneFile.as_path)
            scode1, md1 = winner
            scode2, md2 = None, None
            dupes += 1

        # Put the winner of this match into the keepers
        keepers.add((scode1, pgc_name))

    logger.info("Finished reconciling duplicates, found %d collisions" % dupes)

    # Start renaming files (or writing report); imagery files are "moved" when
    # possible, other files are copied

    logger.info("Start renaming processing...")
    for scode, pgc_name in sorted(keepers):

        scene = scenes.getScene(scode).pop()
        pgc_base_name = os.path.splitext(pgc_name)[0]

        # If this scene is IKONOS bands, determine which band is the current
        # entry from keepers
        if scene.is_ikonos_bands:
            for imgsf in scene.getSceneFile(IMAGE, True):
                if imgsf and imgsf.name_dict.get("band", "xxx") in pgc_name:
                    break

        # Otherwise just get the image file
        else:
            imgsf = scene.imageSceneFile

        # Try renaming (moving) the image file
        if args.execute:
            try:
                os.rename(getFixedPath(imgsf.as_path),
                          getFixedPath(os.path.join(dst_path, pgc_name)))
            except (IOError, OSError) as err:
                logger.error("Could not rename %s because %s - skipping scene" % \
                             (imgsf.as_path, err))
                continue
        else:
            logger.info("Old: %s" % imgsf.as_path)
            logger.info("  New: %s" % os.path.join(dst_path, pgc_name))

        # Create the .rename file
        renfn = pgc_base_name + ".rename"
        if args.execute:
            try:
                renfh = open(getFixedPath(os.path.join(dst_path, renfn)), "w")
            except (IOError, OSError) as err:
                logger.error("Could not create %s because %s - skipping scene" % (renfn, err))
                continue
            else:
                renfh.write("%s,%s" % (pgc_name, imgsf.as_path))
                renfh.close()
                if basic_level == logging.DEBUG:
                    logger.debug("Created %s for %s" % (renfn, pgc_name))
        else:
            logger.info(".rename file: %s" % getFixedPath(os.path.join(dst_path, renfn)))
            logger.info("  Contents: %s,%s" % (pgc_name, imgsf.as_path))

        # Copy non-image, primary files next (metadata, preview, world files)
        for scene_file in scene.getAllSceneFiles():

            if scene_file.kind == IMAGE:
                continue

            ext = os.path.splitext(scene_file.file_name)[1].lower()
            tail = "-BROWSE" if scene_file.kind in (PREVIEW, PREVIEW_WORLD) else ""
            # Change GeoEye metadata file extension
            if ext == ".pvl":
                ext = ".txt"

            # Try and copy the file
            if args.execute:
                if not os.path.isfile(os.path.join(dst_path, pgc_base_name+tail+ext)):
                    try:
                        shutil.copy2(getFixedPath(scene_file.as_path),
                                     getFixedPath(os.path.join(dst_path,
                                                               pgc_base_name+tail+ext)))
                    except (IOError, OSError) as err:
                        logger.error("Could not copy %s because %s - skipping" % \
                                     (scene_file.as_path, err))
                        continue
                else:
                    if basic_level == logging.DEBUG:
                        logger.debug("Skipped %s because it already existed" % \
                                     os.path.join(dst_path, pgc_base_name+tail+ext))
            else:
                logger.info("Old: %s" % scene_file.as_path)
                logger.info("  New: %s" % os.path.join(dst_path, pgc_base_name+tail+ext))

        # Extract metadata and/or preview files from archive if necessary, and rename
        if scene.hasKind(ARCHIVE):

            # Decide if anything is missing
            kinds = SCENE_KINDS
            if scene.is_ikonos_bands:
                kinds.append((IK, IMAGE_BANDS_WORLDS))
            else:
                kinds.append((IK, IMAGE_WORLD))
            flags = [not scene.hasKind(k) for v, k in kinds if v == scene.vendor]

            if any(flags):

                # Build list of targets to search archive members for
                tarsf = scene.getSceneFile(ARCHIVE)
                targets = set()

                if imgsf.vendor == DG:
                    bn = os.path.splitext(imgsf.proxy_name)[0].lower()
                    targets.add(bn+".xml")
                    targets.add(bn+"-browse.jpg")

                    # Additional targets if scene is tiled
                    if imgsf.is_dg_tiled:
                        bn2 = os.path.splitext(imgsf.getDGTiledProxyName())[0].lower()
                        targets.add(bn2+".xml")
                        targets.add(bn2+"-browse.jpg")

                        # Hack to get around R01/R1 tile name problem
                        tcode = imgsf.name_dict.get("tile")
                        if tcode and "r0" in tcode.lower():
                            targets.add(re.sub("r0", "r", bn2+".xml"))

                elif imgsf.vendor == GE:
                    bn = os.path.splitext(imgsf.proxy_name)[0].lower()
                    targets.add(bn+".pvl")
                    targets.add(bn+".jpg")
                    targets.add(bn+".jgw")

                elif imgsf.vendor == IK:
                    po = imgsf.name_dict.get("po", "XXXXXX").lower()
                    comp = imgsf.name_dict.get("cmp", "XXXXXXX").lower()
                    band = imgsf.name_dict.get("band", "XXX").lower()
                    imgext = imgsf.name_dict.get("ext", ".XXX").lower()
                    imgwext = ".nfw" if imgext == ".ntf" else ".tfw"
                    targets.add("po_%s_metadata.txt" % po)
                    targets.add("po_%s_rgb_%s_ovr.jpg" % (po, comp))
                    targets.add("po_%s_rgb_%s_ovr.jgw" % (po, comp))
                    targets.add("po_%s_%s_%s%s" % (po, band, comp, imgwext))

                elif imgsf.vendor == NGA:
                    for member in [ti.name.lower() for ti in getFileContents(tarsf)]:
                        if member.endswith((".xml", "-browse.jpg", "_ovr.jpg", "_ovr.jgw")):
                            targets.add(member)

                else:
                    logger.error("Invalid vendor code: %s" % imgsf.vendor)
                    raise ValueError("Invalid vendor code: %s" % imgsf.vendor)

                # Walk through archive members and extract all target matches
                pgc_bn = os.path.splitext(pgc_name)[0]
                for tinfo in getFileContents(tarsf):

                    if os.path.basename(tinfo.name).lower() in targets:
                        contents = extractFileContentsFromTar(tarsf, tinfo)

                        if contents:
                            ext = os.path.splitext(tinfo.name)[1].lower()
                            tail = "-BROWSE" if ext in (".jpg", ".jgw") else ""
                            if ext in (".xml", ".txt", ".jgw", ".nfw", ".tfw", ".jpg"):
                                new_name = pgc_bn + tail + ext
                            elif ext == ".pvl":
                                new_name = pgc_bn + ".txt"

                            if not os.path.isfile(os.path.join(dst_path, new_name)):

                                # Set write mode for new file
                                mode = "wb" if ext == ".jpg" else "w"

                                if args.execute:

                                    # Try and open the new file for writing
                                    try:
                                        outfh = open(getFixedPath(os.path.join(dst_path, new_name)), mode)
                                    except (IOError, OSError) as err:
                                        logger.error("Could not open file %s for writing during rename because %s" % \
                                                     (os.path.join(dst_path, new_name), err))
                                        break

                                    # Write the extracted contents to the new file
                                    try:
                                        outfh.write(contents)
                                    except (IOError, OSError) as err:
                                        logger.error("Could not write file %s during rename because %s" % \
                                                     (os.path.join(dst_path, new_name), err))
                                        break
                                    finally:
                                        outfh.close()

                                else:
                                    logger.info("Old: extracted %d bytes from TAR" % len(contents))
                                    logger.info("  new: %s" % os.path.join(dst_path, new_name))

                            else:
                                logger.info("File %s already exists, skipping extraction" % \
                                            os.path.join(dst_path, new_name))

                        else:
                            logger.error("Failed to extract file %s from TAR" % tinfo.name)

        # Build an archive if necessary
        if not scene.hasKind(ARCHIVE):

            prefix, arcname, tarfh = None, None, None
            tarfn = pgc_base_name + ".tar"
            img_exp_path = imgsf.file_dir.split(os.sep)

            # arcname marks where in the original image path (which directory) to
            # include in the archive, for the internal TAR paths, to permit correct
            # paths after untarring

            if imgsf.vendor == DG:
                arcname = imgsf.name_dict.get("oid")
            elif imgsf.vendor == GE:
                arcname = RAW_GE_GID % imgsf.name_dict
            elif imgsf.vendor == IK:
                arcname = imgsf.name_dict.get("po")
            elif imgsf.vendor == NGA:
                # TODO: handle NGA old-style scene without TAR (haven't seen any yet)
                arcname = None
            else:
                logger.error("Invalid vendor code: %s" % imgsf.vendor)
                raise ValueError("Invalid vendor code: %s" % imgsf.vendor)

            # Find which part of the image path arcname sits at
            if arcname:
                an_idx = None
                for idx, itm in enumerate(img_exp_path):
                    if arcname.lower() == itm.lower():
                        an_idx = idx

            # The prefix (path) is the portion of the image path prior to arcname
            if an_idx:
                prefix = os.sep.join(img_exp_path[:an_idx])
            else:
                if arcname:
                    prefix = imgsf.file_dir
                    logger.warning("Could not find %s in path %s - using image directory" % \
                                   (arcname, imgsf.file_dir))
                else:
                    logger.error("Could not identify an archive root folder for %s" % imgsf.as_path)
                    break

            # Try making the TAR file
            if args.execute:
                if not os.path.isfile(os.path.join(dst_path, tarfn)):
                    try:
                        tarfh = tarfile.open(getFixedPath(os.path.join(dst_path, tarfn)), "w")
                    except (IOError, OSError) as err:
                        logger.error("Could not create new archive %s because %s" % \
                                     (os.path.join(dst_path, tarfn), err))
                        break
                else:
                    logger.warning("Skipped making %s because it already exists" % \
                                   os.path.join(dst_path, tarfn))
                    continue
            else:
                logger.info("New TAR: %s" % os.path.join(dst_path, tarfn))

            # Now add files to the archive, ensuring preservation of the necessary
            # portions of the file path inside the TAR
            prefix_exp = prefix.split(os.sep)
            for scene_file in scene.getAllSceneFiles(True):
                if scene_file.kind == IMAGE:
                    continue
                sf_exp = scene_file.as_path.split(os.sep)
                for i in prefix_exp:
                    if i in sf_exp:
                        sf_exp.remove(i)
                arc_path = os.sep.join(sf_exp)

                if args.execute:
                    tarfh.add(getFixedPath(scene_file.as_path), arcname=arc_path)
                else:
                    logger.info("  adding: %s" % arc_path)

            if args.execute:
                tarfh.close()

    logger.info("  ...done! Processed %d scenes" % len(keepers))


end_time = datetime.datetime.now()
logger.info("Finished processing - total time: %s" % str(end_time - begin_time))
