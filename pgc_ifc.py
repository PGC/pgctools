#!/usr/bin/env python

import sys
import os
import argparse
import datetime
import logging
import shutil

import win32api
import win32file

import pgclib.esriutils as esri
import pgclib.constants as pgc
import pgclib.utils as utils


__version__ = "2.1.1"

fs_paths = (
	"V:/pgc/agic/private/imagery/satellite",
	"Y:/private/imagery/satellite",
	"/pgc/agic/private/imagery/satellite",
	"/private/imagery/satellite",
	"V:/pgc/data/staging/orig",
	"/pgc/data/staging/orig"
)

scene_exts = (
	".xml", ".txt", "-BROWSE.jpg", "-BROWSE.jgw", ".nfw", ".tfw", ".tar", ".prj"
)


def getDrives(type_list=[3, 4]):
	drives = [drv for drv in win32api.GetLogicalDriveStrings().split("\\\x00") if drv]
	return [drv for drv in drives if win32file.GetDriveType(drv) in type_list]


### Initialization
begin_time = datetime.datetime.now()
argp = argparse.ArgumentParser(description="PGC Imagery File Copier v%s" % __version__)
argp.add_argument("-x", "--execute", action="store_true", dest="execute", default=False,
				  help="Files will not actually be copied if option not included")
argp.add_argument("--shp", dest="shp", default=None, help="Path to shapefile of imagery to copy")
argp.add_argument("--imagefld", dest="imagefld", default="S_FILEPATH",
				  help="Shapefile field containing the path to image file")
argp.add_argument("--folderfld", dest="folderfld", default=None,
				  help="Shapefile field to use for creating subfolders within the output folder")
argp.add_argument("--dst", dest="dst", default=None, help="Destination directory for imagery files")

argp.add_argument("--debug", action="store_true", default=False)
argp.add_argument("--log", dest="logpath")
argp.add_argument("--errlog", dest="errlogpath")

args = argp.parse_args()

# Set up logging
logfile = r"pgc_ifc_v%s_%s_%s.log" % (__version__, args.shp.split(os.sep)[-1],
											   begin_time.strftime("%Y%m%dT%H%M"))
errlogfile = r"pgc_ifc_errors_v%s_%s_%s.log" % (__version__, args.shp.split(os.sep)[-1],
											   begin_time.strftime("%Y%m%dT%H%M"))

if args.logpath:
	if os.path.isdir(args.logpath):
		logfile = os.path.join(args.logpath, logfile)
	else:
		logfile = args.logpath
else:
	default_log_path = os.sep.join((os.environ["HOMEDRIVE"],
									os.environ["HOMEPATH"], "My Documents"))
	logfile = os.path.join(default_log_path, logfile)

if args.errlogpath:
	if os.path.isdir(args.errlogpath):
		errlogfile = os.path.join(args.errlogpath, errlogfile)
	else:
		errlogfile = args.errlogpath
else:
	if args.logpath and os.path.isdir(args.logpath):
		errlogfile = os.path.join(args.logpath, errlogfile)
	else:
		default_errlog_path = os.sep.join((os.environ["HOMEDRIVE"],
										os.environ["HOMEPATH"], "My Documents"))
		errlogfile = os.path.join(default_errlog_path, errlogfile)

basic_level = logging.DEBUG if args.debug else logging.INFO

logger = logging.getLogger("pgclib")
logger.setLevel(basic_level)

formatter = logging.Formatter("%(asctime)s :: %(module)s - %(funcName)s - %(levelname)s: %(message)s")
logfh = logging.FileHandler(logfile)
logfh.setLevel(basic_level)
logfh.setFormatter(formatter)
logefh = logging.FileHandler(errlogfile)
logefh.setLevel(logging.WARNING)
logefh.setFormatter(formatter)
logch = logging.StreamHandler(sys.stdout)
logch.setLevel(logging.INFO)
logch.setFormatter(formatter)

logger.addHandler(logfh)
logger.addHandler(logefh)
logger.addHandler(logch)


### Main application
logger.info("Starting at %s" % begin_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Source shapefile: %s" % args.shp)

if not args.shp or not os.path.isfile(args.shp):
	logger.error("Cannot locate imagery shapefile %s" % args.shp)
	sys.exit(-1)

if not args.dst or not os.path.isdir(args.dst):
	logger.error("Destination directory %s does not exist" % args.dst)
	sys.exit(-1)

# Build a list of external drive letters in case we need to copy imagery from one or more of them
local_disks = [drv for drv in getDrives([3])]

# Try to open shapefile and verify the file path field is defined
shp_file = esri.OgrInterfaceForESRI(esri.OGR_SHAPEFILE_DRIVER, args.shp)
if not args.imagefld in shp_file.getSchema():
	logger.error("Could not find %s in shapefile attribute fields" % args.imagefld)
	sys.exit(-1)

# Walk through features and copy found files (and their scenes) to destination
fcnt = 0
for feature in shp_file:

	# Skip feature if nothing in the file path field
	if not feature[args.imagefld]:
		logger.warning("Feature %d FILEPATH field empty - skipping" % feature.GetFID())
		continue

	logger.info("Trying %s" % os.path.basename(feature[args.imagefld]))

	# If the feature file path is on the PGC file server, just handle it; otherwise
	# determine if the necessary drive is mounted and then handle it
	src_path = None
	if feature[args.imagefld].startswith(fs_paths):
		src_path = feature[args.imagefld]
		if src_path.startswith("/pgc"):
			src_path = "V:" + src_path
		if src_path.startswith("/private"):
			src_path = "Y:" + src_path
	else:
		# If no local disks are mounted, cannot copy anything
		if not local_disks:
			logger.error("Cannot detect any local disks for non-server imagery source")
			sys.exit(-1)

		# If the file path field has a drive letter, discard it
		if ":" in feature[args.imagefld] and feature[args.imagefld].index(":") == 1:
			feat_path = os.path.splitdrive(feature[args.imagefld])[1]
		else:
			feat_path = feature[args.imagefld]

		# Verify the folder the feature file is in exists on a local disk and update path
		sep = "/" if "/" in feat_path else os.sep
		exp_feat_path = [item for item in feat_path.split(sep) if item]

		local_disk = None
		for disk in local_disks:
			if os.path.isdir(sep.join((disk, exp_feat_path[0]))):
				local_disk = disk
				break

		if not local_disk:
			logger.error("Folder %s cannot be found - skipping" % exp_feat_path[0])
			continue

		src_path = os.path.join(local_disk, feat_path)

	if not os.path.isfile(src_path):
		logger.warning("Could not locate feature source file at %s - skipping" % src_path)
		continue

	# Build destination path
	dst_path = args.dst
	if args.folderfld and feature[args.folderfld]:
		dst_path = os.path.join(args.dst, feature[args.folderfld])
		if not os.path.isdir(dst_path):
			try:
				os.mkdir(dst_path)
			except OSError:
				logger.warning("Could not create directory %s - skipping" % dst_path)
				continue

	# Get the feature file basename to derive support file names from, then start copying
	src_dir, feat_file = os.path.split(src_path)
	src_file = os.path.splitext(feat_file)[0]

	# Feature (image) file
	if args.execute:
		if not os.path.isfile(os.path.join(dst_path, feat_file)):
			try:
				shutil.copy2(src_path, dst_path)
			except (IOError, OSError) as err:
				logger.error("Could not copy %s to %s because %s - skipping" % (src_path, dst_path, err))
				continue
		else:
			logger.warning("File %s already exists at destination - skipping" % os.path.join(dst_path, feat_file))
			continue
	else:
		logger.info("Source: %s" % src_path)
		logger.info("Destination: %s" % os.path.join(dst_path, feat_file))

	# Now the support files
	for ext in scene_exts:
		src = os.path.join(src_dir, src_file+ext)
		if os.path.isfile(src):
			if args.execute:
				if not os.path.isfile(os.path.join(dst_path, src_file+ext)):
					try:
						shutil.copy2(src, dst_path)
					except (IOError, OSError) as err:
						logger.error("Could not copy %s to %s because %s - incomplete scene!" % (src, dst_path, err))
				else:
					logger.warning("File %s already exists at destination - skipping" % os.path.join(dst_path, src_file+ext))
					continue
			else:
				logger.info("Source: %s" % src)
				logger.info("Destination: %s" % os.path.join(dst_path, src_file+ext))
		else:
			continue

	fcnt += 1

logger.info("  ...done! Copied %d scenes" % fcnt)

end_time = datetime.datetime.now()
logger.info("Finished processing at: %s" % end_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Total processing time: %s" % str(end_time - begin_time))
