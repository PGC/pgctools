#!/usr/bin/env python

"""
Classes and functions for handling Scenes
"""

import os
import re
import logging
import datetime

from collections import namedtuple

from constants import *
from scenefiles import SceneFile
from scenefiles import _sortRawSceneFilesByGid as raw_sorter
from scenefiles import _sortRenamedSceneFilesByGid as ren_sorter
from geometry import getGeometryAndRasterInfo, getWarpPrefixInfo
from metadata import MetadataDictionary, getIKMetadataSupplement, getMetatext
from footprint import getPgcBaseName
import attributes as attrs


__all__ = [
    "Scene", "ScenesCollection"
]


class Scene(object):
    """
    A Scene represents all the files (as SceneFiles) associated with a particular satellite
    image (typically one part of a strip or swatch); a Scene is complete is it has an image
    file and either a metadata file or archive that contains a metadata file
    """

    ATTRIBUTES_LIST = (
        "gid", "vendor", "mode", "wkt", "raster_info", "strip_id", "order_id", "catalog_id",
        "product_code", "nga_name"
    )

    STRIPID_ACN_LIST = (
        attrs.ACNCatalogId, attrs.ACNOrderId, attrs.ACNStereo_IK, attrs.ACNProductLevel,
        attrs.ACNSpectralType, attrs.ACNSensor, attrs.ACNAcquisitionTime,
        attrs.ACNNGAVendorImageId
    )

    STRIPID_VENDOR_NAME_DEFNS = attrs.getVendorNameDefnsFromAttributes(STRIPID_ACN_LIST)

    def __init__(self, files=None):

        EMPTY_SCENE_PLACEHOLDERS = [
            None,                               # IMAGE
            {b:None for b in IKONOS_BANDS},     # IMAGE_BANDS
            None,                               # IMAGE_WORLD
            {b:None for b in IKONOS_BANDS},     # IMAGE_BANDS_WORLDS
            None,                               # METADATA
            None,                               # METADATA_SECONDARY
            None,                               # PREVIEW
            None,                               # PREVIEW_WORLD
            None,                               # ARCHIVE
            set(),                              # SUPPORT
            None,                               # UNDO
            None                                # PROJECTION
        ]

        self.files_dict = None
        self.attributes = None
        self.is_valid = None
        self.sort_code = None

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)
        if self.debug:
            self.logger.debug("Scene called with list length %d" % len(files))

        # If a collection of SceneFiles is provided, try and turn it into a Scene
        if files:

            # Initialize the files dict
            self.files_dict = dict(zip(SCENE_IMAGERY_TYPES, EMPTY_SCENE_PLACEHOLDERS))
            for scene_file in files:
                if scene_file.kind in SCENE_IMAGERY_TYPES:
                    self.addSceneFile(scene_file)
                    if self.debug:
                        self.logger.debug("Adding %s" % scene_file.as_path)

            # Set scene attributes if possible
            if self.is_complete:
                if self.debug:
                    self.logger.debug("Scene is_complete: %r" % self.is_complete)

                self.attributes = {attr:None for attr in self.ATTRIBUTES_LIST}
                imgsf = self.getSceneFile(IMAGE)
                self.attributes["vendor"] = imgsf.vendor
                #self.attributes["gid"] = imgsf.gid
                self.attributes["mode"] = imgsf.mode
                self.attributes["wkt"] = None
                self.attributes["raster_info"] = None

                if self.debug:
                    self.logger.debug("Image file looked at: %s" % imgsf.as_path)
                    self.logger.debug("Vendor: %s, Mode: %s" % (imgsf.vendor, imgsf.mode))

                try:
                    geom, rinfo = getGeometryAndRasterInfo(imgsf)
                except (RuntimeError, IOError, OSError) as err:
                    geom, rinfo = None, None
                    self.logger.error("Error obtaining geometry or raster info: %s" % err)
                else:
                    self.attributes["wkt"] = geom.ExportToWkt() if geom else None
                    self.attributes["raster_info"] = rinfo if rinfo else {}

                if self.debug:
                    self.logger.debug("Geometry: %r" % geom)
                    self.logger.debug("Raster Info: %s" % str(rinfo))

                # Find the scene strip_id and scene_id
                meta_text = getMetatext(self)
                meta_vendor = self.getSceneFile(METADATA).vendor \
                                        if self.hasKind(METADATA) else self.vendor

                if self.debug:
                    self.logger.debug("Metatext -> %d bytes" % len(meta_text))
                    self.logger.debug("Metadata Vendor: %s" % meta_vendor)

                try:
                    meta_dict = MetadataDictionary(meta_text, meta_vendor,
                                                         self.STRIPID_VENDOR_NAME_DEFNS)
                except RuntimeError as err:
                    self.logger.error("Cannot obtain metadata dictionary for image %s: %s" % \
                                      (self.imageSceneFile.as_path, err))
                    meta_dict = None
                    self.is_valid = False

                if self.debug:
                    self.logger.debug("Metadata Dictionary -> %s" % str(meta_dict))

                if meta_dict and len(meta_dict) > 0:

                    # If there is a meta_dict and this scene is IKONOS, we need some supplemental
                    # information added for scene-specific attributes
                    if self.vendor == IK:
                        ik_supp = getIKMetadataSupplement(imgsf, meta_dict.tree)
                        meta_dict.update(ik_supp)

                        if self.debug:
                            self.logger.debug("Metadata Dictionary IKONOS supplement -> %s" % str(ik_supp))

                    # Find the NGA name for the image
                    if self.debug:
                        self.logger.debug("Creating the NGA name(s)...")
                        self.logger.debug("is_ikonos_bands -> %r" % self.is_ikonos_bands)

                    if not self.is_ikonos_bands:
                        self.attributes["nga_name"] = self.getNGAName(imgsf, meta_dict)
                    else:
                        _nga_name = {sf.proxy_name:self.getNGAName(sf, meta_dict) \
                                     for sf in self.getSceneFile(IMAGE, all_bands=True) if sf}
                        self.attributes["nga_name"] = _nga_name

                    if self.debug:
                        self.logger.debug("NGA Name -> %s" % str(self.attributes.get("nga_name")))

                    if imgsf.mode == RENAMED:
                        scene_id = os.path.splitext(imgsf.file_name)[0]
                    else:
                        try:
                            scene_id = getPgcBaseName(imgsf, meta_dict, geom)
                        except RuntimeError as err:
                            scene_id = None
                            self.logger.warning("Could not get PGC name for %s - using gid" % imgsf.as_path)
                    self.attributes["gid"] = scene_id if scene_id else imgsf.gid

                    if self.debug:
                        self.logger.debug("Scene ID: %s" % str(scene_id))

                    catalog_id = meta_dict.get(attrs.ACNCatalogId)
                    order_id = meta_dict.get(attrs.ACNOrderId)
                    pnum = None

                    if imgsf.vendor == DG:
                        if order_id:
                            # We want the order_id WITH the part number for the attributes
                            # table, but NOT WITH it for the strip_id
                            if len(order_id) > 15:
                                # From metadata and has part number
                                pnum = order_id[-4:]
                                order_id = order_id[:15]
                            else:
                                # TODO: Missing part number, maybe try .rename file?
                                pnum = None
                        else:
                            # If we didn't find it in the metadata, we can get it from the
                            # image file name, if this is RAW imagery
                            order_id = imgsf.name_dict.get("oid")
                            pnum = imgsf.name_dict.get("pnum")

                        product_code = imgsf.name_dict.get("prod")
                        if product_code.startswith("NO"):
                            product_code == "XX"

                        if not catalog_id:
                            catalog_id = meta_dict.get(attrs.ACNNGAVendorImageId)

                    elif imgsf.vendor == GE:
                        product_code = "".join((imgsf.name_dict.get("band"), imgsf.name_dict.get("prod"),
                                                imgsf.name_dict.get("mono")))
                        _id = "".join((imgsf.name_dict.get("siid"), imgsf.name_dict.get("ver"),
                                       imgsf.name_dict.get("mono")))
                        if not order_id:
                            order_id = "_".join((_id, imgsf.name_dict.get("pnum")))
                        else:
                            order_id = "_".join((_id, order_id))

                    elif imgsf.vendor == IK:
                        if imgsf.name_dict.get("band"):
                            spec_type = "P" if "pan" in imgsf.name_dict.get("band") else "M"
                        else:
                            spec_type = "X"
                        if meta_dict.get(attrs.ACNStereo_IK):
                            stereo = "M" if meta_dict.get(attrs.ACNStereo_IK, "M").startswith("M") else "S"
                        else:
                            stereo = "X"
                        product_code = spec_type + "XX" + stereo
                        pnum = imgsf.name_dict.get("cmp")

                    elif imgsf.vendor == NGA:
                        if meta_dict.get(attrs.ACNSpectralType):
                            spec_type = "P" if meta_dict.get(attrs.ACNSpectralType).startswith("P") else "M"
                        else:
                            spec_type = "X"
                        prod_level = meta_dict.get(attrs.ACNProductLevel, "XX")
                        if prod_level is None or prod_level.startswith("NO"):
                            prod_level = "XX"
                        product_code = spec_type + prod_level + "M"

                    else:
                        self.logger.error("Invalid vendor code: %s" % imgsf.vendor)
                        raise ValueError("Invalid vendor code: %s" % imgsf.vendor)

                    if all((catalog_id, order_id, product_code)):
                        self.attributes["strip_id"] = "|".join((catalog_id, order_id, product_code))
                        self.attributes["catalog_id"] = catalog_id
                        self.attributes["order_id"] = "_".join((order_id, pnum)) if pnum else order_id
                        self.attributes["product_code"] = product_code
                        self.is_valid = True
                    else:
                        self.logger.warning("Missing part of strip_id: %r, %r, %r" % (catalog_id, order_id, product_code))
                        self.logger.warning("  image -> %s" % imgsf.as_path)
                        self.attributes["strip_id"] = None
                        self.is_valid = False

                    # Establish a sort code for grouping into scene-like groups without duplication
                    # Use the DG tile code if applicable
                    if imgsf.vendor == DG and imgsf.is_dg_tiled:
                        pc = "_".join((self.attributes.get("product_code"), imgsf.name_dict.get("tile")))
                    else:
                        pc = self.attributes.get("product_code")

                    self.sort_code = "|".join((str(self.attributes.get("catalog_id")),
                                               str(self.attributes.get("order_id")), str(pc)))

                    if self.debug:
                        self.logger.debug("Sort Code: %s" % self.sort_code)

                    # If we can't get important metadata, then mark the scene as invalid
                    if "None" in self.sort_code:
                        self.logger.error("Bad sort_code for %s" % imgsf.as_path)
                        self.is_valid = False

                else:
                    self.is_valid = False

            else:
                self.is_valid = False
                imgsf = self.files_dict.get(IMAGE)
                metasf = self.files_dict.get(METADATA)
                if imgsf:
                    self.logger.error("Missing metadata for image %s" % imgsf.as_path)
                elif metasf:
                    self.logger.warning("Cannot find image for metadata %s" % metasf.as_path)

        if self.debug:
            self.logger.debug("Scene is_valid: %r" % self.is_valid)

    @property
    def is_complete(self):
        return self.hasKind(IMAGE) and (self.hasKind(METADATA) or self.hasKind(ARCHIVE))

    @property
    def is_ikonos_bands(self):
        result = False
        if (self.vendor == IK) and isinstance(self.getSceneFile(IMAGE, True), list):
            scene_files = self.getSceneFile(IMAGE, all_bands=True)
            if isinstance(scene_files, list):
                result = len([sf for sf in scene_files if sf]) >= 4
        return result

    @property
    def vendor(self):
        return self.attributes.get("vendor")

    @property
    def gid(self):
        return self.attributes.get("gid")

    @property
    def mode(self):
        return self.attributes.get("mode")

    @property
    def imageSceneFile(self):
        return self.getSceneFile(IMAGE)

    @property
    def is_dg_tiled(self):
        return self.imageSceneFile.is_dg_tiled

    @property
    def which_tile(self):
        return self.imageSceneFile.name_dict.get("tile")

    @property
    def wkt(self):
        return self.attributes.get("wkt")

    @property
    def raster_info(self):
        return self.attributes.get("raster_info")

    @property
    def strip_id(self):
        return self.attributes.get("strip_id")

    @property
    def catalog_id(self):
        return self.attributes.get("catalog_id")

    @property
    def order_id(self):
        return self.attributes.get("order_id")

    @property
    def product(self):
        return self.attributes.get("product_code")

    @property
    def nga_name(self):
        return self.attributes.get("nga_name")

    def hasKind(self, kind):
        return self.files_dict.get(kind) is not None

    def getSceneFile(self, kind, all_bands=False):
        """Returns a SceneFile of the requested kind"""

        result = None
        if self.debug:
            self.logger.debug("Called asking for kind %s" % kind)

        # If the IMAGE itself is requested, and the Scene is IKONOS, extra
        # handling needed as IKONOS bands are separate files; when only one
        # image is needed, uses the IKONOS panchromatic or blue band

        if kind in [IMAGE, IMAGE_WORLD]:
            if self.vendor != IK:
                result = self.files_dict.get(kind)

            else:

                # If all_bands not requested, just return the image/image world
                if not all_bands:
                    result = self.files_dict.get(kind)

                # Return all the bands files
                else:
                    knd = IMAGE_BANDS if kind == IMAGE else IMAGE_BANDS_WORLDS
                    result = self.files_dict.get(knd).values()

        # For every other kind, just return the file as a SceneFile
        else:
            result = self.files_dict.get(kind)

        if self.debug:
            if isinstance(result, SceneFile):
                self.logger.debug("Returning SceneFile(s) %r" % result.as_path)
            else:
                self.logger.debug("Returning SceneFile %r" % result)

        return result

    def getAllSceneFiles(self, inc_sup_files=False):
        """
        Returns some or all SceneFiles associated with this Scene depending on
        whether the support files are desired or not
        """

        result = set()  # Use a set to remove duplicate support files

        for kind in SCENE_IMAGERY_TYPES:

            # Skip support files by default
            if kind == SUPPORT and not inc_sup_files:
                continue

            # When adding image files, make sure to include all the bands for IKONOS
            all_bands = True if kind in [IMAGE_BANDS, IMAGE_BANDS_WORLDS] else False
            item = self.getSceneFile(kind, all_bands)

            # Add files to set according to what is collected
            if item:

                # IKONOS bands are returned as a dictionary
                if isinstance(item, dict):
                    result = result.union(set([v for v in item.values() if v]))

                # Support files are returned as a list of paths and need to be made
                # into SceneFiles first
                elif isinstance(item, set):
                    for path in item:
                        result.add(SceneFile(path))

                # Everything else is just a SceneFile
                else:
                    result.add(item)

        return list(result)

    def addSceneFile(self, scene_file):
        """Add a SceneFile to this Scene"""

        # Handle metadata files - there can be two if one is NGA metadata
        if scene_file.kind == METADATA:

            # If this is the first one, just add it
            if self.files_dict[METADATA] is None:
                self.files_dict[METADATA] = scene_file

            # If there already is metadata, determine if we need to shuffle files
            else:
                metasf = self.files_dict[METADATA]
                if scene_file.vendor != NGA:

                    # NGA metadata is always secondary if it's not the only metadata
                    if metasf.vendor == NGA and self.files_dict[METADATA_SECONDARY] is None:
                        self.files_dict[METADATA_SECONDARY] = metasf
                        self.files_dict[METADATA] = scene_file

                else:
                    # If the secondary slot is empty, put the NGA metadata there
                    if self.files_dict[METADATA_SECONDARY] is None:
                        self.files_dict[METADATA_SECONDARY] = scene_file

        # Handle image/image_world files
        elif scene_file.kind in [IMAGE, IMAGE_WORLD]:
            knd = IMAGE_BANDS if scene_file.kind == IMAGE else IMAGE_BANDS_WORLDS

            # Is it an IKONOS file that is one band to a file?
            if scene_file.vendor == IK:

                # Check the band froom the file_name
                band = scene_file.name_dict.get("band", "")
                if band in IKONOS_BANDS:
                    self.files_dict[knd][band] = scene_file
                    if band in ["pan", "blu"] and knd == IMAGE_BANDS and not self.hasKind(IMAGE):
                        self.files_dict[IMAGE] = scene_file

                else:
                    # Must be a stacked IKONOS image file
                    self.files_dict[scene_file.kind] = scene_file

            else:
                # Any other vendor, just add the image file
                self.files_dict[scene_file.kind] = scene_file

        # Handle support files
        elif scene_file.kind == SUPPORT:
            self.files_dict[SUPPORT].add(scene_file.as_path)

        # Everything else is a straight addition
        elif scene_file.kind in SCENE_IMAGERY_TYPES:
            self.files_dict[scene_file.kind] = scene_file

    def getNGAName(self, scene_file, meta_dict):
        """Determine the NGA (WARP) name for an image file if possible"""

        _nga = None
        _pass, _op_num, _mission = getWarpPrefixInfo(scene_file)
        if scene_file and scene_file.vendor == IK:
            _mission = "IK01"
        _acqt = meta_dict.get(attrs.ACNAcquisitionTime)
        if all((_pass, _op_num, _mission, _acqt, scene_file)):
            _nga = "%s%s%s%s%s" % (_acqt.strftime("%d%b%y").upper(),
                                   _mission, _pass, _op_num, scene_file.proxy_name)
        return _nga


    def __iter__(self):
        for scene_file in self.getAllSceneFiles(True):
            yield scene_file

    def __lt__(self, other):
        return self.sort_code < other.sort_code

    def __le__(self, other):
        return self.sort_code <= other.sort_code

    def __eq__(self, other):
        return self.sort_code == other.sort_code

    def __ne__(self, other):
        return self.sort_code != other.sort_code

    def __gt__(self, other):
        return self.sort_code > other.sort_code

    def __ge__(self, other):
        return self.sort_code >= other.sort_code

    def __hash__(self):
        return hash(self.sort_code)


class ScenesCollection(object):
    """Container for a collection of Scenes"""

    def __init__(self, scenes=None, unique=False):

        self.scenes_list = []
        self.is_unique = unique

        self.logger = logging.getLogger(__name__)

        if scenes:
            self._loadScenesFromSequence(scenes, unique)

    @property
    def has_valid_scenes(self):
        return len(self.scenes_list) > 0

    @property
    def unique(self):
        return self.is_unique

    def _loadScenesFromSequence(self, scenes, unique):
        """Load container from a sequence of Scenes, discarding incomplete ones"""

        for scene in scenes:
            if not scene.is_complete or not scene.is_valid:
                self.logger.warning("Invalid or incomplete scene - skipping")
                continue
            self.addScene(scene, unique)

    def addScene(self, scene, unique):
        """Add a Scene object to the container"""

        if unique:
            if scene not in self.scenes_list:
                self.scenes_list.append(scene)
            else:
                self.logger.error("Scene already exists in collection: %s" % scene.sort_code)
                self.logger.error("  current image file: %s" % scene.imageSceneFile.as_path)
        else:
            self.scenes_list.append(scene)

    def removeScene(self, scene=None):
        """Remove a specific Scene object from the container"""

        if scene in self.scenes_list:
            del self.scenes_list[self.scenes_list.index(scene)]
        else:
            self.logger.warning("Cannot remove scene from collection, does not exist: %s" % scene.sort_code)

    def getScene(self, sort_code=None):
        """Fetch any Scene object from the container with matching sort_code"""
        return [scene for scene in self.scenes_list if scene.sort_code == sort_code]

    def loadScenesFromSceneFilesCollection(self, scenefiles, rpt=None):
        """Load the container by building Scenes from a SceneFilesCollection"""

        # SceneFilesCollection must have a consistent mode (all RAW or all RENAMED)
        if scenefiles.mode:
            sf_sorter = raw_sorter if scenefiles.mode == RAW else ren_sorter
            groups_dict = sf_sorter(scenefiles)
            for gid in sorted(groups_dict.iterkeys()):
                scene = Scene(groups_dict[gid])
                if not scene or not scene.is_complete or not scene.is_valid:
                    self.logger.warning("Invalid or incomplete scene - skipping")
                    if rpt:
                        if not scene:
                            rpt.write("\nCould not create Scene object\n")
                        elif not scene.is_valid:
                            rpt.write("\nScene is not valid\n")
                        elif not scene.is_complete:
                            rpt.write("\nScene is incomplete\n")
                        else:
                            rpt.write("\n???\n")
                        rpt.write("%s: %s\n" % (datetime.datetime.now().strftime("%H:%M:%S"), gid))
                        for path in [sf.as_path for sf in groups_dict[gid] if sf]:
                            rpt.write("  %s\n" % path)
                    continue
                self.addScene(scene, self.is_unique)
        else:
            self.logger.error("Could not determine SceneFilesCollection mode: %r" % scenefiles.mode)

    def __len__(self):
        return len(self.scenes_list)

    def __iter__(self):
        for scene in sorted(self.scenes_list):
            yield scene
