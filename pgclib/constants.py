#!/usr/bin/env python

"""
Commonly used constants and structures
"""

from collections import namedtuple


### Vendor codes
DG = "DG"    # Digital Globe
GE = "GE"    # GeoEye
IK = "IK"    # IKONOS
NGA = "NGA"  # National Geospatial Intelligence Agency

VENDORS = (
    DG, GE, IK, NGA
)

### File modes
RAW = "raw"
RENAMED = "renamed"

FILE_MODES = (
    RAW, RENAMED
)

### Imagery file types and classifications
IMAGE = "image"
IMAGE_BANDS = "image_bands"
IMAGE_WORLD = "image_world"
IMAGE_BANDS_WORLDS = "image_bands_worlds"

METADATA = "metadata"
METADATA_SECONDARY = "metadata_secondary"

PREVIEW = "preview"
PREVIEW_WORLD = "preview_world"

ARCHIVE = "archive"
PROJECTION = "projection"

SUPPORT = "support"
UNDO = "undo"
IGNORE = "ignore"
INVALID = "invalid"

IMAGERY_TYPES = (
    IMAGE, IMAGE_BANDS, IMAGE_WORLD, IMAGE_BANDS_WORLDS, METADATA, METADATA_SECONDARY,
    PREVIEW, PREVIEW_WORLD, ARCHIVE, PROJECTION, SUPPORT, UNDO, INVALID, IGNORE
)

SCENE_IMAGERY_TYPES = (
    IMAGE, IMAGE_BANDS, IMAGE_WORLD, IMAGE_BANDS_WORLDS, METADATA, METADATA_SECONDARY,
    PREVIEW, PREVIEW_WORLD, ARCHIVE, SUPPORT, UNDO, PROJECTION
)

### File identification items
IKONOS_BANDS = (
    "pan", "blu", "grn", "red", "nir"
)

VALID_IMAGERY_TAILS = (
    "-browse", "_ovr", "metadata"
)

### Regex signatures to identify file vendor, mode, kind, and create the name_dict
RAW_NGA_OLDSTYLE = r"""
			(?P<pfix>[a-z0-9_]*)                 # NGA prefix (optional)
			(?P<ts>\d\d[a-z]{3}\d\d)             # timestamp: ddmmmyy
			(?P<snsr>[a-z]{2}\d\d)               # sensor code
			(?P<unk1>\d{5})                      # unknown
			(?P<band>[a-z0-9]{2})                # pn, pm or ms
			(?P<unk2>\d{3})?                     # unknown (part num?)
			(?P<ext>\.\w+$)
		"""

RAW_DG = r"""
			(?P<pfix>[a-z0-9_]*?)                # NGA prefix (optional)
			(?P<ts>\d\d[a-z]{3}\d{8})-           # timestamp: yymmmddhhmmss
			(?P<prod>\w{4})_?                    # DG product code
			(?P<tile>\w+)?-                      # DG tiling code (optional)
			(?P<oid>\d{12}_\d\d)_                # DG order id
			(?P<pnum>p\d{3})                     # DG order part number
			(?P<tail>[a-z0-9_-]*)                # optional descriptors
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

RENAMED_DG = r"""
			(?P<pfix>ortho)?                     # possible prefix (as for orthos)
			(?P<snsr>\w\w\d\d)_                  # sensor
			(?P<ts>\d\d[a-z]{3}\d{9})-           # timestamp: yymmmddhhmmss.s
			(?P<prod>\w{4})_?                    # DG product code
			(?P<tile>\w+)?-                      # DG tile code (optional)
			(?P<catid>[a-z0-9]+)                 # DG catalog id
			(?P<tail>[a-z0-9_-]*)                # optional suffix
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

RAW_GE = r"""
			(?P<pfix>[a-z0-9_]*?)                # NGA prefix (optional)
			(?P<snsr>\d[a-z])                    # sensor code
			(?P<ts>\d{6})                        # timestamp: yymmdd
			(?P<band>[a-z])                      # pan/ms (M or one of RGBN) flag
			(?P<said>\d{9})                      # GE source archive ID
			(?P<prod>\d[a-z])                    # product code
			(?P<pid>\d{3})                       # product identifier
			(?P<siid>\d{8})                      # sub-image identifier
			(?P<ver>\d)                          # processing version
			(?P<mono>[a-z0-9])_                  # stereo/mono flag
			(?P<pnum>\d{8,9})                    # OPS product number
			(?P<tail>[a-z0-9_-]*)
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

RENAMED_GE = r"""
			(?P<pfix>ortho)?
			(?P<snsr>\w\w\d\d)_                  # sensor
			(?P<ts>\d{6})                        # timestamp: yymmdd
			(?P<band>\w)                         # pan/ms (M or one of RGBN) flag
			(?P<said>\d{9})                      # GE source archive ID
			(?P<prod>\d\w)                       # product code
			(?P<pid>\d{3})                       # product identifier
			(?P<siid>\d{8})                      # sub-image identifier
			(?P<ver>\d)                          # processing version
			(?P<mono>\w)_                        # stereo/mono flag
			(?P<pnum>\d{8,9})                    # OPS product number
			(?P<tail>[a-z0-9_-]*)
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

RAW_IK_1 = r"""
			(?P<pfix>[a-z0-9_]*?)                # NGA prefix (optional)
			po_(?P<po>\d{5,7})_                  # po number
			(?P<band>[a-z]+)_                    # band (pan, rgb, bgrn, blu, grn, red, nir)
			(?P<cmp>\d+)                         # component number
			(?P<tail>_\w*)?                      # optional descriptor
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

RAW_IK_2 = r"""
			(?P<pfix>[a-z0-9_]*?)                # NGA prefix (optional)
			po_(?P<po>\d{5,7})_                  # po number
			(?P<tail>[a-z]+)                     # descriptor ('metadata', etc.)
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

RENAMED_IK = r"""
			(?P<pfix>ortho)?
			(?P<snsr>[a-z]{2}\d\d)_              # sensor
			(?P<ts>\d{12})                       # timestamp: yyyymmddhhmm
			(?P<siid>\d+)_                       # remainder of source image id
			(?P<band>[a-z]+)_                    # band
			(?P<lat>\d{4}[ns])                   # upper left lat of extent & hemisphere
			(?P<tail>[a-z0-9_-]*)
			(?P<ext>\.\w{3}\.\w+$ | \.\w+$)
		"""

# Proxy (stripped) name formats for raw filenames
RAW_DG_PROXY = "%(ts)s-%(prod)s-%(oid)s_%(pnum)s%(tail)s%(ext)s"
RAW_GE_PROXY = "%(snsr)s%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s%(ext)s"
RAW_IK_PROXY_V1 = "po_%(po)s_%(band)s_%(cmp)s%(tail)s%(ext)s"
RAW_IK_PROXY_V2 = "po_%(po)s_%(tail)s%(ext)s"
RAW_NGA_PROXY = "%(ts)s%(snsr)s%(unk1)s%(band)s%(unk2)s%(ext)s"

RAW_DG_PROXY_TILED = "%(ts)s-%(prod)s_%(tile)s-%(oid)s_%(pnum)s%(tail)s%(ext)s"
RENAMED_DG_PROXY_TILED = "%(ts)s-%(prod)s_%(tile)s-%(catid)s%(tail)s%(ext)s"

# Group id formats for raw filenames
RAW_DG_GID = "%(ts)s-%(prod)s-%(oid)s_%(pnum)s"
RAW_DG_TILED_GID = "%(ts)s-%(prod)s_%(tile)s-%(oid)s_%(pnum)s"
RAW_GE_GID = "%(snsr)s%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s"
RAW_IK_GID = "%(po)s_%(spec)s%(cmp)s"
RAW_IK_GID_SECONDARY = "%(po)s"
RAW_NGA_GID = "%(ts)s%(snsr)s%(unk1)s%(band)s%(unk2)s"

# Group id formats for renamed filenames
RENAMED_DG_GID = "%(snsr)s_%(ts)s-%(prod)s-%(catid)s"
RENAMED_DG_TILED_GID = "%(snsr)s_%(ts)s-%(prod)s_%(tile)s-%(catid)s"
RENAMED_GE_GID = "%(snsr)s_%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s"
RENAMED_IK_GID = "%(snsr)s_%(ts)s%(siid)s_%(spec)s%(lat)s"

# File id formats for grouping within scenes
RAW_DG_FID = "%(ts)s-%(prod)s-%(oid)s_%(pnum)s"
RAW_DG_TILED_FID = "%(ts)s-%(prod)s_%(tile)s-%(oid)s_%(pnum)s"
RAW_GE_FID = "%(snsr)s%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s"
RAW_IK_FID_V1 = "po_%(po)s_%(band)s_%(cmp)s"
RAW_IK_FID_V2 = "po_%(po)s"
RAW_NGA_FID = "%(ts)s%(snsr)s%(unk1)s%(band)s%(unk2)s"
RENAMED_DG_FID = "%(snsr)s_%(ts)s-%(prod)s-%(catid)s"
RENAMED_DG_TILED_FID = "%(snsr)s_%(ts)s-%(prod)s_%(tile)s-%(catid)s"
RENAMED_GE_FID = "%(snsr)s_%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s"
RENAMED_IK_FID = "%(snsr)s_%(ts)s%(siid)s_%(band)s_%(lat)s"

# Collection of filename attributes
FilenameAttributes = namedtuple("FilenameAttributes", ["pat", "ven", "mode", "gid", "pnfmt", "fid"])
FILE_IDENTIFIERS = [
	FilenameAttributes(RAW_DG, DG, RAW, RAW_DG_GID, RAW_DG_PROXY, RAW_DG_FID),
	FilenameAttributes(RAW_GE, GE, RAW, RAW_GE_GID, RAW_GE_PROXY, RAW_GE_FID),
	FilenameAttributes(RAW_NGA_OLDSTYLE, NGA, RAW, RAW_NGA_GID, RAW_NGA_PROXY, RAW_NGA_FID),
	FilenameAttributes(RAW_IK_1, IK, RAW, RAW_IK_GID, RAW_IK_PROXY_V1, RAW_IK_FID_V1),
	FilenameAttributes(RAW_IK_2, IK, RAW, RAW_IK_GID_SECONDARY, RAW_IK_PROXY_V2, RAW_IK_FID_V2),
	FilenameAttributes(RENAMED_DG, DG, RENAMED, RENAMED_DG_GID, None, RENAMED_DG_FID),
	FilenameAttributes(RENAMED_GE, GE, RENAMED, RENAMED_GE_GID, None, RENAMED_GE_FID),
	FilenameAttributes(RENAMED_IK, IK, RENAMED, RENAMED_IK_GID, None, RENAMED_IK_FID)
]

# Regexes for identifying imagery files via folder names
RAW_DG_FOLDER = r"[\\/](?P<oid>\d{12}_\d\d)_?(?P<pnum>p\d{3})?(?P<tail>[a-z0-9_]*)[\\/]?(?!\.)"
RAW_GE_FOLDER = r"[\\/](?P<snsr>\d[a-z])(?P<ts>\d{6})(?P<band>[a-z])(?P<said>\d{9})(?P<prod>\d[a-z])(?P<pid>\d{3})(?P<siid>\d{8})(?P<ver>\d)(?P<mono>[a-z])_(?P<pnum>\d{8,9})(?P<tail>[a-z0-9_]*)[\\/]?(?!\.)"
RAW_IK_FOLDER = r"[\\/](?P<po>\d{5,7})[\\/]?(?![\.0-9a-z])"
#RAW_DG_OTHER = r"(?P<oid>\d{12}_\d\d)(?P<tail>[a-z0-9_]*)(?P<ext>\.\w{3})"

RAW_DG_GID_FROM_FOLDER = "%(oid)s"
RAW_GE_GID_FROM_FOLDER = "%(snsr)s%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s"
RAW_IK_GID_FROM_FOLDER = "%(po)s"

FOLDER_IDENTIFIERS = [
	FilenameAttributes(RAW_DG_FOLDER, DG, RAW, RAW_DG_GID_FROM_FOLDER, None, None),
	FilenameAttributes(RAW_GE_FOLDER, GE, RAW, RAW_GE_GID_FROM_FOLDER, None, None),
	FilenameAttributes(RAW_IK_FOLDER, IK, RAW, RAW_IK_GID_FROM_FOLDER, None, None)
]

# Formats for renaming files to PGC forms
PGC_NAME_DG = "%(sensor)s_%(ts)s%(tenths)s-%(prod)s-%(catalog_id)s%(tail)s%(ext)s"
PGC_NAME_DG_TILED = "%(sensor)s_%(ts)s%(tenths)s-%(prod)s_%(tile)s-%(catalog_id)s%(tail)s%(ext)s"
PGC_NAME_GE = "%(sensor)s_%(ts)s%(band)s%(said)s%(prod)s%(pid)s%(siid)s%(ver)s%(mono)s_%(pnum)s%(tail)s%(ext)s"
PGC_NAME_IK = "%(sensor)s_%(catalog_id)s_%(band)s_%(lat)s%(tail)s%(ext)s"

# Imagery file type by filename extension
EXT2TYPE = {
	".ntf":IMAGE, ".tif":IMAGE, ".jp2":IMAGE, ".img":IMAGE,
	".jpg":PREVIEW,
	".xml":METADATA, ".pvl":METADATA, ".txt":METADATA,
	".tar":ARCHIVE,
	".nfw":IMAGE_WORLD, ".tfw":IMAGE_WORLD, ".jgw":PREVIEW_WORLD,
	".prj":PROJECTION,
	".rename":UNDO
}

DG_PREVIEW_URL_FORMAT = \
    "http://browse.digitalglobe.com/imagefinder/showBrowseMetadata?buffer=1.0&catalogId=%s&imageHeight=512&imageWidth=512"
