#!/usr/bin/env python

"""
Classes and functions related to producing footprint output
"""

import sys
import os
import logging
import datetime
import re
import math

from collections import namedtuple
from dateutil import parser as DP

from osgeo import ogr

from constants import *
from utils import translatePath
from geometry import getGeometryAndRasterInfo
from metadata import MetadataDictionary, getIKMetadataSupplement, getMetatext
import attributes as attrs


__all__ = [
    "Footprint", "getPath", "makeFootprintRecord", "getPgcBaseName"
]

REQUIRED_ACNS = ( attrs.ACNNGAVendorImageId, )


class Footprint(object):
    """Class to manage user info related to producing a footprint"""

    def __init__(self, uparams):

        # uparams contains user-supplied information:
        #   'mapping' = True - Intent is to produce a map of selected imagery; can be
        #                      pulled from anywhere (file system or db), can be raw or
        #                      renamed imagery, local volume or server; does not
        #                      update anything; dates not included for raw imagery;
        #                      outpath is any supported display format; status may be
        #                      'local' if not otherwise present
        #   'ingest' = True  - Intent is to produce a display format AND update the
        #                      collection db (extending the collection); imagery must
        #                      be raw; fpadd & recvd required, default to today; status
        #                      is set to 'offline'
        #
        #   '-k': Keep drive letter when setting fields with paths
        #   '--attrset=?': <path> - Path to a text file with a list of desired attribute
        #                  common names, one per line;
        #                  'local' - preset for local attributes (no dates or server paths)
        #                  default - PGC footprint as for 'offline' status
        #   'fpadd', 'recvd': Dates for ingest mode footprinting; both default to date
        #                     script is executed on; fpmod is set to same as fpadd

        self._uparams = uparams
        self._attr_names = None
        self._vendor_defns = None
        self._user_info_dict = None

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)
        if self.debug:
            self.logger.debug("Called with user params -> %s" % str(uparams))

        # Build the user_info dictionary and set the acn list
        self._user_info_dict = {}
        attr_set = self._uparams.get("attrset", "local")

        if self._uparams.get("ingest"):
            default_date = datetime.datetime.now().strftime("%Y-%m-%d")
            self._user_info_dict[attrs.ACNStatus] = self._uparams.get("status", "offline")
            self._user_info_dict[attrs.ACNReceivedDate] = \
                        DP.parse(self._uparams.get("recvd", default_date))
            self._user_info_dict[attrs.ACNFootprintAddedDate] = \
                        DP.parse(self._uparams.get("fpadd", default_date))
            self._user_info_dict[attrs.ACNFootprintModifiedDate] = \
                        self._user_info_dict[attrs.ACNFootprintAddedDate]
            acn_list = attrs.DEFAULT_PGC_ACN_LIST

        elif self._uparams.get("server"):
            default_date = datetime.datetime.now().strftime("%Y-%m-%d")
            self._user_info_dict[attrs.ACNStatus] = self._uparams.get("status", "online")
            self._user_info_dict[attrs.ACNFootprintAddedDate] = \
                        DP.parse(self._uparams.get("fpadd", default_date))
            self._user_info_dict[attrs.ACNFootprintModifiedDate] = \
                        DP.parse(self._uparams.get("fpmod", default_date))
            acn_list = attrs.DEFAULT_PGC_ACN_LIST

        if not self._uparams.get("ingest") and not self._uparams.get("server") \
                and self._uparams.get("status") is None:
            self._user_info_dict[attrs.ACNStatus] = "local"
            if os.path.isfile(attr_set):
                acn_list = map(strip(), open(attr_set).readlines())
            elif attr_set == "local" or self._uparams.get("status") == "local":
                acn_list = attrs.DEFAULT_LOCAL_ACN_LIST

        self._attr_names = acn_list + REQUIRED_ACNS

        if self.debug:
            self.logger.debug("ACN list selected: %s" % str(acn_list))

        # Build the definitions list from the names list; an empty list will default
        # to using all available common names
        _vendor_defns_list = attrs.getVendorNameDefnsFromAttributes(acn_list)

        if _vendor_defns_list:
            self._vendor_defns = _vendor_defns_list
        else:
            self.logger.error("Could not generate an Vendor Definitions List")
            raise RuntimeError("Could not generate an Vendor Definitions List")

    @property
    def vendor_defns(self):
        return self._vendor_defns

    @property
    def attr_names(self):
        return self._attr_names

    @property
    def user_info(self):
        return self._user_info_dict

    @property
    def params(self):
        return self._uparams


### Functions

module_logger = logging.getLogger(__name__)
module_debug = module_logger.isEnabledFor(logging.DEBUG)

def getPath(path, fp_info):
    """Fix up a path according to user parameters"""

    _path = path

    # These flags are only of use in the PGC environment
    if fp_info.params.get("unixpaths", False):
        _path = translatePath(_path, w2g=True)
    elif fp_info.params.get("winpaths", False):
        _path = translatePath(_path, g2w=True)

    # Decide whether to retain a Windows drive letter on the path
    if fp_info.params.get("keepdrv", False):
        _path = _path.replace("\\", "/").strip()
    else:
        _path = os.path.splitdrive(_path)[1].replace("\\", "/").strip()

    return _path

# Generic record for footprints: id is record id, geom is an ogr.Geometry for the extent
# geometry, attrs_dict is a dictionary of all attributes for this record
FootprintRecord = namedtuple("FootprintRecord", ("id", "geom", "attrs_dict"))

def makeFootprintRecord(fpid, image_sf, scene, fp_info):
    """Assemble a FootprintRecord from a Scene and user info from Footprint"""

    fr_geom = None
    attrs_dict = {}
    if module_debug:
        module_logger.debug("makeFootprintRecord called")
        module_logger.debug("attr_defns have %d definitions" % len(fp_info.vendor_defns))

    # Use the scene metadata and attribute definitions to get a metadata dict
    if not fp_info.vendor_defns:
        module_logger.error("Missing vendor name definitions")
        raise RuntimeError("Missing vendor name definitions")

    # Get the metadata text and build the dictionary
    meta_text = getMetatext(scene)
    if not meta_text:
        module_logger.error("Could not get metadata text for %s" % image_sf.as_path)
        raise RuntimeError("Could not get metadata text for %s" % image_sf.as_path)

    # Sometimes imagery is DG/GE or IKONOS but the metadata file is old-style NGA, so
    # use the metadata file vendor if possible
    if scene.hasKind(METADATA):
        vendor = scene.getSceneFile(METADATA).vendor
    else:
        vendor = scene.vendor

    try:
        meta_dict = MetadataDictionary(meta_text, vendor, fp_info.vendor_defns)
    except (RuntimeError, ValueError) as err:
        module_logger.error("Could not create metadata dictionary for %s" % image_sf.as_path)
        raise RuntimeError("Could not create metadata dictionary for %s" % image_sf.as_path)

    attrs_dict.update(meta_dict)
    if module_debug:
        module_logger.debug("meta_dict -> %s" % str(meta_dict))

    # Use the image file to get the extent polygon (as wkt) and raster info
    if image_sf:
        if module_debug:
            module_logger.debug("image_sf -> %s" % image_sf.as_path)

        # Need some supplemental metadata if this is an IKONOS image
        if image_sf.vendor == IK:
            ik_supp = getIKMetadataSupplement(image_sf, meta_dict.tree)
            attrs_dict.update(ik_supp)

        # Add the scene raster metadata to the dictionary
        if scene.raster_info:
            attrs_dict.update(scene.raster_info)
        else:
            module_logger.warning("Failed to get raster info for %s" % image_sf.as_path)

        # Add the scene strip id to the dictionary if desired
        if attrs.ACNStripId in fp_info.attr_names:
            attrs_dict[attrs.ACNStripId] = scene.strip_id

        # Add the NGA name if desired
        if attrs.ACNNGAName in fp_info.attr_names:
            if not scene.is_ikonos_bands:
                _nga_name = scene.nga_name
            else:
                _nga_name = scene.nga_name.get(image_sf.proxy_name)
            attrs_dict[attrs.ACNNGAName] = _nga_name

        # Include some of the file metadata we want
        if attrs.ACNFileSize in fp_info.attr_names:
            file_sz = float(os.path.getsize(image_sf.as_path) / (1024.0*1024.0*1024.0))
            attrs_dict[attrs.ACNFileSize] = file_sz

        file_path = getPath(image_sf.as_path, fp_info)
        drive_name = file_path[file_path.index("/")+1:file_path.index("/", 3)]

        if fp_info.params.get("ingest"):
            if attrs.ACNOriginalFilePath in fp_info.attr_names:
                attrs_dict[attrs.ACNOriginalFilePath] = file_path
            if attrs.ACNOriginalFileName in fp_info.attr_names:
                attrs_dict[attrs.ACNOriginalFileName] = os.path.basename(file_path)
            if attrs.ACNOriginalDrive in fp_info.attr_names:
                attrs_dict[attrs.ACNOriginalDrive] = drive_name

        elif fp_info.params.get("server"):
            if attrs.ACNServerFilePath in fp_info.attr_names:
                attrs_dict[attrs.ACNServerFilePath] = file_path
            if attrs.ACNServerFileName in fp_info.attr_names:
                attrs_dict[attrs.ACNServerFileName] = os.path.basename(file_path)
            if attrs.ACNOriginalFilePath in fp_info.attr_names:
                ren_sf = scene.getSceneFile(UNDO)
                if ren_sf:
                    try:
                        orig_path = open(ren_sf.as_path, "r").read().split(",")[1]
                    except (IOError, OSError) as err:
                        orig_path = None
                        module_logger.warning("Could not open .rename file %s because %s" % \
                                              (ren_sf.as_path, err))
                    orig_path = orig_path.split(":")[-1].replace("\\", "/").strip()
                    attrs_dict[attrs.ACNOriginalFilePath] = orig_path
                    if attrs.ACNOriginalFileName in fp_info.attr_names:
                        attrs_dict[attrs.ACNOriginalFileName] = os.path.basename(orig_path)
                    if attrs.ACNOriginalDrive in fp_info.attr_names:
                        drive_name = orig_path[orig_path.index("/")+1:orig_path.index("/", 3)]
                        attrs_dict[attrs.ACNOriginalDrive] = drive_name
                else:
                    module_logger.warning("No .rename file found for image %s" % image_sf.as_path)
                    attrs_dict[attrs.ACNOriginalFilePath] = ""
                    if attrs.ACNOriginalFileName in fp_info.attr_names:
                         attrs_dict[attrs.ACNOriginalFileName] = ""

        else:
            if fp_info.user_info.get(attrs.ACNStatus, "local") == "online":
                if attrs.ACNServerFilePath in fp_info.attr_names:
                    attrs_dict[attrs.ACNServerFilePath] = file_path
                if attrs.ACNServerFileName in fp_info.attr_names:
                    attrs_dict[attrs.ACNServerFileName] = os.path.basename(file_path)
            else:
                if attrs.ACNOriginalFilePath in fp_info.attr_names:
                    attrs_dict[attrs.ACNOriginalFilePath] = file_path
                if attrs.ACNOriginalFileName in fp_info.attr_names:
                    attrs_dict[attrs.ACNOriginalFileName] = os.path.basename(file_path)
                if attrs.ACNOriginalDrive in fp_info.attr_names:
                    attrs_dict[attrs.ACNOriginalDrive] = drive_name

    else:
        module_logger.error("Could not open or find image file")
        raise RuntimeError("Could not open or find image file")

    # Add the user parameters info
    if fp_info.user_info:
        attrs_dict.update(fp_info.user_info)
        if module_debug:
            module_logger.debug("Added user info -> %s" % str(fp_info.user_info))
    else:
        module_logger.error("Could not get user parameters info")
        raise RuntimeError("Could not get user parameters info")

    # If necessary, find the PGC name for SCENE_ID
    geom = ogr.CreateGeometryFromWkt(scene.wkt)
    if scene.mode == RENAMED:
        scene_id = os.path.splitext(image_sf.file_name)[0]
    else:
        scene_id = getPgcBaseName(image_sf, attrs_dict, geom)
    attrs_dict[attrs.ACNSceneId] = scene_id

    # Preview URL and thumbnail path
    if attrs.ACNPreviewFilePath in fp_info.attr_names:
        if scene.hasKind(PREVIEW):
            prev_sf = scene.getSceneFile(PREVIEW)
            attrs_dict[attrs.ACNPreviewFilePath] = getPath(prev_sf.as_path, fp_info)
        else:
            attrs_dict[attrs.ACNPreviewFilePath] = ""

        if attrs.ACNPreviewUrl in fp_info.attr_names:
            if image_sf.vendor == DG:
                catalog_id = meta_dict.get(attrs.ACNCatalogId)
                attrs_dict[attrs.ACNPreviewUrl] = DG_PREVIEW_URL_FORMAT % catalog_id
            else:
                attrs_dict[attrs.ACNPreviewUrl] = ""


    ## Special cases handling

    # Anything specific to DG imagery goes in this section
    if image_sf.vendor == DG:

        # We perfer to get the order_id from the DG metadata, but sometimes must extract it
        # from the image filename instead
        if attrs.ACNOrderId in fp_info.attr_names:
            if not attrs_dict.get(attrs.ACNOrderId):
                attrs_dict[attrs.ACNOrderId] = image_sf.name_dict.get("oid")
                module_logger.warning(
                    "Could not get order id from metadata for %s, using filename instead" % \
                    image_sf.as_path)

        # Same thing for product level, get it from the filename if it isn't in the metadata
        if attrs.ACNProductLevel in fp_info.attr_names:
            if not attrs_dict.get(attrs.ACNProductLevel):
                attrs_dict[attrs.ACNProductLevel] = image_sf.name_dict.get("prod", "    ")[1:3]
                module_logger.warning(
                    "Could not get product level from metadata for %s, using filename instead" % \
                    image_sf.as_path)

            elif attrs_dict.get(attrs.ACNProductLevel).startswith("NO"):
                attrs_dict[attrs.ACNProductLevel] = None

    # Anything specific to GeoEye imagery goes in this section
    if image_sf.vendor == GE:

        # We need a catalog id, order id and product level for each image, if we cannot find
        # them in the metadata, then we need to get them from the file name

        # Product level
        if attrs.ACNProductLevel in fp_info.attr_names:
            if not attrs_dict.get(attrs.ACNProductLevel):
                attrs_dict[attrs.ACNProductLevel] = image_sf.name_dict.get("prod")
                module_logger.warning(
                    "Could not get product level from metadata for %s, using filename instead" % \
                    image_sf.as_path)

        # Catalog id
        if attrs.ACNCatalogId in fp_info.attr_names:
            if not attrs_dict.get(attrs.ACNCatalogId):
                attrs_dict[attrs.ACNCatalogId] = image_sf.name_dict.get("said")[3:]
                module_logger.warning(
                    "Could not get catalog id from metadata for %s, using filename instead" % \
                    image_sf.as_path)

        # Order id
        if attrs.ACNOrderId in fp_info.attr_names:
            if not attrs_dict.get(attrs.ACNOrderId):
                attrs_dict[attrs.ACNOrderId] = image_sf.name_dict.get("pnum")
                module_logger.warning(
                    "Could not get order id from metadata for %s, using filename instead" % \
                    image_sf.as_path)

        # Ground sample distance for GeoEye must be calculated
        if attrs.ACNGroundSampleDistance in fp_info.attr_names:
            ls = meta_dict.getNodeText("inputImageInfo/idsLineSpacing")
            if ls and len(ls) > 0:
                ls = ls[0]
            ps = meta_dict.getNodeText("inputImageInfo/idsPixelSpacing")
            if ps and len(ps) > 0:
                ps = ps[0]
            if ls and ps:
                attrs_dict[attrs.ACNGroundSampleDistance] = (float(ls) + float(ps)) / 2.0

    # Anything specific to IKONOS imagery goes in this section
    if image_sf.vendor == IK:

        # If we didn't get spectral type from the metadata, determine it from the filename
        if attrs.ACNSpectralType in fp_info.attr_names:
            if not attrs_dict.get(attrs.ACNSpectralType):
                spec_type = "Panchromatic" if "_pan_" in image_sf.file_name.lower() \
                    else "Multispectral"
                attrs_dict[attrs.ACNSpectralType] = spec_type

    return FootprintRecord(fpid, geom, attrs_dict)

def getPgcBaseName(scene_file, meta_dict, geom):
    """Determine what a file's PGC name would be"""

    pgc_name = None
    fmt, dct = None, {}

    if module_debug:
        module_logger.debug("Getting PGC name for file %s" % scene_file.as_path)

    dct["sensor"] = meta_dict.get(attrs.ACNSensor)
    dct["catalog_id"] = meta_dict.get(attrs.ACNCatalogId)
    if dct["catalog_id"] == "NO DATA" or dct["catalog_id"] is None:
        module_logger.error("Invalid catalog id for %s" % scene_file.as_path)
        raise RuntimeError("Invalid catalog id for %s" % scene_file.as_path)

    if scene_file.vendor == DG:
        #acqt = meta_dict.get(attrs.ACNAcquisitionTime)
        #if acqt:
        #    tenths = ("%06d" % acqt.microsecond)[0]
        #    fmt = PGC_NAME_DG
        #    if scene_file.is_dg_tiled:
        #        tenths = "0"
        #        fmt = PGC_NAME_DG_TILED
        #    dct["tenths"] = tenths
        #    dct["ts"] = acqt.strftime("%y%b%d%H%M%S").upper()
        #else:
        #    module_logger.error("Could not get acquisition time from metadata for %s" % scene_file.as_path)
        #    raise RuntimeError("Could not get acquisition time from metadata for %s" % scene_file.as_path)

        if scene_file.is_dg_tiled:
            ts = scene_file.name_dict.get("ts")
            tenths = "0"
            fmt = PGC_NAME_DG_TILED
            dct["tile"] = scene_file.name_dict.get("tile", "")

        else:
            acqt = meta_dict.get(attrs.ACNAcquisitionTime)
            if acqt:
                tenths = ("%06d" % acqt.microsecond)[0]
                fmt = PGC_NAME_DG
                ts = acqt.strftime("%y%b%d%H%M%S").upper()
            else:
                module_logger.error("Could not get acquisition time from metadata for %s" % scene_file.as_path)
                raise RuntimeError("Could not get acquisition time from metadata for %s" % scene_file.as_path)

        dct["tenths"] = tenths
        dct["ts"] = ts

    elif scene_file.vendor == GE:
        fmt = PGC_NAME_GE

    elif scene_file.vendor == IK:
        fmt = PGC_NAME_IK
        if geom:
            lat = geom.Centroid().GetY()
            hem = "S" if lat < 0.0 else "N"
            dct["lat"] = "%04d%s" % (math.trunc(abs(lat) * 100.0), hem)
        else:
            module_logger.error("NULL geometry found when handling %s" % scene_file.as_path)
            raise RuntimeError("NULL geometry found when handling %s" % scene_file.as_path)

    elif scene_file.vendor == NGA:
        pname = meta_dict.get(attrs.ACNNGAVendorImageId)
        if pname:
            ndct, vendor = None, None
            ext = os.path.splitext(scene_file.file_name)[1]
            for v, pat in [(DG, RAW_DG), (IK, RAW_IK_1)]:
                m = re.search(pat, pname+ext, re.I | re.X)
                if m:
                    ndct = m.groupdict()
                    vendor = v
                    break
            if ndct:
                scene_file.name_dict = ndct.copy()
                if vendor == DG:
                    fmt = PGC_NAME_DG
                    dct["tenths"] = "0"
                elif vendor == IK:
                    fmt = PGC_NAME_IK
                    if geom:
                        lat = geom.Centroid().GetY()
                        hem = "S" if lat < 0.0 else "N"
                        dct["lat"] = "%04d%s" % (math.trunc(abs(lat) * 100.0), hem)
                        if not ndct.get("tail"):
                            dct["tail"] = ""
                    else:
                        module_logger.error("NULL geometry found when handling %s" % scene_file.as_path)
                        raise ValueError("NULL geometry found when handling %s" % scene_file.as_path)
                else:
                    module_logger.error("Could not determine vendor for old-style NGA image %s" % scene_file.as_path)
                    raise ValueError("Could not determine vendor for old-style NGA image %s" % scene_file.as_path)
            else:
                module_logger.error("Could not match proxy_name for NGA image %s" % scene_file.as_path)
                raise ValueError("Could not match proxy_name for NGA image %s" % scene_file.as_path)
        else:
            module_logger.error("Did not find %s entry in metadata for %s" % (attrs.ACNNGAVendorImageId, scene_file.as_path))
            raise RuntimeError("Did not find %s entry in metadata for %s" % (attrs.ACNNGAVendorImageId, scene_file.as_path))

    else:
        module_logger.error("Unidentified vendor code %s for image file %s" % (scene_file.vendor, scene_file.as_path))
        raise ValueError("Unidentified vendor code %s for image file %s" % (scene_file.vendor, scene_file.as_path))

    ndct = scene_file.name_dict.copy()
    ndct.update(dct)
    ndct["ext"] = ""
    pgc_name = fmt % ndct

    return pgc_name
