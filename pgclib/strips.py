#!/usr/bin/env python

"""
Classes and functions for handling Strips
"""

import os
import logging

from osgeo import ogr
from osgeo import osr

from constants import *
from geometry import getGeometryAndRasterInfo
import metadata as metad
import attributes as attrs


NORTH_SRS = osr.SpatialReference()
NORTH_SRS.ImportFromEPSG(3413)  # North Polar Stereographic NSDIC Sea Ice
SOUTH_SRS = osr.SpatialReference()
SOUTH_SRS.ImportFromEPSG(3031)  # South Polar Stereographic
CENTER_SRS = osr.SpatialReference()
CENTER_SRS.ImportFromEPSG(3410)  # Cylindrical Equal Area


class Strip(object):

    ATTRIBUTES_LIST = (
        "sensor", "acquisition_time", "product_code", "catalog_id", "order_id",
        "parts_count", "vendor", "band_count", "strip_id", "is_tiled"
    )

    STRIP_ACN_LIST = (
        attrs.ACNSensor, attrs.ACNAcquisitionTime, attrs.ACNCatalogId,
        attrs.ACNOrderId
    )

    STRIP_VENDOR_NAME_DEFNS = attrs.getVendorNameDefnsFromAttributes(STRIP_ACN_LIST)

    def __init__(self, scenes=None):

        self.scenes_list = []
        self.attributes = {}
        self.geometries = {}
        self.extent = None
        self.sort_code = None
        self.is_valid = None

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)
        if self.debug:
            self.logger.debug("Strip called with Scenes list of length %d" % len(scenes))


        ## Verification stage

        # All the scenes must be complete
        bad_scenes = [scene.gid for scene in scenes if not scene.is_complete]
        if bad_scenes:
            self.logger.critical("Incomplete scenes: %s" % str(bad_scenes))
            raise RuntimeError("One or more incomplete Scenes -- cannot make Strip")

        # All scenes must have the same vendor
        vendors = {scene.vendor for scene in scenes}
        if len(vendors) > 1:
            self.logger.critical("Too many vendors: %s" % str(vendors))
            raise RuntimeError("Scene vendors must all match -- cannot make Strip")

        # All scenes must have the same strip_id, which also ensures they have the
        # same catalog_id, order_id and product_code
        strip_ids = {scene.strip_id for scene in scenes}
        if len(strip_ids) > 1:
            self.logger.critical("Too many strip_ids: %s" % str(strip_ids))
            raise RuntimeError("Scene strip_ids must all match -- cannot make Strip")

        self.scenes_list = scenes
        self.attributes["vendor"] = vendors.pop()
        self.attributes["strip_id"] = strip_ids.pop()
        self.attributes["catalog_id"], \
        self.attributes["order_id"], \
        self.attributes["product_code"] = self.attributes["strip_id"].split("|")

        # Set the sort code
        self.sort_code = "|".join((self.attributes.get("catalog_id", "None"),
                                   self.attributes.get("product_code", "None")))

        # Walk through this strip's scenes and determine strip attributes
        _attributes = {}
        for scene in self.scenes_list:

            # Get the metadata values we need
            meta_text = metad.getMetatext(scene)
            meta_dict = metad.MetadataDictionary(meta_text, self.vendor,
                                                 self.STRIP_VENDOR_NAME_DEFNS)
            if self.vendor == IK:
                ik_supp = metad.getIKMetadataSupplement(scene.imageSceneFile, meta_dict.tree)
                meta_dict.update(ik_supp)

            _attributes.setdefault("sensor", []).append(meta_dict[attrs.ACNSensor])
            _attributes.setdefault("acquisition_time", []).append(meta_dict[attrs.ACNAcquisitionTime])
            _attributes.setdefault("is_tiled", []).append(scene.is_dg_tiled)
            _attributes.setdefault("band_count", []).append(scene.raster_info["raster_band_count"])

            if scene.wkt:
                self.geometries[os.path.splitext(scene.imageSceneFile.file_name)[0]] = scene.wkt

        sensors = set(_attributes["sensor"])
        if len(sensors) > 1:
            self.logger.error("Too many sensors: %s" % str(sensors))
            raise RuntimeError("More than one sensor found -- cannot make Strip")
        self.attributes["sensor"] = sensors.pop()

        is_tiled = set(_attributes["is_tiled"])
        if len(is_tiled) > 1:
            self.logger.error("Cannot mix tiled and untiled scenes in a Strip")
            raise RuntimeError("Cannot mix tiled and untiled scenes in a Strip")
        self.attributes["is_tiled"] = is_tiled.pop()

        band_count = set(_attributes["band_count"])
        if len(band_count) != 1:
            self.logger.error("Different band counts in component scenes: %s" % str(band_count))
            raise RuntimeError("Differing band counts found -- cannot make Strip")
        self.attributes["band_count"] = int(band_count.pop())

        self.attributes["acquisition_time"] = min(_attributes["acquisition_time"])
        self.attributes["parts_count"] = len(self.scenes_list)

        # Create the combined extent
        geoms = [ogr.CreateGeometryFromWkt(wkt) for wkt in self.geometries.values() if wkt is not None]
        self.extent = getCombinedExtent(geoms)

        # Is this strip valid?
        self.is_valid = all([
            "None" not in self.sort_code, "None" not in self.strip_id,
            [attr is not None for attr in self.attributes],
            self.extent is not None
        ])

    @property
    def area(self):
        """Returns the area of the Strip extent in square meters"""

        # Must project into a Cartesian system to find area
        # North of 60 deg use EPSG 3413
        # South of -60 deg use EPSG 3031
        # Everywhere else use EPSG 3410
        lat = self.centroid[0]
        if lat >= 60.0:
            new_srs = NORTH_SRS
        elif lat <= -60.0:
            new_srs = SOUTH_SRS
        else:
            new_srs = CENTER_SRS

        # We may have to reproject the extent, so we use a clone rather
        # than change this instance's geometry
        _extent = self.extent.Clone()
        srs = _extent.GetSpatialReference()
        if not srs:
            # Doesn't have an SRS, assume a GeogCS of WGS84
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(4326)

        # If the Strip SRS isn't already one of the above systems, reproject
        if new_srs.IsSame(srs) != 1:
            ct = osr.CoordinateTransformation(srs, new_srs)
            _extent.Transform(ct)

        return _extent.Area()

    @property
    def centroid(self):
        """Returns the latitude, longitude of the strip extent centroid"""
        _centroid = (None, None)
        if self.extent is not None:
            _centroid = (self.extent.Centroid().GetY(), self.extent.Centroid().GetX())
        return _centroid

    @property
    def scenes(self):
        return self.scenes_list

    @property
    def is_tiled(self):
        return self.attributes.get("is_tiled")

    @property
    def strip_id(self):
        return self.attributes.get("strip_id")

    @property
    def sensor(self):
        return self.attributes.get("sensor")

    @property
    def acq_time(self):
        return self.attributes.get("acquisition_time")

    @property
    def product(self):
        return self.attributes.get("product_code")

    @property
    def catalog_id(self):
        return self.attributes.get("catalog_id")

    @property
    def order_id(self):
        return self.attributes.get("order_id")

    @property
    def parts(self):
        return self.attributes.get("parts_count")

    @property
    def vendor(self):
        return self.attributes.get("vendor")

    @property
    def bands(self):
        return self.attributes.get("band_count")

    def is_same(self, other):
        return all([self.sort_code == other.sort_code, self.bands == other.bands,
                    self.area == other.area, self.parts == other.parts])

    def __len__(self):
        return len(self.scenes_list)

    def __iter__(self):
        for scene in self.scenes_list:
            yield scene

    def __hash__(self):
        return hash(self.strip_id)

    def __lt__(self, other):
        return self.strip_id < other.strip_id

    def __le__(self, other):
        return self.strip_id <= other.strip_id

    def __eq__(self, other):
        return self.strip_id == other.strip_id

    def __ne__(self, other):
        return self.strip_id != other.strip_id

    def __gt__(self, other):
        return self.strip_id > other.strip_id

    def __ge__(self, other):
        return self.strip_id >= other.strip_id


class StripsCollection(object):
    """Container for a collection of Strips"""

    def __init__(self, strips=None, unique=False):

        self.strips_list = []
        self.is_unique = unique

        self.logger = logging.getLogger(__name__)

        if strips:
            self._loadStripsFromSequence(strips, unique)

    def addStrip(self, strp, unique):
        """Add a Strip object to the container"""

        if unique:
            if strp.strip_id not in self.strips_list:
                self.strips_list.append(strp)
            else:
                self.logger.error("Strip ID already exists in the collection: %s" % strp.strip_id)
                self.logger.error("Strip not added to collection")
        else:
            self.strips_list.append(strp)

    def getStrip(self, strip_id=None):
        """Fetch a specific Strip from collection"""
        return [strp for strp in self.strips_list if strp.strip_id == strip_id]

    def removeStrip(self, strp=None):
        """Remove a specific Strip from the collection"""
        if strp in self.strips_list:
            del self.strips_list[self.strips_list.index(strp)]
        else:
            self.logger.error("Cannot remove Strip %s from collection, does not exist" % strp.strip_id)

    def _loadStripsFromSequence(self, strips, unique):
        """Load container from a sequence of Strips"""
        for strp in strips:
            self.addStrip(strp, unique)

    def _loadStripsFromScenesCollection(self, scenes, unique=False):
        """Converts a sequence of Scenes into Strips and loads container"""
        strips_dict = sortScenesByStripId(scenes)
        self._loadStripsFromSequence([Strip(scns) for strip_id, scns in strips_dict.items()], unique)

    def __len__(self):
        return len(self.strips_list)

    def __iter__(self):
        for strp in sorted(self.strips_list):
            yield strp


### Functions

module_logger = logging.getLogger(__name__)
module_debug = module_logger.isEnabledFor(logging.DEBUG)

# Combine several geometries into one and return it
def getCombinedExtent(geoms):
    """Return the union of several geometries as a new geometry"""

    union = None

    for geom in geoms:
        if union is None:
            union = geom.Clone()
        else:
            union = union.Union(geom)

    return union

# Sort a collection of Scenes into strips
def sortScenesByStripId(scenes):
    """Return a dictionary of Scenes grouped as strips"""
    strips = {}
    for scene in scenes:
        strips.setdefault(scene.strip_id, []).append(scene)
    return strips
