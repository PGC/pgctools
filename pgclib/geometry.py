#!/usr/bin/env python

"""
Classes and functions related to rasters and geometries
"""

import os
import logging
import datetime
import re
import math
import sys

from osgeo import gdal
from osgeo import osr
from osgeo import ogr
from osgeo.gdalconst import GA_ReadOnly

from constants import *
from esriutils import *


### Functions

COUNTRY_BORDERS = os.path.join(os.path.dirname(sys.modules["pgclib"].__file__),
                               r"data/countries.shp")
WGS84 = osr.SpatialReference()
WGS84.ImportFromEPSG(4326)

module_logger = logging.getLogger(__name__)
module_debug = module_logger.isEnabledFor(logging.DEBUG)

def getGeometryAndRasterInfo(scene_file):
    """Open an image SceneFile as a GDAL dataset, extract metadata and create an extent polygon"""

    _geometry = None
    _raster_info = None
    _proj = None
    _geo_coords = []

    gdal.PushErrorHandler("CPLQuietErrorHandler")

    if module_debug:
        module_logger.debug("Getting GDAL info for %s" % scene_file.as_path)
        gdal.SetConfigOption("CPL_DEBUG", "ON")

    # We only need the raster headers, not the underlying raster data set
    gdal.SetConfigOption("NITF_OPEN_UNDERLYING_DS", "NO")
    gdal.SetConfigOption("GDAL_DISABLE_READDIR_ON_OPEN", "TRUE")

    # The SceneFile provided must be of kind IMAGERY
    if scene_file and scene_file.kind == IMAGE and os.path.isfile(scene_file.as_path):

        # Try opening raster as a gdal.DataSource
        _data_src = None
        try:
            _data_src = gdal.Open(scene_file.as_path, GA_ReadOnly)
            if module_debug:
                module_logger.debug("Opened %s as GDAL data source" % scene_file.as_path)
        except (RuntimeError, IOError, OSError) as err:
            _data_src = None
            module_logger.critical("Could not open %s as GDAL data source because %s" % (scene_file.as_path, err))
            raise

        if _data_src is not None:

            # If the raster has a projection get it, if not use the GCPs
            # NOTE: If the GCPs have a GEOGCS os WGS84 already, we will just use
            #       the coordinates without reprojection
            _proj = _data_src.GetProjectionRef()
            if not _proj:
                _proj = _data_src.GetGCPProjection()
                _geotr = gdal.GCPsToGeoTransform(_data_src.GetGCPs())
                using_gcps = True
            else:
                _geotr = _data_src.GetGeoTransform()
                using_gcps = False

            if _geotr:

                if module_debug:
                    module_logger.debug("Projection: %s" % str(_proj))
                    module_logger.debug("GeoTransform: %s" % str(_geotr))

                # Create a spatial reference and see if it's already WGS84
                _spat_ref = osr.SpatialReference(_proj)
                if using_gcps and _spat_ref.IsSame(WGS84) == 1:

                    # If so, just use the coords as is
                    dct = dict([(gcp.Id, (gcp.GCPX, gcp.GCPY)) for gcp in _data_src.GetGCPs()])
                    if dct.get("UpperLeft"):
                        keys = ("UpperLeft", "UpperRight", "LowerRight", "LowerLeft", "UpperLeft")
                    elif dct.get("1"):
                        keys = ("1", "2", "3", "4", "1")
                    else:
                        module_logger.error("Unrecognized GCP keys: %s, in file: %s" % \
                                            (str(dct), scene_file.as_path))
                        raise RuntimeError("Unrecognized GCP keys: %s, in file: %s" % \
                                           (str(dct), scene_file.as_path))
                    _geo_coords = [dct.get(key) for key in keys]
                    if module_debug:
                        module_logger.debug("Long/Lat (from GCP) coordinates: %s" % str(_geo_coords))

                # Not in WGS84, we need to transform them from pixel coordinates
                else:
                    max_x = _data_src.RasterXSize + 0.5
                    max_y = _data_src.RasterYSize + 0.5
                    pixel_coords = ((0, 0), (max_x, 0), (max_x, max_y), (0, max_y), (0, 0))
                    if module_debug:
                        module_logger.debug("Pixel coordinates: %s" % str(pixel_coords))

                    # Convert pixel coordinates to lat/long
                    for X, Y in pixel_coords:
                        lng = (_geotr[0] + _geotr[1] * X + _geotr[2] * Y) + (_geotr[1] / 2.0)
                        lat = (_geotr[3] + _geotr[4] * X + _geotr[5] * Y) + (_geotr[5] / 2.0)
                        _geo_coords.append((lng, lat))
                    if module_debug:
                        module_logger.debug("Long/Lat coordinates: %s" % str(_geo_coords))

                # Make an ogr.Geometry object from the geo_coords as WKT
                wkt = "POLYGON (( %s ))" % ", ".join(["%s %s" % point for point in _geo_coords])
                _geometry = ogr.CreateGeometryFromWkt(wkt)
                if module_debug:
                    module_logger.debug("Initial geometry -> %s" % str(_geometry))

                # If the spatial reference wasn't WGS84, we need to transform the geometry
                if _spat_ref.IsSame(WGS84) != 1:
                    _coord_tr = osr.CoordinateTransformation(_spat_ref, WGS84)
                    if _coord_tr.this is not None:
                        _geometry.Transform(_coord_tr)
                        if module_debug:
                            module_logger.debug("Transformed geometry -> %s" % str(_geometry))
                    else:
                        module_logger.warning("Could not transform into WGS84: %s - skipping" % \
                                              scene_file.as_path)
                        return (None, None)

                if module_debug:
                    module_logger.debug("Final lat/long coordinates: %s" % str(_geo_coords))

                # Extract the coordinates from the geometry and validate their ranges
                mt = re.findall(r"-?\d+\.\d+", _geometry.ExportToWkt())
                _geo_coords = [(float(lng), float(lat)) for (lng, lat) in [mt[i:i+2] \
                                                            for i in range(0, len(mt), 2)]]
                _valid = not any([abs(lng) > 180 or abs(lat) >= 90 for (lng, lat) in _geo_coords])

                if _valid:

                    # Define the raster_info dictionary
                    _raster_info = {
                        "raster_band_count": _data_src.RasterCount,
                        "raster_rows": _data_src.RasterYSize,
                        "raster_columns": _data_src.RasterXSize,
                        "raster_description": _data_src.GetFileList()[0],
                        "centroid_latitude": _geometry.Centroid().GetY(),
                        "centroid_longitude": _geometry.Centroid().GetX()
                    }

                    # Add the country code
                    _raster_info["country_code"] = getCountryCode(_geometry)

                    if module_debug:
                        module_logger.debug("Raster_info dict: %s" % str(_raster_info))

                else:

                    # Ooops, we generated invalid coordinates, probably a problem image
                    module_logger.warning("Coordinate transformation produced invalid angles for %s" % scene_file.as_path)
                    return (None, None)  # We don't want to halt processing for this, just skip this image

            else:
                module_logger.warning("Could not get geotransform for %s" % scene_file.as_path)
                return (None, None)

        else:
            module_logger.error("Could not open data source at %s" % scene_file.as_path)
            raise RuntimeError("Could not open data source at %s" % scene_file.as_path)

    else:
        if scene_file:
            err_msg = "Imagery file is not an image file: %s" % scene_file.as_path
        else:
            err_msg = "Imagery file does not exist: SceneFile object is None"
        module_logger.error(err_msg)
        raise RuntimeError(err_msg)

    gdal.PopErrorHandler()

    return (_geometry, _raster_info)

# Find NGA country code by geometry intersection rather than relying on dubious metadata
def getCountryCode(geom):
    """Check an extent geometry against country borders to find country code"""

    country_code = None

    if geom is not None and geom.IsValid():
        country_shp = OgrInterfaceForESRI(OGR_SHAPEFILE_DRIVER, COUNTRY_BORDERS)
        country_shp.layer.SetSpatialFilter(geom)
        if country_shp.layer.GetFeatureCount() > 0:
            country_feat = country_shp.layer.GetNextFeature()
            country_code = country_feat["NGA_C_CODE"]
        else:
            country_code = "YY"
    else:
        module_logger.error("Cannot find country code for NULL or invalid geometry")

    return country_code

# Return the embedded NITF metadata needed for determining the NGA name
def getWarpPrefixInfo(imgsf):
    """Return NITF_STDIDC_PASS,NITF_STDIDC_OP_NUM and NITF_STD_MISSION from NITF metadata"""
    _pass, _op_num, _mission = None, None, None
    if imgsf and imgsf.file_name.lower().endswith(".ntf"):
        gdal.PushErrorHandler("CPLQuietErrorHandler")
        ds = gdal.Open(imgsf.as_path)
        gdal.PopErrorHandler()
        if ds is not None:
            md = ds.GetMetadata()
            _pass = md.get("NITF_STDIDC_PASS")
            _op_num = md.get("NITF_STDIDC_OP_NUM")
            _mission = md.get("NITF_STDIDC_MISSION")
    return (_pass, _op_num, _mission)
