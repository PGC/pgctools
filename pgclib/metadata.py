#!/usr/bin/env python

"""
Classes and functions for handling metadata
"""

import os
import re
import datetime
import logging

from xml.etree import cElementTree as ET
from dateutil import parser as DP

from constants import *
from scenefiles import getFileContents, extractFileContentsFromTar
from attributes import VENDOR_METADATA_NAMES, getAttributeAsValue, ATTRIBUTE_COMMON_NAMES


__all__ = [
    "MetadataDictionary", "getMetatext", "getXmlAsETree", "getGEMetatextAsETree",
    "getIKMetatextAsETree", "getIKMetadataSupplement"
]


class MetadataDictionary(dict):
    """Build a metadata dictionary from vendor metadata using a vendor definitions list"""

    def __init__(self, meta_src, vendor, vn_defns_list):

        super(MetadataDictionary, self).__init__()
        self._tree = None
        self._vendor = vendor

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)

        # Were we passed a string or a SceneFile?
        meta_text = None
        if isinstance(meta_src, basestring):
            meta_text = meta_src
            if self.debug:
                self.logger.debug("Called with text -> len(%d):" % len(meta_text))

        # A SceneFile, so extract the metadata text first
        elif meta_src:
            meta_text = getFileContents(meta_src)
            if self.debug:
                self.logger.debug("Called with meta_sf -> %s" % meta_src.as_path)

        if meta_text:
            try:
                self.createDictionary(meta_text, vn_defns_list)
            except ET.ParseError as err:
                if not isinstance(meta_src, basestring):
                    src = "for " + meta_src.as_path
                else:
                    src = ""
                self.logger.critical("Could not parse the metatext %s" % src)

            if self.debug:
                self.logger.debug("Created metadata dictionary with %d entries" % len(self))

        else:
            self.logger.critical("Unable to obtain metatext to create dictionary")
            raise RuntimeError("Unable to obtain metatext to create dictionary")

    @property
    def tree(self):
        return self._tree

    @property
    def vendor(self):
        return self._vendor

    def __hash__(self):
        acqt = self.get("acquisition_time")
        ms = "%06d" % acqt.microsecond
        return hash(self.get("sensor", "")+acqt.strftime("%Y%m%dT%H%M%S")+ms)

    def getNodeText(self, xpath):
        """Perform a direct search of the metadata eTree for a specific xpath"""
        return self.tree.findtext(".//%s" % xpath)

    def createDictionary(self, meta_text, vn_defns_list):
        """Build the metadata dictionary from the contents of a Scene's metadata file"""

        # First we want the metadata text to be in an XML format, some platforms
        # only provide plain text; convert those as necessary
        if self.vendor in [DG, NGA]:
            self._tree = getXmlAsETree(meta_text)
        elif self.vendor == GE:
            self._tree = getGEMetatextAsETree(meta_text)
        elif self.vendor == IK:
            self._tree = getIKMetatextAsETree(meta_text)
        else:
            self.logger.critical("Unknown vendor for metadata text: %s" % self.vendor)
            raise ValueError("Unknown vendor for metadata text: %s" % self.vendor)

        # Now build a dictionary from the eTree
        if self.tree:

            # First, create attribute definitions iterable and create priority dictionary
            attr_defns = [vnd for vnd in vn_defns_list if vnd.vendor == self.vendor]
            priority_dict = {}
            for attr_defn in attr_defns:
                priority_dict.setdefault(attr_defn.cname, {})[attr_defn.xpath] = \
                attr_defn.priority

            if self.debug:
                self.logger.debug("Priority_dict defined: %r" % priority_dict)

            # Temporary dictionary before definition priority handling
            xp_dict = {}

            # Start searching the eTree for each definition
            for attr_defn in attr_defns:

                # We need to check what kind of attribute we're searching for; if it's
                # a 'list' we need to find all relevant nodes, otherwise the only (or
                # first) node is sufficient
                if attr_defn.ctype != "list":
                    meta_value = self.tree.findtext(".//%s" % attr_defn.xpath)
                else:
                    meta_value = "|".join([node.text for node in self.tree.findall(".//%s" % attr_defn.xpath)])

                if self.debug:
                    self.logger.debug("Metadata value found %s -> %r" % (attr_defn.xpath, meta_value))

                # In order to support priorities within metadata definitions (where multiple
                # key/value pairs translate to the same common name), we must first render
                # a dictionary keyed with xpaths; priority reconciliation occurs in a
                # secondary step

                if meta_value is not None:

                    # If the definition has a filter, apply it
                    if attr_defn.filter:
                        matcher = re.search(eval(attr_defn.filter), meta_value, re.I)
                        if matcher:
                            meta_value = matcher.group(1)
                            if self.debug:
                                self.logger.debug("Applied filter: %s with result: %s" % \
                                                  (attr_defn.filter, meta_value))
                        else:
                            module_logger.warning("Filter failure: filter=%s meta_value=%s" % \
                                                  (attr_defn.filter, meta_value))

                    # Convert string to actual value and make any adjustments necessary
                    attr_val = getAttributeAsValue(attr_defn.cname, meta_value)

                    # Any form of 999 in cloud cover is NODATA (None); if the value is not already
                    # in the range 0 to 1, divide by 100
                    if attr_defn.cname == "cloud_cover" and attr_val is not None:
                        if abs(attr_val) == 999 or attr_val < 0:
                            attr_val = None
                        if self.vendor in [IK, NGA] or (1.0 < attr_val <= 100.0):
                            attr_val /= 100.0
                        if attr_val > 100.0:
                            attr_val = 1.0

                    # Many labels are used to indicate raster file is a NITF, they all have
                    # the text 'nitf' somewhere in them; NCD also means NITF
                    if attr_defn.cname == "raster_file_format":
                        if "nitf" in attr_val.lower() or attr_val.lower() == "ncd":
                            attr_val = "NITF"

                    # Change the spectral_type label to something human-readable and regularized
                    if attr_defn.cname == "spectral_type":
                        if attr_val.lower() in ["multi", "ms1", "msi", "rgb"]:
                            attr_val = "Multispectral"
                        elif attr_val.lower() in ["p", "pan"]:
                            attr_val = "Panchromatic"
                        elif attr_val.lower() in ["pan-sharpened msi"]:
                            attr_val = "Pan-sharpened MS"

                    # Fix sensor label weirdnesses
                    if attr_defn.cname == "sensor":
                        if attr_val.lower() in ["ikonos-2"] or self.vendor == IK:
                            attr_val = "IK01"
                        elif attr_val.lower() in ["ov-5"]:
                            attr_val = "GE01"
                        elif attr_val.lower() in ["ov-3"]:
                            attr_val = "OV03"
                        elif attr_val.lower() in ["qb01"]:
                            attr_val = "QB02"

                    # IKONOS product level is not listed in the metadata so just set it
                    if attr_defn.cname == "product_level" and self.vendor == IK:
                        attr_val = "geo"

                else:

                    # If no metadata value was found for this common name/xpath, just
                    # put None in the xp_dict
                    attr_val = None

                # priority_dict[cname] = {xpath:priority, xpath:priority, ...}
                # xp_dict[cname] = [(xpath, attr_val, priority), (xpath, attr_val, priority), ...]
                xp_dict.setdefault(attr_defn.cname, []).append((attr_defn.xpath, attr_val,
                                            priority_dict[attr_defn.cname][attr_defn.xpath]))

            # Convert xpath dictionary to the metadata (common name) dictionary, taking
            # definition priorities into account
            for cname in sorted(xp_dict.iterkeys()):

                # Any common name with only one value doesn't have any priority issues,
                # otherwise find the highest priority, existing metadata value
                if len(xp_dict[cname]) > 1:
                    try:
                        priority, xpath, attr_val = max([(pr, xp, av) for xp, av, pr in \
                            xp_dict[cname] if av is not None])
                    except ValueError:
                        xpath, attr_val, priority = xp_dict[cname][0]

                else:
                    xpath, attr_val, priority = xp_dict[cname][0]

                self[cname] = attr_val
                if self.debug:
                    self.logger.debug("Added %s with value %r" % (cname, attr_val))

        else:
            self.logger.error("Could not derive eTree from metadata text")
            raise RuntimeError("Could not derive eTree from metadata text")


### Functions

module_logger = logging.getLogger(__name__)
module_debug = module_logger.isEnabledFor(logging.DEBUG)

def getMetatext(scene, secondary=False):
    """Extract metadata text from the metadata file even if it's embedded in a TAR"""

    metatext = None
    kind = METADATA if not secondary else METADATA_SECONDARY

    if scene.hasKind(kind):
        scene_file = scene.getSceneFile(kind)
        metatext = getFileContents(scene_file)

    elif scene.hasKind(ARCHIVE):

        scene_file = scene.getSceneFile(ARCHIVE)
        if module_debug:
            module_logger.debug("tarsf -> %s" % scene_file.as_path)
        members = getFileContents(scene_file)
        if module_debug:
            module_logger.debug("members -> %s" % str(members))
        targets = set()

        if scene.imageSceneFile.vendor == DG:
            targets.add(os.path.splitext(scene.imageSceneFile.proxy_name)[0].lower() + ".xml")
            if scene.imageSceneFile.is_dg_tiled:
                targets.add(os.path.splitext(scene.imageSceneFile.getDGTiledProxyName())[0].lower() + ".xml")
                # Total hack to get around the R01/R1 problem temporarily
                tcode = scene.imageSceneFile.name_dict.get("tile")
                if tcode and "R0" in tcode:
                    targets.add(re.sub(r"r0", "r",
                        os.path.splitext(scene.imageSceneFile.getDGTiledProxyName())[0].lower() + ".xml"))
        elif scene.imageSceneFile.vendor == GE:
            targets.add(os.path.splitext(scene.imageSceneFile.proxy_name)[0].lower() + ".pvl")
        elif scene.imageSceneFile.vendor == IK:
            po = scene.imageSceneFile.name_dict.get("po", "XXXXXX")
            targets.add("po_%s_metadata.txt" % po)
        elif scene.imageSceneFile.vendor == NGA:
            for tinfo in members:
                if tinfo.name.lower().endswith(".xml"):
                    targets.add(os.path.basename(tinfo.name).lower())
                    break
        else:
            module_logger.error("Unrecognized vendor code: %s" % scene.imageSceneFile.vendor)

        if module_debug:
            module_logger.debug("targets -> %s" % str(targets))
        for tinfo in members:
            if os.path.basename(tinfo.name).lower() in targets:
                metatext = extractFileContentsFromTar(scene_file, tinfo)
                break

    return metatext

def getXmlAsETree(metatext=None):
    """Put any metatext already in XML format into an eTree"""

    root = ET.Element("root")
    if metatext:
        try:
            root = ET.fromstring(metatext)
        except ET.ParseError as err:
            module_logger.critical("Could not parse metatext because %s" % err)
            raise

    return root

def getGEMetatextAsETree(metatext=None):
    """Convert GE plain-text metadata into an XML eTree"""

    root = None

    if len(re.findall("END;", metatext)) != 1:
        module_logger.error("Invalid metatext (too many END; marks)")
    else:
        if metatext and (metatext.startswith("copyright") and metatext.endswith("END;\n")):
            metatext = metatext.split("\n")
            if module_debug:
                module_logger.debug("getGEMetatextAsETree called, processing %d lines" % len(metatext))

            # GE metadata patterns for tag/data and tag-only (or multiline) entries
            gepat1 = re.compile(r'(?P<tag>\w+) = "?(?P<data>.*?)"?;', re.I)
            gepat2 = re.compile(r"(?P<tag>\w+) = ", re.I)

            # Tags that are groups with attributes
            group_tags = {
                "aoiGeoCoordinate":"coordinateNumber",
                "aoiMapCoordinate":"coordinateNumber",
                "bandSpecificInformation":"bandNumber"
            }

            # Make the root node and initialize pointers
            root = ET.Element("root")
            parent = None    # Parent to current node
            current = root   # Current node
            nstack = []      # Node stack
            mlstr = False    # multi-line string flag

            # Begin processing
            for line in metatext:

                # mlstr is true when working on a multi-line string
                if mlstr:

                    # If it's not the end of the multiline entry just keep concatenating
                    if not line.strip() == ");":
                        data += line.strip()

                    # We have it all, create the new node and update pointers
                    else:
                        data += line.strip()
                        child = ET.SubElement(current, tag)
                        child.text = data
                        mlstr = False
                        if module_debug:
                            module_logger.debug("Found end of multi-line node, tage: %s" % tag)

                # handle tag/value pairs and groups
                m1 = gepat1.search(line)
                if m1:
                    tag = m1.group("tag").strip()
                    data = m1.group("data").strip()
                    if module_debug:
                        module_logger.debug("Found tag: %s with data: %s" % (tag, data))

                    # END; means end of metadata, so terminate processing
                    if tag == "END;":
                        break

                    # handle group begins (data = group name)
                    if tag == "BEGIN_GROUP":
                        if module_debug:
                            module_logger.debug("BEGIN GROUP, parent: %r, current: %r" % (parent, current))

                        # Start a new group of nodes
                        child = ET.SubElement(current, data)
                        if parent is not None:
                            nstack.append(parent)
                        parent, current = current, child

                    # close out a group and adjust pointers
                    elif tag == "END_GROUP":
                        if module_debug:
                            module_logger.debug("END GROUP, parent: %r, current: %r" % (parent, current))
                        current = parent
                        parent = nstack.pop() if nstack else None

                    # Not a group start/end, just add node
                    else:

                        # Check if this tag should have an attribute
                        if current.tag in group_tags and tag == group_tags[current.tag]:
                            current.set(tag, data)
                            if module_debug:
                                module_logger.debug("Current %r set to tag: %s, data: %s" % (current, tag, data))

                        # If not, just add as a regular child node
                        else:
                            child = ET.SubElement(current, tag)
                            child.tag = tag
                            child.text = data
                            if module_debug:
                                module_logger.debug("Child %r created with tag: %s, data: %s" % (child, tag, data))

                # A tag with an '=' but no data means the start of a multiline entry
                else:
                    m2 = gepat2.search(line)
                    if m2:
                        tag = m2.group("tag")
                        data = ""
                        mlstr = True
                        if module_debug:
                            module_logger.debug("Found start of multi-line node, tag: %s" % tag)

    return root

def getIKMetatextAsETree(metatext=None):
    """Convert IKONOS plain-text metadata to an XML eTree"""

    root = None
    if metatext:
        metatext = metatext.split("\n")
        if module_debug:
            module_logger.debug("getIKMetadataAsXml called, processing %d lines" % len(metatext))

        # IKONOS metadata text patterns
        ikpat1 = re.compile(r"(?P<tag>.+?): (?P<data>.+)", re.I)
        ikpat2 = re.compile(r"(?P<tag>[a-zA-Z ()]+)", re.I)

        # Tag/data pairs that need to use data as an attribute and are 2L type nodes
        tags_attr = {
            "Coordinate":"id",
            "Source_Image_ID":"id",
            "Component_ID":"id",
            "Tile_ID":"id"
        }

        # Tags for nodes that should always be children of root
        tags_1L = [
            "Product_Order_Metadata",
            "Source_Image_Metadata",
            "Product_Space_Metadata",
            "Product_Component_Metadata"
        ]

        # Tags for nodes that should always be children of the current 1L node
        tags_2L = [
            "Product_Order_Area_(Geographic_Coordinates)",
            "Product_Order_Area_(Map_Coordinates_in_Map_Units)",
            "Source_Image_ID", "Product_MBR_Geographic_Coordinates",
            "Product_Map_Coordinates_(in_Map_Units)", "Component_ID",
            "Tile_ID"
        ]

        # Define top-level tags, that always start a new group (1st or 2nd level), and
        # special tags that are groups but may appear more than once (and have attributes)
        tags_toplevs = ["root"] + tags_1L + tags_2L
        tags_1L_spec = ["root", "Source_Image_ID", "Component_ID", "Tile_ID"] + tags_1L

        root = ET.Element("root")
        nstack = [root]  # stack of parent nodes

        # Adds a new child node to the last-in parent node on the stack
        def makeChild(tag, data, attr=None):
            child = ET.SubElement(nstack[-1], tag)
            if attr:
                child.set(attr, data)
                if module_debug:
                    module_logger.debug("Adding node with attribute: tag: %s, attr: %s" %(attr, data))
            else:
                child.text = data
                if module_debug:
                    module_logger.debug("Adding node: tag: %s, data: %s" % (tag, data))
            return child

        # Interpret the metadata text line by line
        for line in metatext:

            # Skip dividers and blank lines
            if len(line.strip()) == 0 or line[0] in ["=", "-"]:
                continue

            # Keep track of how many spaces a line may start with
            spclen = len(line) - len(line.lstrip(" "))
            tag, data = "", ""

            # Try patterns to find tag/data pairs, or tag-only group names
            m1 = ikpat1.search(line.strip())
            m2 = ikpat2.search(line.strip())

            # Define tag and data, convert any spaces to underscores and slashes to
            # plus signs to avoid XML XPath problems; skip unmatched lines
            if m1:
                tag = m1.group("tag").strip().replace(" ", "_").replace("/", "+")
                data = m1.group("data").strip()
            elif m2:
                tag = m2.group("tag").strip().replace(" ", "_").replace("/", "+")
                data = ""
            else:
                if module_debug:
                    module_logger.debug("No match for line: %s" % line.strip())
                continue  # Line fits no useful pattern so skip

            # Try to determine what sort of tag/data set we have found

            # A new 1L tag ends the old group and resets the stack
            if tag in tags_1L:
                while nstack[-1].tag != "root":
                    nstack.pop()
                child = makeChild(tag, data)
                nstack.append(child)

            # Tags in this group must be children of the current 1L node; some of
            # them should also use their data field as attributes
            elif tag in tags_2L:
                while nstack[-1].tag not in tags_1L and nstack[-1].tag != "root":
                    nstack.pop()

                # Tag has a attribute data value, so make it an attribute of this node
                if tag in tags_attr:
                    child = makeChild(tag, data, attr=tags_attr[tag])

                # Otherwise just add it as usual
                else:
                    child = makeChild(tag, data)

                nstack.append(child)

            # Special handling for a coordinates group; if this is the
            # end of a coordinate list go back up to a 1L or 2L level
            elif nstack[-1].tag == "Coordinate" and spclen == 0:
                while nstack[-1].tag not in tags_1L_spec:
                    nstack.pop()

                child = makeChild(tag, data)

            # No data component and not 1L or 2L, so add another level to tree
            elif not data:
                child = ET.SubElement(nstack[-1], tag)
                nstack.append(child)

            # Tags with data that need to be parent nodes and use their data
            # as an attribute (like 'Coordinate')
            elif tag in tags_attr:
                if nstack[-1].tag == tag:
                    nstack.pop()
                child = makeChild(tag, data, attr=tags_attr[tag])
                nstack.append(child)

            # Doesn't fit any special case, just add as a regular child node
            else:
                if spclen == 0:
                    while nstack[-1].tag not in tags_toplevs:
                        nstack.pop()

                child = makeChild(tag, data)

    return root

# Special function to extract supplemental metadata about IKONOS scenes, required to
# match Source Image ID, and Component ID or Tile ID sections with a specific image

def getIKMetadataSupplement(image_sf, tree):
    """Match supplemental IKONOS metadata to specific source image"""

    # Requires an image SceneFile and the metadata text as an eTree; returns a dictionary
    # designed to be added on to the standard metadata dictionary

    # Handling varies for RAW versus RENAMED mode

    dct = {}

    if module_debug:
        module_logger.debug("Entering getIKMetadataSupplement")
        module_logger.debug("Image file: %s" % image_sf.as_path)

    if image_sf:

        # Find all the Source Image IDs and Component IDs in the eTree
        siids = tree.findall(".//Source_Image_ID")
        cids = tree.findall(".//Component_ID")

        # If no Component IDs were found, try Tile IDs
        if not cids:
            cids = tree.findall(".//Tile_ID")

        if module_debug:
            module_logger.debug("Found %d siids and %d cids" % (len(siids), len(cids)))

        if siids and cids:

            # In RAW mode we match the Component ID and then find the Source Image ID
            if image_sf.mode == RAW:
                imgs = set([image_sf.proxy_name.lower()])
                cid = None
                for node in cids:
                    fnames = node.findtext("Component_File_Name")
                    if not fnames:
                        fnames = node.findtext("Tile_File_Name")
                    if imgs.intersection(set(fnames.split())):
                        cid = node
                        break
                else:
                    return {}  # No luck, return nothing

                # Matching up Component/Tile ID to Source Image ID is via the Product Image ID
                cpiid = cid.findtext("Product_Image_ID")
                if cpiid:
                    siid = None
                    for node in siids:
                        spiid = node.findtext("Product_Image_ID")
                        if spiid and spiid == cpiid:
                            siid = node
                            break
                    else:
                        return {}

                else:
                    return {}

            # In RENAMED mode we match the Source Image ID and then find the Component ID
            elif image_sf.mode == RENAMED:

                # All bands of a renamed IKONOS scene have the same SIID, so any image
                # filename will work for matching
                siid = None
                imgsiid = "%(ts)s%(siid)s" % image_sf.name_dict
                for node in siids:
                    nid = node.get("id")  # siid is an attribute
                    if nid and nid == imgsiid:
                        siid = node
                        break
                else:
                    return {}

                # Again, the Product Image ID bridges between Source Image ID and Component ID
                spiid = siid.findtext("Product_Image_ID")
                if spiid:
                    cid = None
                    for node in cids:
                        cpiid = node.findtext("Product_Image_ID")
                        if cpiid and cpiid == spiid:
                            cid = node
                            break
                    else:
                        return {}

                else:
                    return {}

            dct["Source_Image_ID"] = [(n.tag, n.text) for n in siid.iter() if n.text]
            dct["Source_Image_ID"].append(("Source_Image_ID", siid.get("id")))
            dct["Component_ID"] = [(n.tag, n.text) for n in cid.iter() if n.text]
            dct["Component_ID"].append(("Component_ID", cid.get("id")))

    # Now that we've found a Source Image ID/Component (or Tile) ID match, we must locate all
    # the IKONOS per-image items in the metadata and add them to the dictionary

    newdct = {}
    if dct:
        suppd = dict(dct["Source_Image_ID"])
        suppd.update(dict(dct["Component_ID"]))

        # Calculate a generic ground sample distance from pan/ms specific values
        if "_pan_" in image_sf.file_name and "Pan_Along_Scan" in suppd:
            newdct["ground_sample_distance"] = sum([float(re.search(r"(\d+\.\d+)\smeters",
                    suppd[k], re.I).group(1)) for k in ["Pan_Along_Scan", "Pan_Cross_Scan"]]) / 2.0
        elif "MS_Along_Scan" in suppd:
            newdct["ground_sample_distance"] = sum([float(re.search(r"(\d+\.\d+)\smeters",
                    suppd[k], re.I).group(1)) for k in ["MS_Along_Scan", "MS_Cross_Scan"]]) / 2.0
        elif "Along_Scan" in suppd:
            newdct["ground_sample_distance"] = sum([float(re.search(r"(\d+\.\d+)\smeters",
                    suppd[k], re.I).group(1)) for k in ["Along_Scan", "Cross_Scan"]]) / 2.0

        # Add an entry for all IKONOS metadata values found
        for vendor_defn in VENDOR_METADATA_NAMES:
            if vendor_defn.vendor != IK:
                continue

            meta_value = suppd.get(vendor_defn.vname)

            if meta_value:

                # Some metadata values have to be 'filtered' to remove unit labels, etc.
                if vendor_defn.filter:
                    matcher = re.search(eval(vendor_defn.filter), meta_value, re.I)
                    if matcher:
                        meta_value = matcher.group(1)
                    else:
                        module_logger.warning("Filter failure: filter=%s meta_value=%s" % (vendor_defn.filter, meta_value))

                newdct[vendor_defn.cname] = getAttributeAsValue(vendor_defn.cname, meta_value)

        # Special cases
        if newdct.get("sensor", "").lower() in ["ikonos-2"]:
            newdct["sensor"] = "IK01"

    return newdct
