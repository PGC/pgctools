#!/usr/bin/env python

"""
General file utilities
"""

import os
import sys
import fnmatch
import hashlib


PFIX = "\\\\?\\"   # Windows pathname length hack

GLUSTER_PREFIX = r"/mnt/agic/storage00/agic"
ESCI_WINDOWS_PREFIX = r"V:/pgc/agic"

__all__ = [
    "getFilesByType", "getMD5Checksum", "getFixedPath", "translatePath"
]


def getFilesByType(root, patterns='*', single_level=False, yield_folders=False):
	"""
	Search a folder hierarchy for files that match the desired pattern(s) and
	return their names, including folders if yield_folders is True
	"""

	# Patterns are of form: *.ext;*.ext
	patterns = patterns.split(';')
	for path, subdirs, files in os.walk(root):
		if yield_folders:
			files.extend(subdirs)
		files.sort()
		for name in files:
			for pattern in patterns:
				if fnmatch.fnmatch(name, pattern):
					yield os.path.join(path, name)
					break
		if single_level:
			break

def getMD5Checksum(path, blocksz=65536):
	"""Generate an MD5 checksum for given file"""
	checksum = None

	try:
		file_handle = open(path, "rb")
	except (IOError, OSError) as err:
		file_handle = None

	if file_handle:
		buffer = file_handle.read(blocksz)  # Get first block from file
		while len(buffer) > 0:
			hashlib.md5().update(buffer)
			buffer = file_handle.read(blocksz)
			checksum = hashlib.md5().digest()

	return checksum

def getFixedPath(in_path):
    """Add Python hack prefix to Windows path if necessary"""
    out_path = in_path

    if sys.platform == "win32":

        # If the path as is doesn't exist...
        if not os.path.isfile(in_path) and not os.path.isdir(in_path):

            # ...try it with the hack prefix
            if os.path.isfile(PFIX+in_path) or os.path.isdir(PFIX+in_path):
                out_path = PFIX + in_path

    return out_path

def translatePath(in_path, g2w=False, w2g=False):
    """Translate a path between PGC server and PGC Windows"""
    out_path = in_path

    if g2w:
        out_path = "".join((ESCI_WINDOWS_PREFIX, in_path.split(GLUSTER_PREFIX)[-1]))
    elif w2g:
        out_path = "".join((GLUSTER_PREFIX, in_path.split(ESCI_WINDOWS_PREFIX)[-1]))

    return out_path
