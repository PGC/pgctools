#!/usr/bin/env python

"""
Classes and functions for defining and handling footprint attributes
"""

import datetime
import logging
from collections import namedtuple
from dateutil import parser as DP

from constants import *


__all__ = [
    "getVendorNameDefnsFromAttributes", "getAttributeAsValue"
]


### Common metadata names to relate different vendor names and different
### footprint media types (Shapefile, File GDB, database, etc.)

# PGC Attribute Common Names
ACNSensor = "sensor"
ACNSceneId = "scene_id"
ACNStripId = "strip_id"
ACNStatus = "scene_status"
ACNCatalogId = "catalog_id"
ACNOrderId = "order_id"
ACNAcquisitionTime = "acquisition_time"
ACNProductLevel = "product_level"
ACNRasterBandCount = "raster_band_count"
ACNRasterRows = "raster_rows"
ACNRasterColumns = "raster_columns"
ACNPixelDepth = "raster_pixel_depth"
ACNFileFormat = "raster_file_format"
ACNCloudCover = "cloud_cover"
ACNSunElevation = "sun_elevation_angle"
ACNScanDirection = "scan_direction"
ACNOffNadir = "off_nadir_angle"
ACNGroundSampleDistance = "ground_sample_distance"
ACNReferenceHeight = "reference_height"
ACNExposureDuration = "exposure_duration"
ACNLineRate = "line_rate"
ACNSpectralType = "spectral_type"
ACNCountryCode = "country_code"
ACNCentroidLatitude = "centroid_latitude"
ACNCentroidLongitude = "centroid_longitude"
ACNOriginalFileName = "original_file_name"
ACNOriginalFilePath = "original_file_path"
ACNOriginalDrive = "original_drive"
ACNServerFileName = "server_file_name"
ACNServerFilePath = "server_file_path"
ACNPreviewFilePath = "preview_file_path"
ACNPreviewUrl = "preview_url"
ACNReceivedDate = "received_date"
ACNFootprintAddedDate = "footprint_added_date"
ACNFootprintModifiedDate = "footprint_modified_date"
ACNFileSize = "file_size"
ACNNGAVendorImageId = "nga_vendor_image_id"
ACNStereo_IK = "stereo_ik"
ACNMosaic_IK = "mosaic_ik"
ACNAbsCalFactor = "absolute_calibration_factor"
ACNEffectiveBandwidth = "effective_bandwidth"
ACNTDI = "tdi"
ACNRadiometryGain = "radiometry_gain"
ACNRadiometryOffset = "radiometry_offet"
ACNNGAName = "nga_name"

ACN_LIST_ALL = (
    ACNSensor, ACNSceneId, ACNStripId, ACNStatus, ACNCatalogId, ACNOrderId, ACNAcquisitionTime,
    ACNProductLevel, ACNRasterBandCount, ACNRasterRows, ACNRasterColumns, ACNPixelDepth,
    ACNFileFormat, ACNCloudCover, ACNSunElevation, ACNScanDirection, ACNOffNadir,
    ACNGroundSampleDistance, ACNReferenceHeight, ACNExposureDuration, ACNLineRate,
    ACNSpectralType, ACNCountryCode, ACNCentroidLatitude, ACNCentroidLongitude,
    ACNOriginalFileName, ACNOriginalFilePath, ACNOriginalDrive, ACNServerFileName,
    ACNServerFilePath, ACNPreviewFilePath, ACNPreviewUrl, ACNReceivedDate,
    ACNFootprintAddedDate, ACNFootprintModifiedDate, ACNFileSize, ACNNGAVendorImageId,
    ACNStereo_IK, ACNMosaic_IK, ACNAbsCalFactor, ACNEffectiveBandwidth, ACNTDI,
    ACNRadiometryGain, ACNRadiometryOffset, ACNNGAName
)

DEFAULT_PGC_ACN_LIST = (
    ACNSensor, ACNSceneId, ACNStripId, ACNStatus, ACNCatalogId, ACNOrderId, ACNAcquisitionTime,
    ACNProductLevel, ACNRasterBandCount, ACNRasterRows, ACNRasterColumns, ACNPixelDepth,
    ACNFileFormat, ACNCloudCover, ACNSunElevation, ACNScanDirection, ACNOffNadir,
    ACNGroundSampleDistance, ACNReferenceHeight, ACNExposureDuration, ACNLineRate,
    ACNSpectralType, ACNCountryCode, ACNCentroidLatitude, ACNCentroidLongitude,
    ACNOriginalFileName, ACNOriginalFilePath, ACNOriginalDrive, ACNServerFileName,
    ACNServerFilePath, ACNPreviewFilePath, ACNPreviewUrl, ACNReceivedDate, ACNFootprintAddedDate,
    ACNFootprintModifiedDate, ACNFileSize, ACNNGAVendorImageId, ACNNGAName
)

DEFAULT_LOCAL_ACN_LIST = (
    ACNSensor, ACNSceneId, ACNStatus, ACNCatalogId, ACNOrderId, ACNAcquisitionTime,
    ACNProductLevel, ACNRasterBandCount, ACNRasterRows, ACNRasterColumns, ACNPixelDepth,
    ACNFileFormat, ACNCloudCover, ACNSunElevation, ACNScanDirection, ACNOffNadir,
    ACNGroundSampleDistance, ACNReferenceHeight, ACNExposureDuration, ACNLineRate,
    ACNSpectralType, ACNCountryCode, ACNCentroidLatitude, ACNCentroidLongitude,
    ACNOriginalFileName, ACNOriginalFilePath, ACNOriginalDrive, ACNPreviewFilePath,
    ACNPreviewUrl, ACNFileSize, ACNNGAVendorImageId, ACNAbsCalFactor, ACNEffectiveBandwidth
)

# Basic Common Name list
CommonName = namedtuple("CommonName", ["cnid", "cname", "type"])

# Common names for basic attributes used by PGC
ATTRIBUTE_COMMON_NAMES = [
    CommonName(0, ACNSensor, "text"),
    CommonName(1, ACNSceneId, "text"),
    CommonName(2, ACNStatus, "text"),
    CommonName(3, ACNCatalogId, "text"),
    CommonName(4, ACNOrderId, "text"),
    CommonName(5, ACNAcquisitionTime, "datetime"),
    CommonName(6, ACNProductLevel, "text"),
    CommonName(7, ACNRasterBandCount, "integer"),
    CommonName(8, ACNRasterRows, "integer"),
    CommonName(9, ACNRasterColumns, "integer"),
    CommonName(10, ACNPixelDepth, "integer"),
    CommonName(11, ACNFileFormat, "text"),
    CommonName(12, ACNCloudCover, "real"),
    CommonName(13, ACNSunElevation, "real"),
    CommonName(14, ACNScanDirection, "text"),
    CommonName(15, ACNOffNadir, "real"),
    CommonName(16, ACNGroundSampleDistance, "real"),
    CommonName(17, ACNReferenceHeight, "real"),
    CommonName(18, ACNExposureDuration, "real"),
    CommonName(19, ACNLineRate, "real"),
    CommonName(20, ACNSpectralType, "text"),
    CommonName(21, ACNCountryCode, "text"),
    CommonName(22, ACNCentroidLatitude, "real"),
    CommonName(23, ACNCentroidLongitude, "real"),
    CommonName(24, ACNOriginalFileName, "text"),
    CommonName(25, ACNOriginalFilePath, "text"),
    CommonName(26, ACNServerFileName, "text"),
    CommonName(27, ACNServerFilePath, "text"),
    CommonName(28, ACNPreviewFilePath, "text"),
    CommonName(29, ACNPreviewUrl, "text"),
    CommonName(30, ACNReceivedDate, "datetime"),
    CommonName(31, ACNFootprintAddedDate, "datetime"),
    CommonName(32, ACNFootprintModifiedDate, "datetime"),
    CommonName(33, ACNFileSize, "real"),
    CommonName(34, ACNNGAVendorImageId, "text"),
    CommonName(35, ACNStereo_IK, "text"),
    CommonName(36, ACNMosaic_IK, "text"),
    CommonName(37, ACNAbsCalFactor, "list"),
    CommonName(38, ACNEffectiveBandwidth, "list"),
    CommonName(39, ACNOriginalDrive, "text"),
    CommonName(40, ACNStripId, "text"),
    CommonName(41, ACNTDI, "list"),
    CommonName(42, ACNRadiometryGain, "list"),
    CommonName(43, ACNRadiometryOffset, "list"),
    CommonName(44, ACNNGAName, "text")
]

# Dictionary for finding data type by common name
DSTYPE_FROM_ACN = {acn.cname:acn.type for acn in ATTRIBUTE_COMMON_NAMES}

# Dictionary for finding id by common name
CNID_FROM_ACN = {acn.cname:acn.cnid for acn in ATTRIBUTE_COMMON_NAMES}

# Vendor, vendor name, vendor xpath, common name, label filter
VendorName = namedtuple("VendorName", ["vendor", "vname", "xpath", "cname", "filter", "priority"])


# Vendor names and XPaths for metadata used as attributes in PGC footprints

# Priority is used when a single vendor has several ways to define the same common name. In those
# cases, the higher priority wins out over lower numbered definitions. In case of tied priorities,
# the last name encountered is used

VENDOR_METADATA_NAMES = [
    VendorName(DG, "SATID", "IMD/IMAGE/SATID", ACNSensor, "", 0),
    VendorName(DG, "CATID", "IMD/IMAGE/CATID", ACNCatalogId, "", 0),
    VendorName(DG, "FIRSTLINETIME", "IMD/IMAGE/FIRSTLINETIME", ACNAcquisitionTime, "", 1),
    VendorName(DG, "EARLIESTACQTIME", "IMD/MAP_PROJECTED_PRODUCT/EARLIESTACQTIME", ACNAcquisitionTime, "", 0),
    VendorName(DG, "PRODUCTORDERID", "IMD/PRODUCTORDERID", ACNOrderId, "", 0),
    VendorName(DG, "PRODUCTLEVEL", "IMD/PRODUCTLEVEL", ACNProductLevel, "r'([0-9][A-Z])'", 0),
    VendorName(DG, "BANDID", "IMD/BANDID", ACNSpectralType, "", 0),
    VendorName(DG, "BITSPERPIXEL", "IMD/BITSPERPIXEL", ACNPixelDepth, "", 0),
    VendorName(DG, "OUTPUTFORMAT", "IMD/OUTPUTFORMAT", ACNFileFormat, "", 0),
    VendorName(DG, "MEANCOLLECTEDGSD", "IMD/IMAGE/MEANCOLLECTEDGSD", ACNGroundSampleDistance, "", 0),
    VendorName(DG, "SUNEL", "IMD/IMAGE/SUNEL", ACNSunElevation, "", 0),
    VendorName(DG, "MEANSUNEL", "IMD/IMAGE/MEANSUNEL", ACNSunElevation, "", 1),
    VendorName(DG, "CLOUDCOVER", "IMD/IMAGE/CLOUDCOVER", ACNCloudCover, "", 0),
    VendorName(DG, "SCANDIRECTION", "IMD/IMAGE/SCANDIRECTION", ACNScanDirection, "", 0),
    VendorName(DG, "MEANOFFNADIRVIEWANGLE", "IMD/IMAGE/MEANOFFNADIRVIEWANGLE", ACNOffNadir, "", 1),
    VendorName(DG, "OFFNADIRVIEWANGLE", "IMD/IMAGE/OFFNADIRVIEWANGLE", ACNOffNadir, "", 0),
    VendorName(DG, "AVGLINERATE", "IMD/IMAGE/AVGLINERATE", ACNLineRate, "", 0),
    VendorName(DG, "EXPOSUREDURATION", "IMD/IMAGE/EXPOSUREDURATION", ACNExposureDuration, "", 0),
    VendorName(DG, "HEIGHTOFFSET", "RPB/IMAGE/HEIGHTOFFSET", ACNReferenceHeight, "", 0),
    VendorName(DG, "ABSCALFACTOR", "ABSCALFACTOR", ACNAbsCalFactor, "", 0),
    VendorName(DG, "EFFECTIVEBANDWIDTH", "EFFECTIVEBANDWIDTH", ACNEffectiveBandwidth, "", 0),
    VendorName(DG, "TDILEVEL", "TDILEVEL", ACNTDI, "", 0),
    VendorName(GE, "archiveId", "inputImageInfo/archiveId", ACNCatalogId, "", 1),
    VendorName(GE, "archiveIdentifier", "sourceInformation/archiveIdentifier", ACNCatalogId, "", 0),
    VendorName(GE, "firstLineAcquisitionDateTime", "inputImageInfo/firstLineAcquisitionDateTime", ACNAcquisitionTime, "", 0),
    VendorName(GE, "firstLineAcquisitionDateTime", "imageInfo/firstLineAcquisitionDateTime", ACNAcquisitionTime, "", 0),
    VendorName(GE, "satelliteName", "sensorInfo/satelliteName", ACNSensor, "", 0),
    VendorName(GE, "opsOrderNumber", "productOrderInfo/opsOrderNumber", ACNOrderId, "", 2),
    VendorName(GE, "inputImageIdentifier", "sourceInformation/inputImageIdentifier", ACNOrderId, "", 1),
    VendorName(GE, "orderId", "productOrderInfo/orderId", ACNOrderId, "", 0),
    VendorName(GE, "outputFormat", "productInfo/outputFormat", ACNFileFormat, "", 0),
    VendorName(GE, "productSpectralType", "productInfo/productSpectralType", ACNSpectralType, "", 0),
    VendorName(GE, "productCloudCoverPercentage", "productInfo/productCloudCoverPercentage", ACNCloudCover, "", 0),
    VendorName(GE, "percentCloudCover", "inputImageInfo/percentCloudCover", ACNCloudCover, "", 0),
    VendorName(GE, "countryCode", "productInfo/countryCode", ACNCountryCode, "", 0),
    VendorName(GE, "bitsPerPixel", "productInfo/bitsPerPixel", ACNPixelDepth, "", 0),
    VendorName(GE, "heightOffset", "productInfo/rationalFunctions/heightOffset", ACNReferenceHeight, "", 0),
    VendorName(GE, "firstLineSunElevationAngle", "inputImageInfo/firstLineSunElevationAngle", ACNSunElevation, "", 0),
    VendorName(GE, "scanDirection", "inputImageInfo/scanDirection", ACNScanDirection, "", 0),
    VendorName(GE, "firstLineRollAngle", "inputImageInfo/firstLineRollAngle", ACNOffNadir, "", 0),
    VendorName(GE, "lineSampleTime", "inputImageInfo/lineSampleTime", ACNExposureDuration, "", 0),
    VendorName(GE, "lineRate", "inputImageInfo/lineRate", ACNLineRate, "", 0),
    VendorName(GE, "gain", "bandSpecificInformation/radiometry/gain", ACNRadiometryGain, "", 0),
    VendorName(GE, "offset", "bandSpecificInformation/radiometry/offset", ACNRadiometryOffset, "", 0),
    VendorName(GE, "tdiMode", "bandSpecificInformation/tdiMode", ACNTDI, "", 0),
	VendorName(GE, "firstLineSunElevationAngle", "inputImageInfo/firstLineSunElevationAngle", ACNSunElevation, "", 0),
    VendorName(IK, "Sensor_Name", "Sensor_Name", ACNSensor, "", 0),
    VendorName(IK, "Sensor", "Sensor", ACNSensor, "", 0),
    VendorName(IK, "Stereo", "Stereo", ACNStereo_IK, "", 0),
    VendorName(IK, "Mosaic", "Mosaic", ACNMosaic_IK, "", 0),
    VendorName(IK, "Reference_Height", "Reference_Height", ACNReferenceHeight, "r'(-?\d+\.\d+)(?:\smeters)?'", 0),
    VendorName(IK, "Product_Order_Number", "Product_Order_Number", ACNOrderId, "", 0),
    VendorName(IK, "File_Format", "File_Format", ACNFileFormat, "", 0),
    VendorName(IK, "Bits_per_Pixel_per_Band", "Bits_per_Pixel_per_Band", ACNPixelDepth, "r'(\d+)(?:\sbits)?'", 0),
    VendorName(IK, "Source_Image_ID", "Source_Image_ID", ACNCatalogId, "", 0),
    VendorName(IK, "Sun_Angle_Elevation", "Sun_Angle_Elevation", ACNSunElevation, "r'(\d+\.\d+)(?:\sdegrees)?'", 0),
    VendorName(IK, "Acquisition_Date", "Acquisition_Date+Time", ACNAcquisitionTime, "", 0),
    VendorName(IK, "Scan_Direction", "Scan_Direction", ACNScanDirection, "", 0),
    VendorName(IK, "Country_Code", "Country_Code", ACNCountryCode, "", 0),
    VendorName(IK, "Percent_Component_Cloud_Cover", "Percent_Component_Cloud_Cover", ACNCloudCover, "", 0),
    VendorName(IK, "Panchromatic_TDI_Mode", "Panchromatic_TDI_Mode", ACNTDI, "", 0),
    VendorName(NGA, "bitsPerPixel", "ciMetadata/bitsPerPixel", ACNPixelDepth, "", 0),
    VendorName(NGA, "cloudCover", "ciMetadata/cloudCover", ACNCloudCover, "", 0),
    VendorName(NGA, "groundSampleDistance", "ciMetadata/groundSampleDistance", ACNGroundSampleDistance, "", 0),
    VendorName(NGA, "country", "ciMetadata/country/code", ACNCountryCode, "", 0),
    VendorName(NGA, "imageFormat", "ciMetadata/imageFormat", ACNFileFormat, "", 0),
    VendorName(NGA, "platform", "ciMetadata/platform/code", ACNSensor, "", 0),
    VendorName(NGA, "sensorType", "ciMetadata/sensorType/description", ACNSpectralType, "", 0),
    VendorName(NGA, "elevation", "ciMetadata/sun/elevation", ACNSunElevation, "", 0),
    VendorName(NGA, "vendorArchiveId", "ciMetadata/vendorArchiveId", ACNCatalogId, "", 0),
    VendorName(NGA, "whenCollected", "ciMetadata/whenCollected", ACNAcquisitionTime, "", 0),
    VendorName(NGA, "vendorImageId", "ciMetadata/vendorImageId", ACNNGAVendorImageId, "", 0),
    VendorName(NGA, "processingLevel", "ciMetadata/processingLevel", ACNProductLevel, "", 0),
]

# Common name id, common name, common name type, vendor, vendor metadata xpath, metadata
# value filter
VendorNameDefn = namedtuple("VendorNameDefn", ["cnid", "cname", "ctype", "vendor",
                                               "xpath", "filter", "priority"])


### Functions

module_logger = logging.getLogger(__name__)
module_debug = module_logger.isEnabledFor(logging.DEBUG)

# Match vendor metadata names/definitions to common name attributes
def getVendorNameDefnsFromAttributes(acn_list=None):
    """
    Given a list of desired attributes, build a AttributeDefinitionsList for use when creating
    metadata dictionaries; all defined attributes is the default
    """

    # If an ACN list is not specified, just use them all; ACN list is simply a list
    # of attribute common names desired
    if acn_list is None:
        name_defns = [(cn, vn) for cn in ATTRIBUTE_COMMON_NAMES for vn in \
            VENDOR_METADATA_NAMES if cn.cname == vn.cname]
    else:
        name_defns = [(cn, vn) for cn in ATTRIBUTE_COMMON_NAMES for vn in \
            VENDOR_METADATA_NAMES if cn.cname == vn.cname and cn.cname in acn_list]

    # Now build a definitions list of the requested common names from the vendor
    # definitions
    vn_defns_list = []

    for cn, vn in name_defns:
        vn_defns_list.append(VendorNameDefn(cn.cnid, cn.cname, cn.type, vn.vendor,
                                              vn.xpath, vn.filter, vn.priority))

    return vn_defns_list

# Convert attribute-as-string (from metadata) to actual value accoridng to type
def getAttributeAsValue(common_name, in_value):
    """Convert a string attribute to its actual type and return"""

    out_value = None
    if in_value:
        data_type = DSTYPE_FROM_ACN.get(common_name)
        if data_type == "real":
            out_value = float(in_value)
        elif data_type == "integer":
            out_value = int(in_value)
        elif data_type == "long":
            out_value = long(in_value)
        elif data_type in ["datetime", "date"]:
            if not isinstance(in_value, datetime.datetime):
                out_value = DP.parse(in_value)
            else:
                out_value = in_value
        elif isinstance(in_value, basestring):
            out_value = in_value.strip()

    return out_value
