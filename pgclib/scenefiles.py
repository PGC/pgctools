#!/usr/bin/env python

"""
Classes and functions for handling SceneFiles
"""

import os
import re
import tarfile
import logging
import copy

from constants import *
from utils import getFixedPath, getFilesByType, getMD5Checksum

__all__ = [
    "SceneFile", "SceneFilesCollection", "getFileContents", "extractFileContentsFromTar",
    "sortSceneFilesByGid"
]


class SceneFile(object):

    """
    SceneFile represents an imagery file and maintains its PGC attributes such as
    vendor, mode and kind
    """

    ATTRIBUTES = (
        "vendor", "mode", "kind", "gid", "fid", "proxy_name", "size", "file_name", "file_dir",
        "is_short_gid", "checksum"
    )

    FILENAME_FIELDS = (
        "pfix", "ts", "snsr", "band", "ext", "tail", "prod", "tile", "oid", "pnum",
        "catid", "said", "pid", "siid", "ver", "mono", "po", "cmp", "lat",
        "unk1", "unk2"
    )

    def __init__(self, path, checksum=False):
        self.name_dict = None   # Dictionary of parts of the file name
        self.attributes = None  # Dictionary of file attributes

        _path = getFixedPath(path)

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)
        if self.debug:
            self.logger.debug("SceneFile called with path: %s" % _path)

        # If the path exists, set the attributes
        if _path:
            self._setAttributes(_path, checksum)

    @property
    def file_name(self):
        return self.attributes.get("file_name", "")

    @property
    def file_dir(self):
        return self.attributes.get("file_dir", "")

    @property
    def as_path(self):
        return os.path.join(self.file_dir, self.file_name)

    @property
    def size(self):
        return self.attributes.get("size", "")

    @property
    def vendor(self):
        return self.attributes.get("vendor", "")

    @property
    def mode(self):
        return self.attributes.get("mode", "")

    @property
    def is_short_gid(self):
        return self.attributes.get("is_short_gid", "")

    @property
    def proxy_name(self):
        return self.attributes.get("proxy_name", "")

    @property
    def gid(self):
        return self.attributes.get("gid", "")

    @gid.setter
    def gid(self, value):
        self.attributes["gid"] = value

    @property
    def fid(self):
        return self.attributes.get("fid", "")

    @fid.setter
    def fid(self, value):
        self.attributes["fid"] = value

    @property
    def has_proxy_name(self):
        return all([self.file_name, self.proxy_name, self.file_name != self.proxy_name])

    @property
    def kind(self):
        return self.attributes.get("kind", "")

    @property
    def is_dg_tiled(self):
        return bool(self.vendor == DG and self.name_dict.get("tile"))

    @property
    def is_ortho(self):
        return self.name_dict.get("pfix", "").lower() == "ortho"

    @property
    def checksum(self):
        return self.attributes.get("checksum")

    def getDGTiledProxyName(self):
        fmt = {RAW:RAW_DG_PROXY_TILED, RENAMED:RENAMED_DG_PROXY_TILED}[self.mode]
        return fmt % self.name_dict

    def _setAttributes(self, path, checksum):
        """Set the attributes for this SceneFile"""

        self.attributes = {attribute:"" for attribute in self.ATTRIBUTES}
        self.name_dict = {field:"" for field in self.FILENAME_FIELDS}

        if self.debug:
            self.logger.debug("Setting attributes for file %s" % path)

        # File size
        try:
            _filesz = os.path.getsize(path)
        except (IOError, OSError) as err:
            _filesz = 0
            self.logger.warning("Could not get file size for %s because %s" % (path, err))

        self.attributes["size"] = _filesz
        if self.debug:
            self.logger.debug("File size is %d bytes" % _filesz)

        # MD5 Checksum
        _checksum = None
        if checksum and _filesz > 0:
            _checksum = getMD5Checksum(path)
        self.attributes["checksum"] = _checksum
        if self.debug:
            self.logger.debug("MD5 Checksum is %s" % str(_checksum))

        # File directory and basename
        self.attributes["file_dir"], self.attributes["file_name"] = os.path.split(path)

        # Only proceed with identification if file is not zero length
        if _filesz > 0:

            # Use regexes to try and match the file name, or possibly the file's
            # directory name
            matcher = None
            is_folder_id = False  # Identifies match was made via directory name
            is_short_gid = False  # Identifies match made on IKONOS po number without component
            for fi in FILE_IDENTIFIERS:
                matcher = re.search(fi.pat, self.file_name, re.I | re.X)
                if matcher:
                    is_short_gid = fi.ven == IK and not matcher.groupdict().get("cmp")
                    break

            else:
                for fi in FOLDER_IDENTIFIERS:
                    matcher = re.search(fi.pat, self.file_dir, re.I)
                    if matcher:
                        is_short_gid = True
                        is_folder_id = True
                        break

            # If a match was made, start setting attributes
            if matcher:
                self.attributes["vendor"] = fi.ven
                self.attributes["mode"] = fi.mode
                self.attributes["is_short_gid"] = is_short_gid
                if self.debug:
                    self.logger.debug("Matched: vendor = %s, mode = %s, is_short_gid = %r" % \
                                      (fi.ven, fi.mode, is_short_gid))

                # Load the name_dict with all matched parts
                for key, value in matcher.groupdict().items():
                    value = "" if value is None else value
                    self.name_dict[key] = value

                if self.debug:
                    self.logger.debug("Built name_dict: %r" % self.name_dict)

                # If the match was by directory name, file is a support file and
                # only a few attributes need be set, otherwise set all attributes
                if is_folder_id:
                    self.attributes["kind"] = SUPPORT
                    self.attributes["proxy_name"] = self.file_name
                    self.attributes["is_short_gid"] = is_short_gid
                    if self.debug:
                        self.logger.debug("File identified by folder: %s" % self.file_name)

                else:

                    # Set the proxy_name; often the same as the filename, but different if
                    # the file was renamed by someone other than the vendor, in which case
                    # it is the 'original' filename
                    if fi.pnfmt:
                        # Hack to eliminate NITF chip names
                        if self.name_dict.get("tail", "").startswith("__"):
                            self.name_dict["tail"] = ""
                        self.attributes["proxy_name"] = fi.pnfmt % self.name_dict
                    else:
                        self.attributes["proxy_name"] = self.file_name

                    if self.debug:
                        self.logger.debug("Proxy name set to %s" % self.attributes["proxy_name"])

                    # Find the GID (group id) and FID (file id)
                    if fi.ven == DG and self.name_dict.get("tile"):
                        fid_fmt = {RAW:RAW_DG_TILED_FID, RENAMED:RENAMED_DG_TILED_FID}[fi.mode]
                        gid_fmt = {RAW:RAW_DG_TILED_GID, RENAMED:RENAMED_DG_TILED_GID}[fi.mode]
                    else:
                        fid_fmt = fi.fid
                        gid_fmt = fi.gid
                    if fi.ven == IK:
                        _band = self.name_dict.get("band", "")
                        if "pan" in _band:
                            self.name_dict["spec"] = "pan_"
                        elif _band == "":
                            self.name_dict["spec"] = ""
                        else:
                            self.name_dict["spec"] = "msi_"
                    self.attributes["fid"] = fid_fmt % self.name_dict
                    self.attributes["gid"] = gid_fmt % self.name_dict

                    if self.debug:
                        self.logger.debug("File fid: %s" % self.attributes["fid"])
                        self.logger.debug("File gid: %s" % self.attributes["gid"])

                    # Determine file type, primarily by extension
                    kind = ""
                    ext = self.name_dict.get("ext", "").lower()
                    if ext:
                        kind = EXT2TYPE.get(ext, SUPPORT)  # Select initial type by extension

                        # If file is a projection, relable as support unless it's an ortho
                        if kind == PROJECTION:
                            if not self.is_ortho:
                                kind = SUPPORT

                        # JPEG and text files are sometimes not previews or metadata, so
                        # additional filename checking is required
                        elif kind in (PREVIEW, METADATA):
                            tail = self.name_dict.get("tail", "").lower()
                            if tail and tail not in VALID_IMAGERY_TAILS and not self.is_ortho:
                                kind = SUPPORT

                    # Set everything else to type IGNORE
                    else:
                        kind = IGNORE
                        if self.debug:
                            self.logger.debug("Unidentified file: %s" % self.as_path)

                    self.attributes["kind"] = kind
                    if self.debug:
                        self.logger.debug("File kind: %s" % kind)

            # Anything failing to match a regex is ignored
            else:
                self.attributes["kind"] = IGNORE
                if self.debug:
                    self.logger.debug("Unmatched file: %s" % self.as_path)

        # Zero length files are invalid
        else:
            # Sometimes zero-length files are used to mark the end of file copying,
            # do not flag as invalid
            if all(["_eot" not in self.file_name.lower(), ".fin" not in self.file_name.lower()]):
                self.attributes["kind"] = INVALID
                if self.debug:
                    self.logger.debug("Found zero length file: %s" % self.as_path)

    def __lt__(self, other):
        return self.proxy_name < other.proxy_name

    def __le__(self, other):
        return self.proxy_name <= other.proxy_name

    def __eq__(self, other):
        return self.proxy_name == other.proxy_name

    def __ne__(self, other):
        return self.proxy_name != other.proxy_name

    def __gt__(self, other):
        return self.proxy_name > other.proxy_name

    def __ge__(self, other):
        return self.proxy_name >= other.proxy_name

    def __hash__(self):
        return hash(self.proxy_name)

    def __str__(self):
        return self.as_path


class SceneFilesCollection(object):

    """
    Container for a collection of SceneFiles

    Given a path to a root directory, walks throough all files and collects them as
    SceneFiles

    May also provide a list of file types to either skip or keep during collection;
    types must be of the kinds found in constants.py
    """

    def __init__(self, root, skips=None, keeps=None, checksums=False):
        self._root = None
        self.scene_files = None

        _root = getFixedPath(root)

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)
        if self.debug:
            self.logger.debug("Called with root path: %s" % _root)

        if _root:
            self._root = _root
            self.scene_files = self.collectFiles(_root, skips, keeps, checksums)

        else:
            self.logger.warning("Root path invalid or not a directory: %s" % _root)

    @property
    def root(self):
        return self._root

    @property
    def files(self):
        return self.scene_files

    @property
    def mode(self, ):
        mode = None
        modes = {scene_file.mode for scene_file in self.files if scene_file.mode}

        if len(modes) > 1:
            self.logger.warning("Multiple modes found in collection: %r" % modes)

        if len(modes) > 0:
            mode = modes.pop()

        return mode

    def __len__(self):
        count = len(self.files) if self.files else 0
        return count

    def __iter__(self):
        for scene_file in self.files:
            yield scene_file

    def collectFiles(self, root, skips, keeps, checksums):
        """Walk through directory hierarchy and collect files as SceneFiles"""

        scene_files = []

        for item in getFilesByType(root):

            # Skip symlinks and other hidden files
            if os.path.basename(item).startswith("."):
                continue

            scene_file = SceneFile(item, checksums)

            # Check SceneFile is valid
            if scene_file.attributes is None:
                self.logger.warning("Failed to create SceneFile from %s, skipping" % item)
                continue

            # Handle filters
            if skips and scene_file.kind in skips:
                continue
            if keeps and scene_file.kind not in keeps:
                continue

            # Do not include zero-length files
            if scene_file.size == 0:
                continue

            scene_files.append(scene_file)

        return scene_files


# Functions

module_logger = logging.getLogger(__name__)

def getFileContents(scene_file):
    """
    Read and return the contents of a SceneFile; if the type is an archive (TAR) then
    return a list of the members (tarinfos) instead
    """

    contents = None

    if os.path.isfile(scene_file.as_path) and scene_file.size > 0:
        file_mode = None

        # For archives (tarballs) return a list of tarinfo members
        if scene_file.kind == ARCHIVE:
            try:
                tar = tarfile.open(scene_file.as_path, mode="r")
            except (IOError, OSError) as err:
                module_logger.error("Could not open archive %s because %s" % (scene_file.as_path, err))
            else:
                try:
                    contents = tar.getmembers()
                except (tarfile.TarError, IOError) as err:
                    contents = []
                    module_logger.error("Could not read archive %s because %s" % (scene_file.as_path, err))
            finally:
                tar.close()

        # Preview JPEGs have to be read as binary files
        elif scene_file.kind == PREVIEW:
            file_mode = "rb"

        # Other acceptable kinds are read as text
        elif scene_file.kind in (METADATA, METADATA_SECONDARY, PREVIEW_WORLD, IMAGE_WORLD,
                                 PROJECTION, UNDO):
            file_mode = "r"

        else:
            module_logger.warning("Kind %s is not supported for contents extraction" % scene_file.kind)

        if file_mode:
            try:
                contents = open(scene_file.as_path, file_mode).read()
            except (IOError, OSError) as err:
                contents = None
                module_logger.error("Could not read file %s because %s" % (scene_file.as_path, err))

    else:
        module_logger.error("File %s is not accessible at this time" % scene_file.as_path)

    return contents

def extractFileContentsFromTar(scene_file, tinfo):
    """
    Given a tarinfo object, find, extract and return the corresponding file contents
    """

    contents = None

    if scene_file.kind == ARCHIVE:
        try:
            tar = tarfile.open(scene_file.as_path, mode="r")
        except (IOError, OSError) as err:
            tar = None
            module_logger.error("Could not open archive %s because %s" % (scene_file.as_path, err))

        if tar:
            tar.errorlevel = 2
            try:
                contents = tar.extractfile(tinfo).read()
            except tarfile.ExtractError as err:
                contents = None
                module_logger.error("Coould not extract %s from archive because %s" % (tinfo.name, err))
            finally:
                tar.close()

    return contents

def sortSceneFilesByGid(collection):
    """Returns a dictionary of SceneFiles grouped as scenes"""
    return {RAW:_sortRawSceneFilesByGid,
            RENAMED:_sortRenamedSceneFilesByGid}[collection.mode](collection)

def _sortRenamedSceneFilesByGid(collection):
    """Returns a dictionary of renamed SceneFiles grouped as scenes"""
    gids = {}
    for scene_file in collection:
        if scene_file.kind in [INVALID, IGNORE]:
            continue
        if scene_file.gid:
            gids.setdefault(scene_file.gid, []).append(scene_file)
    return gids

def _sortRawSceneFilesByGid(collection):
    """Returns a dictionary of raw SceneFiles grouped as scenes"""
    short_gids = {}
    nga_gids = {}
    gids = {}

    # Sort into dictionaries according to type of gid and if vendor is NGA
    for scene_file in collection:
        if scene_file.kind in [INVALID, IGNORE]:
            continue
        if scene_file.gid:
            if scene_file.is_short_gid:
                short_gids.setdefault(scene_file.gid, []).append(scene_file)
            elif scene_file.vendor == NGA:
                nga_gids.setdefault(scene_file.gid, []).append(scene_file)
            else:
                gids.setdefault(scene_file.gid, []).append(scene_file)

    # Reconcile short_gids into gids
    ikpat = re.compile(r"(\d{5,7})_[a-z]{3}_\d{7}")
    dgpat = re.compile(r"\d\d[a-z]{3}\d{8}-(?:[a-z0-9]{4}|[a-z0-9]{4}_[a-z0-9]{4})-(\d{12}_\d\d)_P\d{3}", re.I)
    for gid in sorted(gids.iterkeys()):
        m1, m2 = ikpat.match(gid), dgpat.match(gid)
        if not m1 and not m2:
            continue
        m = m1 if m1 else m2
        if m.group(1) in short_gids:
            chk_dir = {sf.file_dir for sf in gids[gid]}.pop()
            for sf in short_gids[m.group(1)]:
                if sf.file_dir.lower() != chk_dir.lower():
                    continue
                sf_new = copy.copy(sf)
                sf_new.gid = gid
                gids[gid].append(sf_new)

    # Reconcile orphan NGA metadata into gids
    for nga_gid in sorted(nga_gids.iterkeys()):
        if len(nga_gids[nga_gid]) == 1 and nga_gids[nga_gid][0].kind == METADATA:
            mtext = getFileContents(nga_gids[nga_gid][0])
            if mtext:
                m = re.search(r"<vendorImageId>(.*)</vendorImageId>", mtext)
                if m:
                    if m.group(1) in gids:
                        sf_new = copy.copy(nga_gids[nga_gid][0])
                        sf_new.gid = m.group(1)
                        gids[m.group(1)].append(sf_new)
        else:
            for sf in nga_gids[nga_gid]:
                gids.setdefault(sf.gid, []).append(sf)

    # Reconcile orphaned DG tiled support files
    # First, find the DG tiled scenes and index them in a dictionary
    tiled_scenes = {}  # key = non-tiled gid,  val = list of gids
    for gid in sorted(gids.iterkeys()):
        if len([sf for sf in gids[gid] if sf.is_dg_tiled]) > 0:
            # SceneFiles in this gid are DG tiled, does it have an image?
            sfiles = [sf for sf in gids[gid] if sf.kind == IMAGE]
            if len(sfiles) > 0:
                # Found the image file, determine the non-tiled gid
                nt_gid = RAW_DG_GID % sfiles[0].name_dict
                tiled_scenes.setdefault(nt_gid, []).append(gid)

    # Now check the main gids dictionary for orphans
    for nt_gid in sorted(tiled_scenes.iterkeys()):
        if nt_gid in gids:
            if len([sf for sf in gids[nt_gid] if sf.kind == IMAGE]) == 0:
                # For each orphaned scenefile...
                for sf in gids[nt_gid]:
                    # For each tiled_gid in the main dictionary...
                    for t_gid in tiled_scenes[nt_gid]:
                        sf.gid = t_gid  # Set the new, tiled gid and ...
                        gids[t_gid].append(sf)  # Add the corrected scenefile

    return gids
