#!/usr/bin/env python

"""
Convenience functions for use with ESRI Shapefiles and File GDBs
"""

import os
import logging
import datetime

from collections import namedtuple, OrderedDict

from osgeo import ogr
from osgeo import osr

from attributes import CNID_FROM_ACN


__all__ = [
    "OgrInterfaceForESRI", "getStandardAttributeForCommonName", "makeStandardAttributeDefnsList",
    "OGR_SHAPEFILE_DRIVER", "OGR_FILEGDB_DRIVER"
]


# Default coordinate system (WGS84)
DEFAULT_CRS = osr.SpatialReference()
DEFAULT_CRS.ImportFromEPSG(4326)

# Define attribute types as OGR data types
# NOTE: Shapefiles/FileGDBs do not support long integers, converting from long
#       could result in value truncation
ATTRIBUTE_TYPE_TO_OGR_TYPE = {
    "integer": ogr.OFTInteger,
    "long": ogr.OFTInteger,
    "real": ogr.OFTReal,
    "text": ogr.OFTString,
    "datetime": ogr.OFTDateTime,
    "list": ogr.OFTString
}

# OGR Drivers
OGR_SHAPEFILE_DRIVER = "ESRI Shapefile"
OGR_FILEGDB_DRIVER = "FileGDB"

OGR_DRIVERS = (OGR_SHAPEFILE_DRIVER, OGR_FILEGDB_DRIVER)

# common name id, attribute field name, storage type, field width, field precision
StandardAttribute = namedtuple("StandardAttribute", ("cnid", "afn", "stype", "fwid", "fprc"))

# Attributes defined in the PGC master footprint
PGC_FOOTPRINT_ESRI_ATTRIBUTE_DEFINITIONS = [
    StandardAttribute(0, "SENSOR", ogr.OFTString, 12, 0),
    StandardAttribute(1, "SCENE_ID", ogr.OFTString, 48, 0),
    StandardAttribute(40, "STRIP_ID", ogr.OFTString, 72, 0),
    StandardAttribute(2, "STATUS", ogr.OFTString, 16, 0),
    StandardAttribute(3, "CATALOG_ID", ogr.OFTString, 32, 0),
    StandardAttribute(4, "ORDER_ID", ogr.OFTString, 32, 0),
    StandardAttribute(5, "ACQ_TIME", ogr.OFTString, 32, 0),  # String to preserve microseconds
    StandardAttribute(6, "PROD_LEVEL", ogr.OFTString, 6, 0),
    StandardAttribute(7, "BANDS", ogr.OFTInteger, 2, 0),
    StandardAttribute(8, "ROWS", ogr.OFTInteger, 8, 0),
    StandardAttribute(9, "COLUMNS", ogr.OFTInteger, 8, 0),
    StandardAttribute(10, "BITS_PIXEL", ogr.OFTInteger, 2, 0),
    StandardAttribute(11, "OUTPUT_FMT", ogr.OFTString, 8, 0),
    StandardAttribute(12, "CLOUDCOVER", ogr.OFTReal, 12, 4),
    StandardAttribute(13, "SUN_ELEV", ogr.OFTReal, 12, 3),
    StandardAttribute(14, "SCAN_DIR", ogr.OFTString, 12, 0),
    StandardAttribute(15, "OFF_NADIR", ogr.OFTReal, 12, 3),
    StandardAttribute(16, "MEANGSD", ogr.OFTReal, 12, 3),
    StandardAttribute(17, "REF_HEIGHT", ogr.OFTReal, 12, 3),
    StandardAttribute(18, "EXPOSURE", ogr.OFTReal, 14, 6),
    StandardAttribute(19, "LINE_RATE", ogr.OFTReal, 12, 3),
    StandardAttribute(20, "SPEC_TYPE", ogr.OFTString, 16, 0),
    StandardAttribute(21, "COUNTRY", ogr.OFTString, 4, 0),
    StandardAttribute(22, "CENT_LAT", ogr.OFTReal, 18, 12),
    StandardAttribute(23, "CENT_LONG", ogr.OFTReal, 18, 12),
    StandardAttribute(24, "O_FILENAME", ogr.OFTString, 96, 0),
    StandardAttribute(25, "O_FILEPATH", ogr.OFTString, 254, 0),
    StandardAttribute(39, "O_DRIVE", ogr.OFTString, 16, 0),
    StandardAttribute(26, "S_FILENAME", ogr.OFTString, 96, 0),
    StandardAttribute(27, "S_FILEPATH", ogr.OFTString, 254, 0),
    StandardAttribute(28, "PREVIEWJPG", ogr.OFTString, 254, 0),
    StandardAttribute(29, "PREVIEWURL", ogr.OFTString, 254, 0),
    StandardAttribute(30, "RCVD_DATE", ogr.OFTDate, 12, 0),
    StandardAttribute(31, "ADDED_DATE", ogr.OFTDate, 12, 0),
    StandardAttribute(32, "MOD_DATE", ogr.OFTDate, 12, 0),
    StandardAttribute(44, "NGA_NAME", ogr.OFTString, 72, 0),
    StandardAttribute(33, "FILE_SZ", ogr.OFTReal, 18, 12)
]

PGCFP_COMMON_NAME_TO_ESRI_NAME = {ad.cnid:ad.afn for ad in PGC_FOOTPRINT_ESRI_ATTRIBUTE_DEFINITIONS}
STD_ATTRS_BY_CNID = OrderedDict(zip([sa.cnid for sa in PGC_FOOTPRINT_ESRI_ATTRIBUTE_DEFINITIONS],
    PGC_FOOTPRINT_ESRI_ATTRIBUTE_DEFINITIONS))


### Class to manage OGR sessions with shapefile or filegdb

class OgrInterfaceForESRI(object):
    """Interface class to manage ESRI shapefiles and File GDBs with OGR"""

    # driver is either the ESRI shapefile or file GDB driver
    # path is the location of the data source, folder or file
    # layer is the initial layer to load (if file GDB or folder)
    # mode is 'r' for read-only, 'w' for read/write (new/overwrite), and
    #   'u' is for update existing data source

    def __init__(self, driver, fp_path, layer=None, mode="r"):

        self._driver = None
        self._data_src = None
        self._layer = None

        self.logger = logging.getLogger(__name__)
        self.debug = self.logger.isEnabledFor(logging.DEBUG)
        if self.debug:
            self.logger.debug("Instantiating OgrInterfaceForESRI instance: driver -> '%s'" % driver)

        ogr.UseExceptions()

        # Load the driver
        _driver = ogr.GetDriverByName(driver)
        if _driver is not None:
            self._driver = _driver
        else:
            self.logger.critical("Could not load OGR Driver '%s'" % driver)
            raise RuntimeError("Could not load OGR Driver '%s'" % driver)

        # Try and establish the data source
        _update = 0 if mode == "r" else 1
        _exists = os.path.exists(fp_path)
        _is_gdb = fp_path.endswith(".gdb")

        _data_src = None

        if mode in ("r", "u"):
            if _exists:
                _data_src = ogr.Open(fp_path, _update)

                if _data_src is None:
                    self.logger.critical("OGR could not open %s as data source" % fp_path)
                    raise RuntimeError("OGR could not open %s as data source" % fp_path)
                else:
                    if self.debug:
                        self.logger.debug("Opened %s as OGR data source" % fp_path)

            else:
                self.logger.critical("Could not find data source %s" % fp_path)
                raise RuntimeError("Could not find data source %s" % fp_path)

        elif mode == "w":

            # If the output data source already exists, as a single shapefile, delete
            # it first and create a new one; otherwise just create one

            # NOTE: You must "update" an existing GDB to add a new layer

            # If the named source exists, try to delete it first (overwrite)
            if _exists:
                try:
                    self._driver.DeleteDataSource(fp_path)
                except RuntimeError as err:
                    self.logger.error("Output data source %s could not be overwritten because %s" % (fp_path, err))
                    raise

            try:
                _data_src = self._driver.CreateDataSource(fp_path)
            except RuntimeError as err:
                _data_src = None
                self.logger.error("Could not create data source at %s because %s" % (fp_path, err))
                raise

        else:
            self.logger.error("Mode must be 'r', 'w' or 'u' -- received '%s'" % mode)
            raise ValueError("Mode must be 'r', 'w' or 'u' -- received '%s'" % mode)

        self._data_src = _data_src

        # If we have a data source, we need to either get a current layer or create one
        if self._data_src is not None:

            _layer = None

            # In read-only mode, just get the named layer, or the first one otherwise
            if mode == "r":
                if layer is not None:
                    _layer = self._data_src.GetLayerByName(layer)

                else:
                    _layer = self._data_src.GetLayerByIndex(0)

                if _layer is None:
                    self.logger.error("Could not retrieve a layer from %s" % fp_path)
                    raise RuntimeError("Could not retrieve a layer from %s" % fp_path)

            elif mode == "u":

                # For GDBs we will either try to add a new layer, or open an existing one
                # depending on whether a provided layer name is found
                if _is_gdb:

                    # If a layer name is provided, see if it already exists, otherwise
                    # try creating a new layer by that name
                    if layer:
                        for i in range(self._data_src.GetLayerCount()):
                            _layer = self._data_src.GetLayerByIndex(i)
                            if _layer.GetName() == layer:
                                break
                            else:
                                _layer = None

                        if _layer is None:
                            _layer = self.createOgrLayer(label=layer)
                            self.logger.warning("Did not find %s as existing layer - will create")

                            if _layer is None:
                                self.logger.error("Could not create layer %s in %s" % (layer, fp_path))
                                raise RuntimeError("Could not create layer %s in %s" % (layer, fp_path))

                    # Otherwise just get the first layer
                    else:
                        _layer = self._data_src.GetLayerByIndex(0)

                # Shapefiles only have one layer, so just get it
                else:
                    _layer = self._data_src.GetLayerByIndex(0)

                if _layer is None:
                    self.logger.error("Could not retrieve a layer from %s" % fp_path)
                    raise RuntimeError("Could not retrieve a layer from %s" % fp_path)

            elif mode == "w":

                # We are creating a new data source, so just create a layer for it
                label = layer if layer else None
                _layer = self.createOgrLayer(label=layer)

                if _layer is None:
                    self.logger.error("Could not create layer %s in %s" % (layer, fp_path))
                    raise RuntimeError("Could not create layer %s in %s" % (layer, fp_path))

            self._layer = _layer

        else:
            self.logger.error("Could not establish a data source at %s" % fp_path)
            raise RuntimeError("Could not establish a data source at %s" % fp_path)

    @property
    def data_src(self):
        return self._data_src

    @property
    def driver(self):
        return self._driver

    @property
    def layer(self):
        return self._layer

    @property
    def layers(self):
        return [self.data_src.GetLayerByIndex(i).GetName() for i in range(self.data_src.GetLayerCount())]

    def createOgrLayer(self, label=None, crs=DEFAULT_CRS, geotype=ogr.wkbPolygon):
        """Create an OGR.Layer in this instance's data source"""

        _layer = None

        # If no label is provided, use the name of the data source
        if not label:
            label = os.path.basename(os.path.splitext(self.data_src.GetName())[0])

        # Try and establish a layer
        _layer = self.data_src.CreateLayer(label, crs, geotype)

        if _layer is None:
            self.logger.error("Could not create layer %s" % label)
        else:
            if self.debug:
                self.logger.debug("Created layer %s in data source %s" % (label,
                                                            self.data_src.GetName()))

        return _layer

    def getSchema(self):
        """Return an ordered dictionary of field properties for this data source's attributes"""

        _attributes = OrderedDict()
        _layer_defn = self.layer.GetLayerDefn()

        if _layer_defn is not None:
            for i in range(_layer_defn.GetFieldCount()):
                _field_defn = _layer_defn.GetFieldDefn(i)
                _attributes[_field_defn.GetName()] = (
                    _field_defn.GetType(), _field_defn.GetWidth(), _field_defn.GetPrecision()
                )
                if self.debug:
                    self.logger.debug("Added %s to attributes: %s, %d, %d" % (_field_defn.GetName(),
                            _field_defn.GetTypeName(), _field_defn.GetWidth(), _field_defn.GetPrecision()))

        else:
            _attributes = None
            self.logger.error("Could not obtain layer definition for layer %s" % self.layer.GetName())

        return _attributes

    def setSchema(self, attrs_list):
        """Takes a list of StandardAttribute tuples and defines this data source's attribute schema"""

        if self.layer is not None:
            for sa in attrs_list:
                _field_defn = ogr.FieldDefn(sa.afn, sa.stype)
                _field_defn.SetWidth(sa.fwid)
                _field_defn.SetPrecision(sa.fprc)
                if self.layer.CreateField(_field_defn) != 0:
                    self.logger.error("Could not create field %s in layer %s" % (sa.afn, self.layer.GetName()))
                else:
                    if self.debug:
                        self.logger.debug("Field %s is type %s, width %d" % (_field_defn.GetName(),
                                            _field_defn.GetTypeName(), _field_defn.GetWidth()))

        else:
            self.logger.critical("Cannot define schema for NULL layer")
            raise RuntimeError("Cannot define schema for NULL layer")

    def makeFeatureFromFootprintRecord(self, fp_record):
        """Create an ogr.Feature from a FootprintRecord tuple"""

        # geom must be an ogr.Geometry object
        # attrs is an OrderedDict: key=ESRI-suitable field name, value=field value
        # Current layer definition used

        feature = None

        if self.layer is not None:
            feature = ogr.Feature(self.layer.GetLayerDefn())
            for attr_name, attr_val in fp_record.attrs_dict.items():

                if attr_name not in CNID_FROM_ACN:
                    continue

                if CNID_FROM_ACN[attr_name] not in PGCFP_COMMON_NAME_TO_ESRI_NAME:
                    continue

                fp_attr_name = PGCFP_COMMON_NAME_TO_ESRI_NAME[CNID_FROM_ACN[attr_name]]
                field_defn = feature.GetFieldDefnRef(fp_attr_name)

                # Special case: acquisition_time is saved as a string to preserve the subseconds
                if attr_name == "acquisition_time":
                    msecs = ".%06d" % attr_val.microsecond
                    attr_val = attr_val.strftime("%Y-%m-%dT%H:%M:%S") + msecs

                # ogr.Feature.SetField works differently for datetime values
                if isinstance(attr_val, datetime.datetime):

                    if field_defn.GetType() == ogr.OFTDateTime:
                        feature.SetField(fp_attr_name, attr_val.year, attr_val.month, attr_val.day,
                                         attr_val.hour, attr_val.minute, attr_val.second, 0)
                    elif field_defn.GetType() == ogr.OFTDate:
                        feature.SetField(fp_attr_name, attr_val.year, attr_val.month, attr_val.day,
                                         0, 0, 0, 0)

                else:
                    # Strings must be ASCII encoded
                    if isinstance(attr_val, basestring):
                        attr_val = attr_val.encode("ascii", "").strip()

                        # Guard against overflowing max text field size
                        if len(attr_val) > field_defn.GetWidth():
                            attr_val = attr_val[:field_defn.GetWidth() - 2] + "~"

                    feature.SetField(fp_attr_name, attr_val)

            # Check and set the geometry
            if fp_record.geom is not None and fp_record.geom.IsValid():
                feature.SetGeometry(fp_record.geom)
            else:
                self.logger.warning("Unable to add a valid geometry to feature")

        else:
            self.logger.error("Cannot create features for a NULL layer")

        return feature

    def addFeature(self, feature):
        """Add an ogr.Feature to the current layer"""
        return self.layer.CreateFeature(feature)

    def getFeature(self, fid):
        """Return the feature with the given FID"""
        return self.layer.GetFeature(fid)

    def setFeature(self, feature):
        """Update a provided feature in the current layer"""
        return self.layer.SetFeature(feature)  # Returns OGR error code

    def __len__(self):
        count = self.layer.GetFeatureCount() if self.layer is not None else 0
        return count

    def __iter__(self):
        self.layer.ResetReading()
        for feature in self.layer:
            yield feature


### Functions

# Get a StandardAttribute definition for a particular common name
def getStandardAttributeForCommonName(cname, attr_defns):
    """Find a StandardAttribute definition that matches a common name"""

    sa_defn = None
    if cname in CNID_FROM_ACN:
        sa_defn = attr_defns[CNID_FROM_ACN[cname]]

    return sa_defn

# Build a StandardAttribute definitions list from selected VendorNameDefns
def makeStandardAttributeDefnsList(cnames):
    """Build a list of StandardAttribute tuples from a list of attribute common names"""
    cnids = [CNID_FROM_ACN[cname] for cname in cnames]
    return [sa for cnid, sa in STD_ATTRS_BY_CNID.items() if cnid in cnids]
