#!/usr/bin/env python

import sys
import os
import argparse
import datetime
import logging
import shutil
import csv

import win32api
import win32file

from pgclib.scenefiles import SceneFilesCollection
import pgclib.esriutils as esri
import pgclib.constants as pgc
import pgclib.utils as utils


__version__ = "2.2.0"

DRIVE_FOLDER_PREFIXES = ("NGA_", "DG_", "GE_")

def getDrives(type_list=[3, 4]):
	drives = [drv for drv in win32api.GetLogicalDriveStrings().split("\\\x00") if drv]
	return [drv for drv in drives if win32file.GetDriveType(drv) in type_list]


### Initialization
begin_time = datetime.datetime.now()
argp = argparse.ArgumentParser(description="PGC Raw Imagery File Copier v%s" % __version__)
argp.add_argument("-x", "--execute", action="store_true", dest="execute", default=False,
				  help="Files will not actually be copied if option not included")
argp.add_argument("--shp", dest="shp", default=None, help="Path to shapefile of imagery to copy")
argp.add_argument("--fld", dest="fld", default="O_FILEPATH",
				  help="Shapefile field with containing path to image file")
argp.add_argument("--dst", dest="dst", default=None, help="Destination directory for imagery files")

argp.add_argument("--debug", action="store_true", default=False)
argp.add_argument("--log", dest="logpath")
argp.add_argument("--errlog", dest="errlogpath")

args = argp.parse_args()

# Set up logging
logfile = r"pgc_rifc_v%s_%s_%s.log" % (__version__, args.shp.split(os.sep)[-1],
											   begin_time.strftime("%Y%m%dT%H%M"))
errlogfile = r"pgc_rifc_errors_v%s_%s_%s.log" % (__version__, args.shp.split(os.sep)[-1],
                                               begin_time.strftime("%Y%m%dT%H%M"))

if args.logpath:
    if os.path.isdir(args.logpath):
        logfile = os.path.join(args.logpath, logfile)
    else:
        logfile = args.logpath
else:
    default_log_path = os.sep.join((os.environ["HOMEDRIVE"],
                                    os.environ["HOMEPATH"], "My Documents"))
    logfile = os.path.join(default_log_path, logfile)

if args.errlogpath:
    if os.path.isdir(args.errlogpath):
        errlogfile = os.path.join(args.errlogpath, errlogfile)
    else:
        errlogfile = args.errlogpath
else:
    if args.logpath and os.path.isdir(args.logpath):
        errlogfile = os.path.join(args.logpath, errlogfile)
    else:
        default_errlog_path = os.sep.join((os.environ["HOMEDRIVE"],
                                        os.environ["HOMEPATH"], "My Documents"))
        errlogfile = os.path.join(default_errlog_path, errlogfile)

basic_level = logging.DEBUG if args.debug else logging.INFO

logger = logging.getLogger("pgclib")
logger.setLevel(basic_level)

formatter = logging.Formatter("%(asctime)s :: %(module)s - %(funcName)s - %(levelname)s: %(message)s")
logfh = logging.FileHandler(logfile)
logfh.setLevel(basic_level)
logfh.setFormatter(formatter)
logefh = logging.FileHandler(errlogfile)
logefh.setLevel(logging.WARNING)
logefh.setFormatter(formatter)
logch = logging.StreamHandler(sys.stdout)
logch.setLevel(logging.INFO)
logch.setFormatter(formatter)

logger.addHandler(logfh)
logger.addHandler(logefh)
logger.addHandler(logch)


### Main application
logger.info("Starting at %s" % begin_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Source shapefile: %s" % args.shp)

shp_file = esri.OgrInterfaceForESRI(esri.OGR_SHAPEFILE_DRIVER, args.shp)
if not args.fld in shp_file.getSchema():
    logger.error("Could find %s in shapefile attribute fields" % args.fld)
    sys.exit(-1)

# Load the drive map to provide simple index numbers for drive serial numbers
drive_mapfile = os.path.join(os.path.dirname(sys.modules["pgclib"].__file__),
							 r"data/drive_number_map.csv")
drive_map = {}
with open(drive_mapfile, "rb") as drive_info:
	rdr = csv.reader(drive_info, delimiter=",")
	drive_map = {row[0]:row[1] for row in rdr}
if not drive_map:
	logger.warning("Could not read drive number map file -- cannot provide simple drive numbers")

# Load the list of failed drives
failed_drives_filepath = os.path.join(os.path.dirname(sys.modules["pgclib"].__file__),
                                      r"data/failed_drives.txt")
with open(failed_drives_filepath) as failed_drives_file:
    failed_drives = [line.strip().upper() for line in failed_drives_file]

# Identify the drives found in the indicated shapefile
logger.info("Examining shapefile...")
drives = set()
for feature in shp_file:
	if not feature[args.fld] or feature[args.fld] is None:
		continue
	if ":" in feature[args.fld] and feature[args.fld].index(":") == 1:
		path = os.path.splitdrive(feature[args.fld])[1]
	else:
		path = feature[args.fld]
	sep = "/" if "/" in path else os.sep
	exp_path = path.split(sep)
	if exp_path[1].startswith(DRIVE_FOLDER_PREFIXES):
		drives.add(exp_path[1])
needed_drives = []
for drv in sorted(drives):
    if drv.upper() in failed_drives:
        needed_drives.append(("FAILED DRIVE", drv))
    else:
        needed_drives.append((drive_map.get(drv, ""), drv))

logger.info("You will need these drives:\n\n  %s\n" % "\n  ".join("%4s: %s" % tup for tup in needed_drives))

# Try and discover which, if any, drives are presently mounted
drives_dct = {}
for drv in drives:
	for disk in getDrives([3]):
		path = os.sep.join((disk, drv))
		if os.path.isdir(path):
			drives_dct.setdefault(drv, []).append(disk)

# Make sure we found something
if not drives_dct:
	logger.error("Could not identify any drives from %s" % args.shp)
	sys.exit(-1)

found_drives = [os.sep.join((disks[0], drv)) for drv, disks in drives_dct.items()]
logger.info("Will use drives:\n\n  %s\n" % "\n  ".join(found_drives))

# For each discovered drive, collect it's scenes and then copy any of them that exist
# in the selected shapefile
for drv_path in found_drives:
	logger.info("Collecting scene files from %s" % drv_path)
	scene_files = SceneFilesCollection(drv_path, keeps=[pgc.IMAGE])
	sf_dict = {sf.file_name.lower():sf for sf in scene_files}
	logger.info("  ...found %d image scene files" % len(sf_dict))
	scene_files = None

	# Find all the directories containing feature image files
	logger.info("Examining shapefile...")
	copy_items = set()
	for feature in shp_file:
		if not feature[args.fld]:
			continue
		feat_fn = os.path.basename(feature[args.fld])
		if feat_fn.lower() in sf_dict:
			copy_items.add(sf_dict[feat_fn.lower()].file_dir)
	logger.info("  ...found %d image file directories" % len(copy_items))
	sf_dict = None

	# Start copying items
	logger.info("Start copying items...")
	exp_drv = drv_path.split(os.sep)
	for dir_path in sorted(copy_items):
		exp_path = dir_path.split(os.sep)
		cmn_path = os.sep.join([p for p in exp_path if p not in exp_drv])  # part of src path to keep
		destp = os.path.join(args.dst, cmn_path)  # item dest directory
		if args.execute and not os.path.isdir(destp):
			os.makedirs(destp)  # create all the necessary directories

		# Walk through image file home directory
		for item in utils.getFilesByType(dir_path):
			# Does the item already exist at the destination?
			dest = os.path.join(destp, os.path.basename(item))
			if not os.path.isfile(dest):
				if args.execute:
					try:
						shutil.copyfile(item, dest)
					except IOError as err:
						logger.error("Could not copy %s because %s" % (item, err))
						continue  # Skip to next item
				else:
					logger.info("Copy %s to %s" % (item, dest))
			else:
				logger.info("Skipping %s -- file already exists at %s" % (item, destp))

	logger.info("  ...done!")

end_time = datetime.datetime.now()
logger.info("Finished processing at: %s" % end_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Total processing time: %s" % str(end_time - begin_time))
