#!/usr/bin/env python

import sys
import os
import datetime
import logging
import argparse

from pgclib.scenefiles import SceneFilesCollection
from pgclib.scenes import ScenesCollection
from pgclib.footprint import Footprint, makeFootprintRecord
import pgclib.constants as pgc
import pgclib.attributes as attrs
import pgclib.esriutils as esri


__version__ = "2.3.6"


### Initialization
begin_time = datetime.datetime.now()
argp = argparse.ArgumentParser(description="PGC Footprinter v%s" % __version__)

argp.add_argument(dest="src_path", metavar="<source folder>",
                  help="Path to the folder of imagery to footprint")
argp.add_argument(dest="output", metavar="<destination>", help="destination")

fpt_group = argp.add_mutually_exclusive_group()
fpt_group.add_argument("--ingest", action="store_true", help="Footprint for ingest")
fpt_group.add_argument("--server", action="store_true", help="Footprint of server volume")

path_group = argp.add_mutually_exclusive_group()
path_group.add_argument("--winpaths", action="store_true", help="Write all paths for Windows")
path_group.add_argument("--unixpaths", action="store_true", help="Write all paths for Gluster server")

argp.add_argument("--attrset", dest="attrset", default=None)
argp.add_argument("--layer", dest="lyr_name", default=None)
argp.add_argument("--status", dest="status", default=None)
argp.add_argument("--recvd", dest="recvd", default=None)
argp.add_argument("--fpadd", dest="fpadd", default=None)
argp.add_argument("--fpmod", dest="fpmod", default=None)
argp.add_argument("-d", "--debug", action="store_true", default=False)
argp.add_argument("--log", dest="logpath")
argp.add_argument("--errlog", dest="errlogpath")

argp.add_argument("-k", "--keepdrv", action="store_true", default=False,
                  help="Keep drive letter in paths")
argp.add_argument("-u", "--update", action="store_true", default=False,
                  help="Update existing footprint")

args = argp.parse_args()

# Set up logging
logfile = r"pgc_fp_v%s_%s_%s.log" % (__version__, args.src_path.split(os.sep)[-1],
                                               begin_time.strftime("%Y%m%dT%H%M"))
errlogfile = r"pgc_fp_errors_v%s_%s_%s.log" % (__version__, args.src_path.split(os.sep)[-1],
                                               begin_time.strftime("%Y%m%dT%H%M"))

if args.logpath:
    if os.path.isdir(args.logpath):
        logfile = os.path.join(args.logpath, logfile)
    else:
        logfile = args.logpath
else:
    default_log_path = os.sep.join((os.environ["HOMEDRIVE"],
                                    os.environ["HOMEPATH"], "My Documents"))
    logfile = os.path.join(default_log_path, logfile)

if args.errlogpath:
    if os.path.isdir(args.errlogpath):
        errlogfile = os.path.join(args.errlogpath, errlogfile)
    else:
        errlogfile = args.errlogpath
else:
    if args.logpath and os.path.isdir(args.logpath):
        errlogfile = os.path.join(args.logpath, errlogfile)
    else:
        default_errlog_path = os.sep.join((os.environ["HOMEDRIVE"],
                                        os.environ["HOMEPATH"], "My Documents"))
        errlogfile = os.path.join(default_errlog_path, errlogfile)

basic_level = logging.DEBUG if args.debug else logging.INFO

logger = logging.getLogger("pgclib")
logger.setLevel(basic_level)

formatter = logging.Formatter("%(asctime)s :: %(module)s - %(funcName)s - %(levelname)s: %(message)s")
logfh = logging.FileHandler(logfile)
logfh.setLevel(basic_level)
logfh.setFormatter(formatter)
logefh = logging.FileHandler(errlogfile)
logefh.setLevel(logging.WARNING)
logefh.setFormatter(formatter)
logch = logging.StreamHandler(sys.stdout)
logch.setLevel(logging.INFO)
logch.setFormatter(formatter)

logger.addHandler(logfh)
logger.addHandler(logefh)
logger.addHandler(logch)


### Main application
logger.info("Starting at %s" % begin_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Source path: %s" % args.src_path)

## Collect SceneFiles, organize into Scenes and create Footprint
logger.info("Start collecting scene files...")
sfc = SceneFilesCollection(args.src_path, skips=[pgc.SUPPORT])
logger.info("Found %d SceneFiles" % len(sfc))

if len(sfc) == 0:
    logger.warning("No files found to process - exiting...")
    sys.exit(-1)

logger.info("Sorting into scenes...")
sc = ScenesCollection()
sc.loadScenesFromSceneFilesCollection(sfc)
logger.info("Produced %d Scenes" % len(sc))

uparams = vars(args)
fp = Footprint({key:value for key, value in uparams.items() if value is not None})


## Walking through files and creating/updating a footprint

# Determine which OGR driver to use
if uparams["output"].lower().endswith(".gdb"):
    driver = esri.OGR_FILEGDB_DRIVER
elif uparams["output"].lower().endswith(".shp"):
    driver = esri.OGR_SHAPEFILE_DRIVER
else:
    logger.error("Unsupported output type: %s" % uparams["output"])
    sys.exit(-1)

# Update if output already exists, otherwise write or overwrite
if uparams.get("update") and os.path.exists(uparams["output"]):
    mode = "u"
else:
    mode = "w"

# Create the OGR handle and set attribute scema
if args.lyr_name:
    fp_out = esri.OgrInterfaceForESRI(driver, args.output, mode=mode, layer=args.lyr_name)
else:
    fp_out = esri.OgrInterfaceForESRI(driver, args.output, mode=mode)

if mode == "w":
    fp_out.setSchema(esri.makeStandardAttributeDefnsList(fp.attr_names))

scn_cnt = len(sc)
for idx, scn in enumerate(sc):

    if scn.imageSceneFile is None:
        logger.warning("Scene with no image file -- skipping: %s" % scn.sort_code)
        continue

    if not scn.is_valid:
        logger.warning("Scene not valid -- skipping: %s" % scn.imageSceneFile.as_path)
        continue

    now = datetime.datetime.now()
    logger.info("Scene: (%d/%d) strip_id -> %s" % (idx+1, scn_cnt, scn.strip_id))
    logger.info("  path -> %s" % scn.imageSceneFile.as_path)

    if scn.vendor != pgc.IK:
        image_sfs = [scn.imageSceneFile]
    else:
        if scn.is_ikonos_bands:
            image_sfs = scn.getSceneFile(pgc.IMAGE, all_bands=True)
        else:
            image_sfs = [scn.imageSceneFile]

    for image_sf in image_sfs:
        if not image_sf:
            continue

        try:
            fp_rec = makeFootprintRecord(scn.gid, image_sf, scn, fp)
        except Exception as err:
            logger.error("Scene %s: Could not create footprint record because %s" % \
                         (scn.imageSceneFile.as_path, err))
            continue

        if fp_rec.attrs_dict:
            c_code = fp_rec.attrs_dict.get("country_code")
            a_time = fp_rec.attrs_dict.get("acquisition_time")
            if c_code and a_time:
                logger.info("  country code -> %s, acq time -> %s.%06d" % (c_code,
                                a_time.strftime("%Y-%m-%dT%H:%M:%S"), a_time.microsecond))
            else:
                logger.warning("  Missing attributes!!! Skipping... -> %s" % str(fp_rec.attrs_dict))
                continue

        feat = fp_out.makeFeatureFromFootprintRecord(fp_rec)
        success = fp_out.addFeature(feat)
        if success != 0:
            logger.warning("  add feature failed")


end_time = datetime.datetime.now()
logger.info("Finished processing - total time: %s" % str(end_time - begin_time))
