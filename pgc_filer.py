#!/usr/bin/env python

import sys
import os
import datetime
import logging
import argparse
import shutil

from dateutil import parser as DP

from pgclib.scenefiles import SceneFilesCollection
from pgclib.scenes import ScenesCollection
from pgclib.constants import *
import pgclib.esriutils as esri
import pgclib.utils as utils

__version__ = "1.1.0"


### Initialization
begin_time = datetime.datetime.now()
argp = argparse.ArgumentParser(description="PGC Filer v%s" % __version__)

argp.add_argument(dest="src_path", metavar="<source folder>",
				  help="Path to the folder of renamed imagery to be filed")

argp.add_argument("-x", "--execute", action="store_true", dest="execute", default=False,
                  help="Files will not be copied or moved without this flag")
argp.add_argument("--fromunix", action="store_true", dest="from_unix", default=False,
                  help="Calculate destination path in UNIX terms")
argp.add_argument("--layer", dest="layer", default=None, help="Name of GDB layer to open")

argp.add_argument("--debug", action="store_true", default=False)
argp.add_argument("--log", dest="logpath")
argp.add_argument("--errlog", dest="errlogpath")

args = argp.parse_args()

# Set up logging
logfile = r"pgc_filer_v%s_%s_%s.log" % (__version__, args.src_path.split(os.sep)[-1],
											   begin_time.strftime("%Y%m%dT%H%M"))
errlogfile = r"pgc_filer_errors_v%s_%s_%s.log" % (__version__, args.src_path.split(os.sep)[-1],
                                               begin_time.strftime("%Y%m%dT%H%M"))

if args.logpath:
    if os.path.isdir(args.logpath):
        logfile = os.path.join(args.logpath, logfile)
    else:
        logfile = args.logpath

if args.errlogpath:
    if os.path.isdir(args.errlogpath):
        errlogfile = os.path.join(args.errlogpath, errlogfile)
    else:
        errlogfile = args.errlogpath
else:
    if os.path.isdir(args.logpath):
        errlogfile = os.path.join(args.logpath, errlogfile)

basic_level = logging.DEBUG if args.debug else logging.INFO

logger = logging.getLogger("pgclib")
logger.setLevel(basic_level)

formatter = logging.Formatter("%(asctime)s :: %(module)s - %(funcName)s - %(levelname)s: %(message)s")
logfh = logging.FileHandler(logfile)
logfh.setLevel(basic_level)
logfh.setFormatter(formatter)
logefh = logging.FileHandler(errlogfile)
logefh.setLevel(logging.WARNING)
logefh.setFormatter(formatter)
logch = logging.StreamHandler(sys.stdout)
logch.setLevel(logging.INFO)
logch.setFormatter(formatter)

logger.addHandler(logfh)
logger.addHandler(logefh)
logger.addHandler(logch)


### Main application
logger.info("Starting at %s" % begin_time.strftime("%Y-%m-%d+%H:%M:%S"))
logger.info("Source path: %s" % args.src_path)

## Pre-defined values
SENSOR_FOLDERS = {
    "WV01":"worldview1", "WV02":"worldview2", "QB02":"quickbird",
    "GE01":"geoeye1", "IK01":"ikonos", "OV03":"orbview3"
}

if args.from_unix:
    SERVER_PATH_PREFIX = utils.GLUSTER_PREFIX
    MASTER_FOOTPRINT_PATH = utils.GLUSTER_PREFIX
else:
    SERVER_PATH_PREFIX = utils.ESCI_WINDOWS_PREFIX
    MASTER_FOOTPRINT_PATH = utils.ESCI_WINDOWS_PREFIX
SERVER_PATH_PREFIX += "/private/imagery/satellite/archive/original"
MASTER_FOOTPRINT_PATH += "/private/imagery/satellite/_footprints/AGIC_footprint/pgcImageryIndexV3.gdb"

## Collect SceneFiles, organize into Scenes and create Footprint
logger.info("Start collecting scene files...")
scene_files = SceneFilesCollection(args.src_path)
logger.info("Found %d SceneFiles" % len(scene_files))

if len(scene_files) == 0:
    logger.warning("No files found to process - exiting...")
    sys.exit(-1)

## Open the master footprint and make a lookup table for updating
#lyr_name = args.layer if args.layer else "pgcImageryIndexV3_" + \
#           begin_time.strftime("%Y%b%d").lower()
#fp_gdb = esri.OgrInterfaceForESRI(esri.OGR_FILEGDB_DRIVER, MASTER_FOOTPRINT_PATH,
#                                  layer=lyr_name, mode="u")
#logger.info("Checking the master footprint (layer: %s)..." % lyr_name)
#logger.info("  Obtained layer %s" % fp_gdb.layer.GetName())
#logger.info("  Layer has %d features" % fp_gdb.layer.GetFeatureCount())
#
#logger.info("Building lookup table...")
#fid_lookup = {}
#for feature in fp_gdb.layer:
#    #pgc_id = "|".join((feature["SCENE_ID"], feature["ORDER_ID"]))
#    fid_lookup.setdefault(feature["SCENE_ID"], []).append(feature.GetFID())
#logger.info("  >> last feature %d -> %s" % (feature.GetFID(), feature["SCENE_ID"]))
#logger.info("  ...done! Lookup table has %d entries" % len(fid_lookup))
#feature = None

#cnt = 0
#for pgc_id in sorted(fid_lookup.iterkeys()):
#    fids = fid_lookup[pgc_id]
#    if len(fids) > 1:
#        cnt += 1
#        logger.info("scene_id: %s -> %s" % (pgc_id, str(fids)))
#logger.info("Final count -> %d" % cnt)

# Group SceneFiles by image
logger.info("Grouping scene files...")
scenes = ScenesCollection()
scenes.loadScenesFromSceneFilesCollection(scene_files)
logger.info("  ...done! Made %d scenes" % len(scenes))
scene_files = None


### Processing loop
logger.info("Start processing scenes...")
scene_count = 0
cf = open(r"E:\changes.txt", "w")
cf.write("SCENE_ID,S_FILENAME,S_FILEPATH,PREVIEWJPG,MOD_DATE\n")
for scene in scenes:
    scene_count += 1

    image_sf = scene.imageSceneFile
    if image_sf.mode != RENAMED:
        logger.warning("SceneFile %s is not renamed imagery - skipping" % image_sf.as_path)

    logger.info("Processing (%d/%d) %s" % (scene_count, len(scenes), image_sf.as_path))
    if basic_level == logging.DEBUG:
        logger.debug("  scene_id -> %s" % scene.gid)

    # Determine the remainder of the destination path from the image sensor,
    # acquisition time and product code

    # Sensor
    sensor = image_sf.name_dict.get("snsr")
    if not sensor:
        logger.warning("Sensor code not found for %s - skipping" % image_sf.as_path)
        continue
    if basic_level == logging.DEBUG:
        logger.debug("  sensor -> %s" % sensor)

    # Product Code
    if image_sf.vendor == DG:
        product = image_sf.name_dict.get("prod")[1:3]
    elif image_sf.vendor == GE:
        product = image_sf.name_dict.get("prod")
    elif image_sf.vendor == IK:
        product = "geo"
    else:
        product = None
        logger.warning("Product code not found for %s - skipping" % image_sf.as_path)
        continue
    if basic_level == logging.DEBUG:
        logger.debug("  product code -> %s" % product)

    # Acquisition Time
    if image_sf.vendor == DG:
        ts = image_sf.name_dict.get("ts")[:7]   # yymmmdd
        ts = "-".join((ts[2:5], ts[5:7], ts[0:2]))
    elif image_sf.vendor == GE:
        ts = image_sf.name_dict.get("ts")   # yymmdd
    elif image_sf.vendor == IK:
        ts = image_sf.name_dict.get("ts")[:8]   # yyyymmdd
    else:
        ts = None
        logger.warning("Acquisition time not found for %s - skipping" % \
                       image_sf.as_path)
        continue

    try:
        acqt = DP.parse(ts)
    except Exception as err:
        acqt = None
        logger.error("Failed to parse acquisition time: %s - skipping" % err)
        continue

    if basic_level == logging.DEBUG:
        logger.debug("  acquisition time -> %s" % acqt.strftime("%Y-%m-%d"))

    # Make destination path
    dest_dir = "/".join((SERVER_PATH_PREFIX, SENSOR_FOLDERS[sensor], product,
                         str(acqt.year), acqt.strftime("%m_%b").lower(), "%02d" % acqt.day))
    if basic_level == logging.DEBUG:
        logger.debug("  dest_dir -> %s" % dest_dir)

    # Create the necessary destination folder(s) first, as needed
    if args.execute:
        if not os.path.isdir(dest_dir):
            try:
                os.makedirs(dest_dir)
            except IOError as err:
                logger.error("Cannot create destination directory %s because %s - skipping" % \
                             (dest_dir, err))
                continue
            logger.info("Created destination directory: %s" % dest_dir)

    # Time to move the image file
    if basic_level == logging.DEBUG:
        logger.debug("Relocating file %s" % image_sf.as_path)
        logger.debug("Destination directory: %s" % dest_dir)

    dest_path = "/".join((dest_dir, image_sf.file_name))
    if not os.path.isfile(dest_path):
        if args.execute:
            try:
                shutil.copyfile(image_sf.as_path, dest_path)
            except IOError as err:
                logger.error("Could not copy/move %s because %s - skipping" % (path, err))
                continue

            #if fid:
            #    feature = fp_gdb.getFeature(fid)
            #    feature.SetField("STATUS", "online")
            #    feature.SetField("S_FILENAME", os.path.basename(dest_path))
            #    feature.SetField("S_FILEPATH", dest_path)
            #    if feature["PREVIEWJPG"]:
            #        feature.SetField("PREVIEWJPG", "/".join((dest_dir, os.path.basename(feature["PREVIEWJPG"]))))
            #    feature.SetField("MOD_DATE", begin_time.year, begin_time.month,
            #                     begin_time.day, 0, 0, 0, 0)
            #    ec = fp_gdb.setFeature(feature)
            #    logger.info("Updated feature %d: return code -> %d" % (fid, ec))
            #    if ec != 0:
            #        logger.warning("OGR Return Code Non-Zero for feature %d" % fid)
            prev_sf = scene.getSceneFile(PREVIEW)
            cf.write("%s,%s,%s,%s,%s\n" % (scene.gid, os.path.basename(dest_path),
                                           dest_path, "/".join((dest_dir, prev_sf.file_name)),
                                           begin_time.strftime("%Y-%m-%d")))

        else:
            logger.info("Copying: %s" % image_sf.as_path)
            logger.info("     to: %s" % dest_path)
            #if fid:
            #    feature = fp_gdb.getFeature(fid)
            #    logger.info("Current STATUS: %s" % feature["STATUS"])
            #    logger.info("O_FILENAME: %s" % feature["O_FILENAME"])
            #    logger.info("O_FILEPATH: %s" % feature["O_FILEPATH"])
            #    logger.info("New S_FILENAME: %s" % os.path.basename(dest_path))
            #    logger.info("New S_FILEPATH: %s" % dest_path)
            #    logger.info("New PREVIEWJPG: %s" % "/".join((dest_dir,
            #                            os.path.basename(feature["PREVIEWJPG"]))))
            #    logger.info("New MOD_DATE: %s" % begin_time.strftime("%m/%d/%Y"))

    else:
        logger.warning("File %s already exists at destination - skipping" % image_sf.as_path)
        continue

    # Now move the remaining files
    for scene_file in [sf for sf in scene if sf.kind != IMAGE]:

        dest_path = "/".join((dest_dir, scene_file.file_name))
        if not os.path.isfile(dest_path):
            if args.execute:
                try:
                    shutil.copyfile(scene_file.as_path, dest_path)
                except IOError as err:
                    logger.error("Could not copy %s because %s - skipping" % (scene_file.as_path, err))
                    continue

                if basic_level == logging.DEBUG:
                    logger.debug("Copied %s to %s" % (scene_file.as_path, dest_path))

            else:
                logger.info("Copying: %s" % scene_file.as_path)
                logger.info("     to: %s" % dest_path)

    logger.info("  ...done! Finished scene %d" % scene_count)


logger.info("  ...done!")
cf.close()


end_time = datetime.datetime.now()
logger.info("Finished processing - total time: %s" % str(end_time - begin_time))
