
# PGC Tools for managing original imagery

 This package contains applications for working with and managing original, or 'raw', imagery
 as received at PGC. Utilities include:

    pgc_rn.py         - Renamer: renames raw imagery with PGC names, bundles extra support
                        files in a tarball if necessary or extracts metadata and preview files
                        from pre-existing tarball; also detects intra-volume scene duplication.

    pgc_fp.py         - Footprinter: generates a shapefile footprint of a collection of imagery
                        as a layer of polygons, marked with a selection of attributes about
                        each scene.

	pgc_rifc.py 	  - Raw Imagery File Copier: copies files/directories from archive disks
						to a specified destination before renaming and other processing; uses
						a footprint shapefile to determine which images (and their support
						files) to copy. Will produce a list of archive disks required to copy
						all files found in footprint.

    pgc_ifc.py        - Imagery File Copier: copies files from sources of renamed imagery to
                        a specified destination; uses a footprint shapefile to determine which
                        images (and their support files) to copy. A field in the shapefile may
                        be designated to use for names of subfolders to group the imagery into.


## Footprinter

  Usage: python pgc_fp.py [options] <source folder> <destination>

    source:       Full path to a folder of raw imagery to be footprinted
    destination:  Full path to where footprint should be created. Form depends on how the
                  path is constructed:

                  * If the path ends in a filename (file.shp), a shapefile will be assumed
                  * If the path ends in a folder name with a .gdb extension, an ESRI
                    file geodatabase (GDB) will be assumed
                  * If the path ends in any other sort of name, a folder containing
                    shapefiles will be assumed

    --ingest:       A preset that sets several other options for the "add new volume of
                    imagery to the collection" workflow. Received, added and modified dates
                    are all set (defaulting to current date), status is set to "offline",
                    and PGC master footprint attributes are assumed. These options can be
                    overridden by specifying them explicitly.
    --server:       Another preset, useful only for PGC staff tasked with updating the
                    master footprint with renamed imagery loaded to the PGC file server
    <nothing>       If neither of the previous presets are chosen, "mapping only" mode is
                    assumed. Status defaults to "local" and none of the date fields are
                    included.

    --winpaths:     PGC-only option. Forces file paths in attributes to be written in
                    Windows form, to PGC pre-defined server locations.
    --unixpaths:    PGC-only option. Like winpaths, but forces UNIX-style paths.

    --attrset:      Specifies a path to a text file containing a list (one per line) of the
                    attribute common names desired for this footprint. Common names can be
                    found in pgclib/attributes.py.

    --layer:        Specifies the name of a layer in a GDB or folder data source. If a new
                    source is being created, becomes the name of the new layer. If an
                    existing source is being updated, and the layer exists, becoomes the
                    layer updated. Otherwise becomes the new layer written.

    --status:       Forces the status attribute to be the specified value, regardless of
                    other options. Must be one of: "local", "online", "offline".
    --recvd:        Sets the received_date attribute to the date specified. Should be of
                    form: YYYY-mm-dd. Defaults to current date.
    --fpadd:        Same as --recvd, but sets the footprint_added attribute field.
    --fpmod:        Same as --recvd, but sets the footprint_modified attribute field.

    -k, --keepdrv:  Causes the drive letter in Windows-style paths to be retained in
                    attribute fields with paths for values.
    -u, --update:   Specifies the destination is an existing data source to be updated,
                    rather than created or overwritten.

    -d, --debug:    When included, additional information is written to the log and console
    --log:          Specifies where to write the regular log. If the path is for a file, the
                    log will be written to that file. If the path is to a folder, a log file
                    with a constructed name will be written into that folder.
    --errlog:       Same as log, except only warnings and more severe errors will be
                    written to the log. Convenient for finding errors when using the --debug
                    option on the regular log.

## Renamer

  Usage: python pgc_rn.py [options] <source folder> <output folder>

    source:       Full path to a folder of raw imagery to be renamed
    destination:  Full path to a folder to write renamed imagery into

    NOTE: These should NOT be the same folder

    -x, --execute:  Include this parameter to carry out actual operations, otherwise only a
                    report is generated
    -d, --debug:    When included, additional information is written to the log and console
    -u, --undo:     When included, reverses normal operations and moves renamed imagery back to
                    its original locations (must use with -x for actual execution)
    --log:          Specifies where to write the regular log. If the path is for a file, the
                    log will be written to that file. If the path is to a folder, a log file
                    with a constructed name will be written into that folder.
    --errlog:       Same as log, except only warnings and more severe errors will be
                    written to the log. Convenient for finding errors when using the --debug
                    option on the regular log.


## Raw Imagery File Copier

  Usage: python pgc_rifc.py [options]

	-x, --execute:	Generates a report (log) only if not included
    --shp:			Path to shapefile footprint of imagery to copy
	--fld:			Field in shapefile containing paths to imagery files
					Default: O_FILEPATH
	--dst:			Path to destination directory for copied imageryo
    -d, --debug:    When included, additional information is written to the log and console
    --log:          Specifies where to write the regular log. If the path is for a file, the
                    log will be written to that file. If the path is to a folder, a log file
                    with a constructed name will be written into that folder.
    --errlog:       Same as log, except only warnings and more severe errors will be
                    written to the log. Convenient for finding errors when using the --debug
                    option on the regular log.


## Imagery File Copier

  Usage: python pgc_ifc.py [options]

    -x, --execute:  Generates a report (log) only if not specified
    --shp:          Path to shapefile containing paths to imagery to copy
    --imagefld:     Field in shapefile containing paths to imagery files
                    DEFAULT: S_FILEPATH
    --folderfld:    Field in shapefile to use to create subfolders for copied
                    imagery (new folder each time field changes)
    --dst:          Path to destination directory for copied imagery
    -d, --debug:    When included, additional information is written to the log and console
    --log:          Specifies where to write the regular log. If the path is for a file, the
                    log will be written to that file. If the path is to a folder, a log file
                    with a constructed name will be written into that folder.
    --errlog:       Same as log, except only warnings and more severe errors will be
                    written to the log. Convenient for finding errors when using the --debug
                    option on the regular log.
