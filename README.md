
# PGC Tools for managing original imagery

 This package contains applications for working with and managing original, or 'raw', imagery
 as received at PGC. Utilities include:

    pgc_rn.py         - Renamer: renames raw imagery with PGC names, bundles extra support
                        files in a tarball if necessary or extracts metadata and preview files
                        from pre-existing tarball; also detects intra-volume scene duplication.

    pgc_fp.py         - Footprinter: generates a shapefile footprint of a collection of imagery
                        as a layer of polygons, marked with a selection of attributes about
                        each scene.

	pgc_rifc.py 	  - Raw Imagery File Copier: copies files/directories from archive disks
						to a specified destination before renaming and other processing; uses
						a footprint shapefile to determine which images (and their support
						files) to copy. Will produce a list of archive disks required to copy
						all files found in footprint.

    pgc_ifc.py        - Imagery File Copier: copies files from sources of renamed imagery to
                        a specified destination; uses a footprint shapefile to determine which
                        images (and their support files) to copy. A field in the shapefile may
                        be designated to use for names of subfolders to group the imagery into.


## pgclib

    2.0.9       - Initial commit
    2.1.0       - Changed geometry.py to use existing (GCP) WGS84 coordinates when present, rather
                  than reprojecting anyway
                - Added strip_id property to Scene objects to pave way for Strip objects
                - Updated attributes.py, footprint.py and pgc_fp.py to accomodate strip_id
                  changes
    2.1.1       - Added initial Strip support (strips.py)
                - Moved pgctools/data to pgctools/pgclib/data to provide for universal locating
                  the countries.shp shapefile
                - More error checking, small fixes
    2.1.2       - Changed scenefiles.py and regexes to properly separate IKONOS pan and ms
                  when creating scenes; needed for proper Strip building

	2.2.0		- New attributes in footprint, bug fixes, support for single-file IKONOS ms,
				  better default locations for logging
	2.2.1		- Fixed bug in IKONOS file handling that prevented all band files from being available
    2.2.2       - Fixed a bug that prevented O_FILENAME, O_FILEPATH and O_DRIVE from being populated
                  when making a local footprint; fixed a bug in the nga_name for IKONOS images;
                  fixed a typo bug in pgc_ifc.py
	2.2.3		- Fixed a fatal exception when a geotransform failed to be created for certain rasters
	2.2.4		- Fixed a problem when creating NGA names for IKONOS bands that were not blue or pan
	2.2.5		- Fixed two cases where malformed metadata files caused fatal exceptions; bad scenes are
				  now logged and skipped
	
	2.3.0		- Fixed a logic error where a Scene would incorrectly report it was IKONOS bands; updated
				  strips.py with new capabilities; updated pgc_rifc.py to use new simplified drive numbers 
				  as well as drive serial numbers, to make picking disks from bins easier
	
	2.3.1		- Fixed a problem with certain DG tiled images not geting proper PGC names
	2.3.2		- Fixed a case where a returned None value would cause a crash
	2.3.3		- Fixed a problem with generating PGC names for tiled DG imagery

## Release notes for 2.3.0

	pgc_rifc.py
	
	* Required drives list now includes both the original drive serial number and a simple drive number
	  from a list of assigned numbers, to make it easier to find particular disks in the storage bins
	
	scenes.py
	
	* Better handling of IKONOS images, both separate bands and stacked files
	* More cases where a bad scene is skipped rather than creating a fatal exception
	
	strips.py
	
	* Rewrote Strip and StripsCollection to bring it in line with how Scene and ScenesCollection operates
	* Added __hash__ and comparison operators based on strip_id rather than just catalog_id & order_id
	* Added is_same method to determine if deep comparison is needed to determine duplication
	* Added a validity check (cannot have empty or malformed strip_id or sort_code, or empty attributes)

## Release notes for 2.2.0

	pgc_fp.py, pgc_rn.py, pgc_ifc.py, pgc_rifc.py

	* Change in logging: default location is now local user "My Documents" directory if no location
	  is specified for log or errorlog (specifying a log location puts errorlog there by default)

	pgc_rn.py

	* Fixed bug where files extracted from TARs had incorrect names

	scenes.py

	* New attributes: nga_name, sort_code
	* Fixed bug causing fatal exception if no pgc_name could be generated; scene should now
	  simply be marked invalid
	* Added support for single-file IKONOS ms
	* ScenesCollection no longer automatically filters out duplicate names, is now an optional
	  argument to activate filtering
	* ScenesCollection now based on list rather than dictionary to support duplicate names

	metadata.py

	* Attributes may now be "list" types (strings concatenated with pipe | characters)

	geometry.py

	* Handles rasters with GCP keys of "1".."4" as well as "UpperLeft".."LowerLeft"
	* Fixed bug where invalid coordinates caused crash rather than return None
	* Added support for providing embedded NITF metadata needed to generate the WARP prefix
	  portion of NGA names

	attributes.py

	* New footprint attributes: strip_id, original_drive and nga_name
	* New metadata attributes: absolute_calibration_factor (DG), effective_bandwidth (DG), tdi,
	  radiometry_gain (GE), radiometry_offset (GE)
	* Added support for "list" attributes
